<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

Route::get('/coba', function () {
    $owner = \App\Models\Role::where('name', 'owner')->first();
    dd($owner == null);
});


Auth::routes();

Route::group(['prefix'=>'/', 'middleware'=>'auth', 'namespace'=>'Admin'], function() {
    Route::pattern('id', '[0-9]+');
    Route::pattern('userId', '[0-9]+');

    Route::get('cekhakakses/{hakAkses}', 'SearchController@cekHakAkses');

    Route::get('/', 'DashboardController@index');
    Route::get('/home', 'DashboardController@index');

    Route::resource('identitas', 'IdentitasController', ['except' => [
        'show', 'create', 'store', 'destroy', 'edit'
    ]]);

    Route::get('/getidentitas', 'IdentitasController@getIdentitas');
    Route::get('/getnamasekolah', 'IdentitasController@getNamaSekolah');

    Route::resource('pengesah', 'PengesahController', ['except' => [
        'show', 'create', 'store', 'destroy'
    ]]);
    Route::get('pengesahs', 'PengesahController@pengesahs');

    Route::group(['prefix'=> 'pengaturan'], function () {
        Route::resource('program', 'ProgramController', ['except' => 'show']);
        Route::get('programs', 'ProgramController@programs');

        Route::resource('tahun', 'TahunController', ['except' => 'show']);
        Route::get('tahuns', 'TahunController@tahuns');

        Route::resource('kelas', 'KelasController', ['except' => 'show']);
        Route::get('kelases', 'KelasController@kelases');
        Route::get('get_program_studi', 'KelasController@getProgramStudi');

        Route::resource('tarif', 'TarifController', ['except' => 'show']);
        Route::get('tarifs', 'TarifController@tarifs');

        Route::resource('mulaiiuran', 'MulaiIuranController', ['except' => ['show', 'destroy']]);
        Route::get('mulaiiurans', 'MulaiIuranController@mulaiiurans');
//
    });

    Route::resource('siswa', 'SiswaController', ['except' => 'show']);
    Route::get('siswas', 'SiswaController@siswas');
    Route::get('kelases', 'SiswaController@kelases');

    Route::get('get_daftar_siswa', 'SiswaController@getDaftarSiswa');
    Route::get('get_bulan', 'SearchController@getBulan');
    Route::get('getkodetransaksi', 'SearchController@getKodeTransaksi');
    Route::get('siswamulaisearch/{keyword}', 'SearchController@siswamulaisearch');
    Route::get('siswabynisn/{keyword}', 'SearchController@siswabynisn');

    Route::get('daftartunggakan', 'SearchController@daftarTunggakan');

    Route::resource('iuran', 'IuranController', ['except' => 'show', 'edit', 'update']);
    Route::get('iurans', 'IuranController@iurans');
    Route::get('iuran/nota/{keyword}', 'IuranController@nota');
    Route::get('iuran/previewnota/{keyword}', 'IuranController@previewnota');

    Route::group(['prefix'=> 'laporan'], function () {
        Route::get('daftarpenunggak',  'LaporanController@daftarPenunggak');
        Route::get('getdaftarpenunggak', 'LaporanController@getDaftarPenunggak');
        Route::get('surattagihan/{rows_selected}', 'LaporanController@suratTagihan');
        Route::get('lapdaftarpenunggak', 'LaporanController@laporanDaftarPenunggak');

        Route::get('daftarsiswa',['as' => 'laporan.daftarsiswa', 'uses' => 'LaporanController@daftarSiswa']);
        Route::get('getdaftarsiswa',['as' => 'laporan.getdaftarsiswa', 'uses' => 'LaporanController@getDaftarSiswa']);
        Route::get('laporandaftarsiswa',['as' => 'laporan.laporandaftarsiswa', 'uses' => 'LaporanController@laporanDaftarSiswa']);
        Route::get('laporanindividusiswa/{rows_selected}',['as' => 'laporan.laporanindividusiswa', 'uses' => 'LaporanController@laporanIndividuSiswa']);

        Route::get('siswaperprogram',['as' => 'laporan.siswaperprogram', 'uses' => 'LaporanController@siswaPerProgram']);
        Route::get('getsiswaperprogram',['as' => 'laporan.getsiswaperprogram', 'uses' => 'LaporanController@getSiswaPerProgram']);
        Route::get('laporansiswaperprogram/{keywords}',['as' => 'laporan.laporansiswaperprogram', 'uses' => 'LaporanController@laporanSiswaPerprogram']);

        Route::get('siswaperkelas',['as' => 'laporan.siswaperkelas', 'uses' => 'LaporanController@siswaPerKelas']);
        Route::get('getsiswaperkelas',['as' => 'laporan.getsiswaperkelas', 'uses' => 'LaporanController@getSiswaPerKelas']);
        Route::get('laporansiswaperkelas/{keywords}',['as' => 'laporan.laporansiswaperkelas', 'uses' => 'LaporanController@laporanSiswaPerKelas']);

        Route::get('daftarpembayaran',['as' => 'laporan.daftarpembayaran', 'uses' => 'LaporanController@daftarPembayaran']);
        Route::get('getdaftarpembayaran',['as' => 'laporan.getdaftarpembayaran', 'uses' => 'LaporanController@getDaftarPembayaran']);
        Route::post('laporaniurandetailterseleksi',['as' => 'laporan.laporaniurandetailterseleksi', 'uses' => 'LaporanController@laporanIuranDetailTerseleksi']);
        Route::post('laporaniuran',['as' => 'laporan.laporaniuran', 'uses' => 'LaporanController@laporanIuran']);
        Route::post('laporaniurandetail',['as' => 'laporan.laporaniurandetail', 'uses' => 'LaporanController@laporanIuranDetail']);

    });

    Route::resource('grup', 'GrupController', ['except' => 'show']);
    Route::get('grups', 'GrupController@grups');

    Route::resource('pengguna', 'PenggunaController');
    Route::get('penggunas', 'PenggunaController@penggunas');
    Route::get('gantipassword', 'PenggunaController@gantipassword');
    Route::post('change_password', 'PenggunaController@changePassword');
    Route::get('get_select_group', 'PenggunaController@getSelectGroup');

    Route::get('getpenggunaterdaftar', 'SearchController@getPenggunaTerdaftar');
    Route::get('gettotalsiswa', 'SearchController@getTotalSiswa');
    Route::get('gettotalpenunggak', 'SearchController@gettotalpenunggak');
    Route::get('gettotalprodi', 'SearchController@gettotalprodi');
    Route::get('gettotalkelas', 'SearchController@gettotalkelas');



//    Route::get('{name?}', 'DashboardController@showView');
});
