<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => ':attribute harus diterima.',
    'active_url'           => ':attribute bukan URL yang valid.',
    'after'                => ':attribute harus berupa tanggal setelah :date.',
    'alpha'                => ':attribute harus berupa huruf.',
    'alpha_dash'           => ':attribute harus berupa huruf, angka, atau tanda baca.',
    'alpha_num'            => ':attribute harus berisi huruf dan angka.',
    'array'                => ':attribute harus berupa array.',
    'before'               => ':attribute harus tanggal sebelum :date.',
    'between'              => [
        'numeric' => ':attribute harus di antara :min sampai :max.',
        'file'    => ':attribute harus di antara :min sampai :max KB.',
        'string'  => ':attribute harus di antara :min sampai :max karakter.',
        'array'   => ':attribute harus di antara :min sampai :max item.',
    ],
    'boolean'              => 'Inputan :attribute harus benar atau salah.',
    'confirmed'            => 'Konfirmasi :attribute tidak cocok.',
    'date'                 => ':attribute bukan tanggal yang valid.',
    'date_format'          => ':attribute tidak cocok dengan format :format.',
    'different'            => ':attribute dan :other harus berbeda.',
    'digits'               => ':attribute harus :digits digit.',
    'digits_between'       => ':attribute harus di antara :min sampai :max digit.',
    'dimensions'           => ':attribute memiliki dimensi gambar yang tidak valid.',
    'distinct'             => 'Inputan :attribute sudah terdaftar di sistem.',
    'email'                => ':attribute harus berupa email yang valid.',
    'exists'               => 'Pilihan :attribute tidak benar.',
    'file'                 => ':attribute harus berupa file.',
    'filled'               => 'Inputan :attribute harus ada.',
    'image'                => ':attribute harus berupa gambar.',
    'in'                   => ':attribute yang telah diseleksi tidak benar.',
    'in_array'             => 'Inputan :attribute tidak ada di :other.',
    'integer'              => ':attribute harus sebuah bilangan.',
    'ip'                   => ':attribute harus berupa alamat IP yang benar.',
    'json'                 => ':attribute harus berupa pernyataan JSON yang benar.',
    'max'                  => [
        'numeric' => ':attribute tidak boleh lebih dari :max.',
        'file'    => ':attribute tidak boleh lebih dari :max KB.',
        'string'  => ':attribute tidak boleh lebih dari :max karakter.',
        'array'   => ':attribute tidak boleh lebih dari :max item.',
    ],
    'mimes'                => ':attribute harus sebuah file dengan tipe: :values.',
    'min'                  => [
        'numeric' => ':attribute harus paling sedikit :min.',
        'file'    => ':attribute harus paling sedikit :min KB.',
        'string'  => ':attribute harus paling sedikit :min karakter.',
        'array'   => ':attribute harus paling sedikit :min item.',
    ],
    'not_in'               => 'Pilihan :attribute tidak benar.',
    'numeric'              => ':attribute harus berupa angka.',
    'present'              => 'Inputan  :attribute harus ada.',
    'regex'                => 'Format :attribute tidak benar.',
    'required'             => 'Inputan :attribute harus ada.',
    'required_if'          => 'Inputan :attribute harus ada ketika :other bernilai :value.',
    'required_unless'      => 'Inputan :attribute harus ada kecuali :other ada nilai :values.',
    'required_with'        => 'Inputan :attribute harus ada ketika nilai :values diinputkan.',
    'required_with_all'    => 'Inputan :attribute harus ada ketika nilai :values diinputkan.',
    'required_without'     => 'Inputan :attribute harus ada ketika nilai :values tidak diinputkan.',
    'required_without_all' => 'Inputan :attribute harus ada ketika tidak ada nilai :values yang diinputkan.',
    'same'                 => ':attribute dan :other harus sama.',
    'size'                 => [
        'numeric' => ':attribute harus :size.',
        'file'    => ':attribute harus :size KB.',
        'string'  => ':attribute harus :size karakter.',
        'array'   => ':attribute harus berisi :size item.',
    ],
    'string'               => ':attribute harus berupa kalimat.',
    'timezone'             => ':attribute harus berupa zona yang benar.',
    'unique'               => 'Inputang :attribute sudah terdaftar.',
    'url'                  => 'Format :attribute tidak benar.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [],

];
