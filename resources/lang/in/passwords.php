<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Sandi harus minimal enam karakter dan cocok dengan sandi konfirmasi!',
    'reset' => 'Kata sandi anda sudah direset!',
    'sent' => 'Kami telah mengirimi anda sebuah email berisi link untuk mereset password!',
    'token' => 'Token untuk reset password tidak valid.',
    'user' => "Kami tidak dapat menemukan akun yang cocok dengan email ini.",

];
