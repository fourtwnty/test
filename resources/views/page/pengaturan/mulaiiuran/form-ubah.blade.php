<?php
/**
 * Created by PhpStorm.
 * User: ocol
 * Date: 09/11/16
 * Time: 20:48
 */
?>

<div class="form-group">
    {!! Form::label('nisn', 'NISN') !!}
    {!! Form::text('nisn', null, ['class' => 'form-control', 'placeholder' => 'NISN Siswa', 'disabled']) !!}
</div>

<div class="form-group">
    {!! Form::label('nama', 'Nama') !!}
    {!! Form::text('nama', null, ['class' => 'form-control', 'placeholder' => 'Nama Siswa', 'disabled']) !!}
</div>

<div class="form-group">
    {!! Form::label('bulan_id', 'Mulai di Bulan') !!}
    {!!  Form::select('bulan_id', [], null, ['class' => 'form-control select2', 'style'=>"width:100%;"])  !!}
</div>

<div class="form-group">
    {!! Form::label('tahun_id', 'Mulai di Tahun Ajaran') !!}
    {!!  Form::select('tahun_id', [], null, ['class' => 'form-control select2', 'style'=>"width:100%;"])  !!}
</div>
