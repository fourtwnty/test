<?php
/**
 * Created by PhpStorm.
 * User: ocol
 * Date: 09/11/16
 * Time: 14:08
 */
?>

@extends('layouts.master')

@section('title_name')
    Tambah Mulai Iuran
@endsection

@section('active_page')
    <li>Pengaturan</li>
    <li class="active">Mulai Iuran Baru</li>
@endsection

@section('content')
    @include('page.pengaturan.mulaiiuran.modal-cari')

    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-default">
                    <div class="box-header with-border">
                        <h3 class="box-title">Tambah Mulai Iuran Baru</h3>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <input type="hidden" name="token" id="token" value="{{ csrf_token() }}" />

                            <div class="col-lg-12">
                                @include('page.pengaturan.mulaiiuran.form')
                            </div>


                        </div>
                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                        <a href="#" class="btn btn-primary btn-flat" id="simpan"><i class="fa fa-save"></i> Simpan Data</a>
                        <a href="/pengaturan/mulaiiuran" class="btn btn-warning btn-flat" id="batal"><i class="fa fa-close"></i> Batal</a>
                    </div>

                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
@endsection

@section('header_top')
        <!-- DataTables -->
    <link href="/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="/datatables-responsive/dataTables.responsive.css" rel="stylesheet">

    <link rel="stylesheet" href="/theme/plugins/select2/select2.min.css">
@endsection

@section('footer_add')
    <script src="/datatables/js/jquery.dataTables.min.js"></script>
    <script src="/datatables-plugins/dataTables.bootstrap.min.js"></script>
    <script src="/datatables-responsive/dataTables.responsive.js"></script>

    <script src="/theme/plugins/select2/select2.full.min.js"></script>
@endsection

@section('custom_script')
    <script src="/js/page/pengaturan/mulaiiuran/create.js"></script>
@endsection


