<?php
/**
 * Created by PhpStorm.
 * User: ocol
 * Date: 09/11/16
 * Time: 14:08
 */
?>

{!! Form::label('nisn-cari', 'NISN') !!} <br/>
<div class="form-group input-group">
    {!! Form::text('nisn-cari', null, ['class' => 'form-control', 'placeholder' => 'Masukan NISN', 'onkeypress'=>"return validasiAngka(event);"]) !!}
    {{--<input type="text" class="form-control">--}}
    <span class="input-group-btn">
            <a class="btn btn-default" type="button" data-toggle="modal" data-target="#modalCari" id="carisiswa">
                <i class="fa fa-search"></i>
            </a>
        </span>
</div>

{{--<div class="form-group">--}}
{{--{!! Form::label('nisn-cari', 'NISN') !!} <br/>--}}
{{--{!! Form::text('nisn-cari', null, ['class' => 'form-control input-cari-group input-cari', 'placeholder' => 'Masukan NISN', 'onkeypress'=>"return validasiAngka(event);", 'onkeyup'=>"siswamulaikeyup(event);", 'onkeydown'=>"siswamulaikeydown(event);"]) !!}--}}
{{--<a class="btn btn-info pull-right input-cari-group button-cari" data-toggle="modal"--}}
{{--data-target="#modalCari">--}}
{{--Cari Data--}}
{{--</a>--}}
{{--</div>--}}

<div class="form-group">
    {!! Form::label('nama', 'Nama') !!}
    {!! Form::text('nama', null, ['class' => 'form-control hasil-cari', 'placeholder' => 'Nama Siswa', 'disabled']) !!}
</div>

<div class="form-group">
    {!!  Form::hidden('nisn', null, ['class' => 'form-control nisn-asli'])  !!}
</div>

<div class="form-group">
    {!! Form::label('bulan_id', 'Mulai di Bulan') !!}
    {!!  Form::select('bulan_id', [], null, ['class' => 'form-control select2', 'style'=>"width:100%;"])  !!}
</div>

<div class="form-group">
    {!! Form::label('tahun_id', 'Mulai di Tahun Ajaran') !!}
    {!!  Form::select('tahun_id', [], null, ['class' => 'form-control select2', 'style'=>"width:100%;"])  !!}
</div>
