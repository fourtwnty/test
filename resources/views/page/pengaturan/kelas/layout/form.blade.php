<?php
/**
 * Created by PhpStorm.
 * User: ocol
 * Date: 09/11/16
 * Time: 4:17
 */
?>

<div class="form-group">
    {!! Form::label('program_studi', 'Program Studi/Jurusan') !!}
    {!!  Form::select('program_studi', [], null, ['class' => 'form-control select2', 'style'=>"width:100%;"])  !!}
</div>

{{--<div class="form-group">--}}
{{--{!! Form::label('tingkatan', 'Tingkat') !!}--}}
{{--{!!  Form::select('tingkatan', [' ' => 'Pilih Tingkatan Kelas', 'I' => 'I', 'II' => 'II', 'III'=>'III'], ' ', ['class' => 'form-control'])  !!}--}}
{{--</div>--}}

<div class="form-group">
    {!! Form::label('tingkatan', 'Tingkatan Kelas') !!}
    {!! Form::text('tingkatan', null, ['class' => 'form-control', 'placeholder' => 'Masukan tingkatan kelas']) !!}
</div>
