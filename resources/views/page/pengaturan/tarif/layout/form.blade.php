<?php
/**
 * Created by PhpStorm.
 * User: ocol
 * Date: 09/11/16
 * Time: 3:52
 */
?>

<div class="form-group">
    {!! Form::label('program_studi', 'Program Studi/Jurusan') !!}
    {!!  Form::select('program_studi', [], null, ['class' => 'form-control select2', 'style'=>"width:100%;"])  !!}
</div>

<div class="form-group">
    {!! Form::label('nilai', 'Harga Tarif') !!}
    {!! Form::text('nilai', null, ['class' => 'form-control inputanangka', 'placeholder' => 'Masukan Harga Tarif Iuran']) !!}
</div>
