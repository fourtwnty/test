<?php
/**
 * Created by PhpStorm.
 * User: ocol
 * Date: 09/11/16
 * Time: 3:52
 */
?>

@extends('layouts.master')

@section('header_top')
        <!-- Select2 -->
<link rel="stylesheet" href="/theme/plugins/select2/select2.min.css">
@endsection

@section('title_name')
    Tambah Tarif Iuran
@endsection

@section('active_page')
    <li>Pengaturan</li>
    <li class="active">Tarif Iuran Baru</li>
@endsection

@section('content')
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-default">
                    <div class="box-header with-border">
                        <h3 class="box-title">Tambah Tarif Iuran Baru</h3>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <input type="hidden" name="token" id="token" value="{{ csrf_token() }}" />
                                @include('page.pengaturan.tarif.layout.form')
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                        {{--{!! link_to('#', $title='Simpan Data', $attributes=['id'=>'simpan', 'class'=>'btn btn-success']) !!}--}}
                        {{--{!! link_to('/pengaturan/tarif', $title='Batal', $attributes=['id'=>'batal', 'class'=>'btn btn-warning']) !!}--}}

                        <a href="#" class="btn btn-primary btn-flat" id="simpan"><i class="fa fa-save"></i> Simpan Data</a>
                        {{--                        {!! link_to('#', $title='Simpan Data', $attributes=['id'=>'simpan', 'class'=>'btn btn-success']) !!}--}}
                        <a href="/pengaturan/tarif" class="btn btn-warning btn-flat" id="batal"><i class="fa fa-close"></i> Batal</a>
                    </div>

                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
@endsection

@section('footer_add')
        <!-- Select2 -->
    <script src="/theme/plugins/select2/select2.full.min.js"></script>
@endsection

@section('custom_script')
    <script src="/js/page/pengaturan/tarif/create.js"></script>
@endsection



