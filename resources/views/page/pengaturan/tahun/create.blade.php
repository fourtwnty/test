<?php
/**
 * Created by PhpStorm.
 * User: ocol
 * Date: 09/11/16
 * Time: 3:28
 */
?>

@extends('layouts.master')

@section('header_top')
    <link href="/bootstrap-datepicker-master/bootstrap-datepicker3.css" rel="stylesheet">
@endsection

@section('title_name')
    Tambah Tahun Ajaran
@endsection

@section('active_page')
    <li>Pengaturan</li>
    <li class="active">Tahun Ajaran Baru</li>
@endsection

@section('content')
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-default">
                    <div class="box-header with-border">
                        <h3 class="box-title">Tambah Tahun Ajaran Baru</h3>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <input type="hidden" name="token" id="token" value="{{ csrf_token() }}" />

                            {{--<div class="col-md-12">--}}
                                {{--<div class="form-group">--}}
                                {{--<label>Nama Tahun Ajaran</label>--}}

                                {{--<div class="input-group">--}}
                                {{--<div class="input-group-addon">--}}
                                {{--<i class="fa fa-calendar"></i>--}}
                                {{--</div>--}}
                                {{--<input type="text" class="form-control pull-right" id="reservation">--}}
                                {{--</div>--}}
                                {{--<!-- /.input group -->--}}
                                {{--</div>--}}
                            {{--</div>--}}

                            <div class="col-md-12">
                            {!! Form::label('start', 'Nama Tahun Ajaran') !!}
                            <div class="input-daterange input-group form-group" id="datepicker">
                                {!! Form::text('start', null, ['class'=>'form-control', 'id' => 'start', 'placeholder'=> 'YYYY']) !!}
                                <span class="input-group-addon">/</span>
                                {!! Form::text('end', null, ['class'=>'form-control', 'id'=>'end', 'placeholder'=> 'YYYY']) !!}
                            </div>
                            </div>


                        </div>
                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                        <a href="#" class="btn btn-primary btn-flat" id="simpan"><i class="fa fa-save"></i> Simpan Data</a>
                        <a href="/pengaturan/tahun" class="btn btn-warning btn-flat" id="batal"><i class="fa fa-close"></i> Batal</a>

                        {{--{!! link_to('#', $title='Simpan Data', $attributes=['id'=>'simpan', 'class'=>'btn btn-success']) !!}--}}
                        {{--{!! link_to('/pengaturan/tahun', $title='Batal', $attributes=['id'=>'batal', 'class'=>'btn btn-warning']) !!}--}}
                    </div>

                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
@endsection

@section('footer_add')
    <script src="/bootstrap-datepicker-master/bootstrap-datepicker.min.js"></script>
@endsection

@section('custom_script')
    <script src="/js/page/pengaturan/tahun/create.js"></script>
@endsection


