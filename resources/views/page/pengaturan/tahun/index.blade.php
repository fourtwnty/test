<?php
/**
 * Created by PhpStorm.
 * User: ocol
 * Date: 09/11/16
 * Time: 3:28
 */
?>

<?php
/**
 * Created by PhpStorm.
 * User: ocol
 * Date: 09/11/16
 * Time: 2:59
 */
?>

@extends('layouts.master')

@section('header_top')
        <!-- DataTables -->
<link href="/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">

<!-- DataTables Responsive CSS -->
<link href="/datatables-responsive/dataTables.responsive.css" rel="stylesheet">

<link href="/bootstrap-datepicker-master/bootstrap-datepicker3.css" rel="stylesheet">
@endsection

@section('title_name')
    Tahun Ajaran
@endsection

@section('active_page')
    <li>Pengaturan</li>
    <li class="active">Tahun Ajaran</li>
@endsection

@section('content')
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-default">
                    <div class="box-header with-border">
                        <h3 class="box-title">Daftar Tahun Ajaran</h3>
                    </div>

                    @include('page.pengaturan.tahun.modal')
                            <!-- /.box-header -->

                    @if(Auth::user()->can('tambah_tahun'))
                    <div class="col-md-12 daftar-tombol">
                        <a href="/pengaturan/tahun/create" class="btn btn-primary btn-flat"><i class="fa fa-plus"></i> Data Baru</a>
                            {{--<a href="/pengaturan/tahun/create" class="btn btn-primary pull-right">Data Baru</a>--}}
                    </div>
                    @endif

                    <div class="box-body">



                        <table width="100%" class="table table-striped table-bordered table-hover" id="dataTableBuilder">
                            <thead>
                            <tr>
                                <td>Nama</td>
                                <td>Aksi</td>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>

                    </div>
                    <!-- /.box-body -->
                    @if(Auth::user()->can('tambah_tahun'))
                        <div class="box-footer">
                            <a href="/pengaturan/tahun/create" class="btn btn-primary btn-flat"><i class="fa fa-plus"></i> Data Baru</a>
                        </div>
                    @endif

                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
@endsection

@section('footer_add')
    <script src="/datatables/js/jquery.dataTables.min.js"></script>
    <script src="/datatables-plugins/dataTables.bootstrap.min.js"></script>
    <script src="/datatables-responsive/dataTables.responsive.js"></script>

    <script src="/bootstrap-datepicker-master/bootstrap-datepicker.min.js"></script>
@endsection

@section('custom_script')
    <script src="/js/page/pengaturan/tahun/index.js"></script>
@endsection


