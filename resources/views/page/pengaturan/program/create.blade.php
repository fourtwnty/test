<?php
/**
 * Created by PhpStorm.
 * User: ocol
 * Date: 09/11/16
 * Time: 3:16
 */
?>

@extends('layouts.master')

@section('header_add')
@endsection

@section('title_name')
    Tambah Program Studi
@endsection

@section('active_page')
    <li>Pengaturan</li>
    <li class="active">Program Studi Baru</li>
@endsection

@section('content')
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-default">
                    <div class="box-header with-border">
                        <h3 class="box-title">Tambah Program Studi Baru</h3>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <input type="hidden" name="token" id="token" value="{{ csrf_token() }}" />

                            <div class="col-md-12">

                                <div class="form-group">
                                    {!! Form::label('name', 'Nama Program Studi / Jurusan') !!}
                                    {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Masukan Nama Program Studi / Jurusan']) !!}
                                </div>
                            </div>


                        </div>
                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                        <a href="#" class="btn btn-primary btn-flat" id="simpan"><i class="fa fa-save"></i> Simpan Data</a>
                        <a href="/pengaturan/program" class="btn btn-warning btn-flat" id="batal"><i class="fa fa-close"></i> Batal</a>
                    </div>

                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
@endsection

@section('footer_add')

@endsection

@section('custom_script')
    <script src="/js/page/pengaturan/program/create.js"></script>
@endsection

