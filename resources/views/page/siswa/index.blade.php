<?php
/**
 * Created by PhpStorm.
 * User: ocol
 * Date: 09/11/16
 * Time: 13:35
 */
?>
@extends('layouts.master')

@section('title_name')
    Siswa
@endsection

@section('active_page')
    <li class="active">Siswa</li>
@endsection

@section('content')
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-default">
                    <div class="box-header with-border">
                        <h3 class="box-title">Daftar Siswa</h3>
                    </div>

                    @include('page.siswa.modal')
                            <!-- /.box-header -->
                    @if(Auth::user()->can('tambah_siswa'))
                        <div class="col-md-12 daftar-tombol">
                            <a href="/siswa/create" class="btn btn-primary btn-flat"><i class="fa fa-plus"></i> Data Baru</a>
                            {{--<a href="/pengaturan/program/create" class="btn btn-primary pull-right">Data Baru</a>--}}
                        </div>
                    @endif

                    <div class="box-body">

                        <table width="100%" class="table table-striped table-bordered table-hover" id="dataTableBuilder">
                            <thead>
                            <tr>
                                <td>NISN</td>
                                <td>Nama</td>
                                <td>JK</td>
                                <td>Kelas</td>
                                <td>Status</td>
                                <td>Aksi</td>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>

                    </div>
                    <!-- /.box-body -->
                    @if(Auth::user()->can('tambah_siswa'))
                        <div class="box-footer">
                            <a href="/siswa/create" class="btn btn-primary btn-flat"><i class="fa fa-plus"></i> Data Baru</a>
                        </div>
                    @endif

                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
@endsection

@section('header_top')
        <!-- DataTables -->
    <link href="/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="/datatables-responsive/dataTables.responsive.css" rel="stylesheet">

    <link href="/bootstrap-datepicker-master/bootstrap-datepicker3.css" rel="stylesheet">
    <link rel="stylesheet" href="/theme/plugins/select2/select2.min.css">
@endsection

@section('footer_add')
    <script src="/datatables/js/jquery.dataTables.min.js"></script>
    <script src="/datatables-plugins/dataTables.bootstrap.min.js"></script>
    <script src="/datatables-responsive/dataTables.responsive.js"></script>

    <script src="/bootstrap-datepicker-master/bootstrap-datepicker.min.js"></script>
    <script src="/theme/plugins/select2/select2.full.min.js"></script>
@endsection

@section('custom_script')
    <script src="/js/page/siswa/index.js"></script>
@endsection


