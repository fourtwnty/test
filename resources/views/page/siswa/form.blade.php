<?php
/**
 * Created by PhpStorm.
 * User: ocol
 * Date: 09/11/16
 * Time: 13:35
 */
?>

<div class="col-lg-6">
    <div class="form-group">
        {!! Form::label('nisn', 'NISN') !!}
        {!! Form::text('nisn', null, ['class' => 'form-control', 'placeholder' => 'Masukan NISN Siswa','onkeypress'=>"return validasiAngka(event);"]) !!}
    </div>

    <div class="form-group">
        {!! Form::label('nama', 'Nama') !!}
        {!! Form::text('nama', null, ['class' => 'form-control', 'placeholder' => 'Masukan Nama Siswa', 'onkeypress'=>"return validasihuruf(event);"]) !!}
    </div>

    <div class="form-group">
        {!! Form::label('jk', 'Jenis Kelamin') !!}
        {!!  Form::select('jk', [' ' => 'Pilih Jenis Kelamin', 'L' => 'Laki - Laki', 'P' => 'Perempuan'], ' ', ['class' => 'form-control select2', 'style'=>"width:100%;"])  !!}
        {{--    {!!  Form::select('jk', [' ' => 'Pilih Jenis Kelamin', 'L' => 'Laki - Laki', 'P' => 'Perempuan'], ' ', ['class' => 'form-control select2', 'style'=>'width: 100%;'])  !!}--}}
    </div>

    <div class="form-group">
        {!! Form::label('tempat_lahir', 'Tempat Kelahiran') !!}
        {!! Form::text('tempat_lahir', null, ['class' => 'form-control', 'placeholder' => 'Masukan Tempat Kelahiran']) !!}
    </div>
</div>
<div class="col-lg-6">
    <div class="form-group">
        {!! Form::label('tgl_lahir', 'Tanggal Kelahiran') !!}
        {!! Form::text('tgl_lahir', null, ['class' => 'form-control tgllahir', 'placeholder' => 'MM/DD/YYYY']) !!}
    </div>

    <div class="form-group">
        {!! Form::label('kelas_id', 'Kelas Siswa') !!}
        {!!  Form::select('kelas_id', [], null, ['class' => 'form-control select2', 'style'=>'width: 100%;'])  !!}
    </div>

    <div class="form-group">
        {!! Form::label('aktif', 'Status Siswa') !!}
        {!!  Form::select('aktif', [' ' => 'Pilih Status Siswa', '0' => 'Tidak Aktif', '1' => 'Aktif'], ' ', ['class' => 'form-control select2', 'style'=>'width: 100%;'])  !!}
    </div>

    <div class="form-group">
        {!! Form::label('alamat', 'Alamat Siswa') !!}
        {!! Form::textarea('alamat',null,['class'=>'form-control text-alamat', 'placeholder'=>'Masukan Alamat']) !!}
    </div>
</div>
