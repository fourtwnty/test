<?php
/**
 * Created by PhpStorm.
 * User: ocol
 * Date: 09/11/16
 * Time: 13:35
 */
?>

<?php
/**
 * Created by PhpStorm.
 * User: ocol
 * Date: 09/11/16
 * Time: 3:16
 */
?>
@extends('layouts.master')

@section('title_name')
    Tambah Siswa
@endsection

@section('active_page')
    <li class="active">Siswa Baru</li>
@endsection

@section('content')
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-default">
                    <div class="box-header with-border">
                        <h3 class="box-title">Tambah Siswa Baru</h3>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <input type="hidden" name="token" id="token" value="{{ csrf_token() }}" />

                            <div class="col-lg-12">
                                @include('page.siswa.form')
                            </div>


                        </div>
                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                        <a href="#" class="btn btn-primary btn-flat" id="simpan"><i class="fa fa-save"></i> Simpan Data</a>
                        <a href="/siswa" class="btn btn-warning btn-flat" id="batal"><i class="fa fa-close"></i> Batal</a>
                    </div>

                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
@endsection

@section('header_top')
    <link href="/bootstrap-datepicker-master/bootstrap-datepicker3.css" rel="stylesheet">
    <link rel="stylesheet" href="/theme/plugins/select2/select2.min.css">
@endsection

@section('footer_add')
    <script src="/bootstrap-datepicker-master/bootstrap-datepicker.min.js"></script>
    <script src="/theme/plugins/select2/select2.full.min.js"></script>
@endsection

@section('custom_script')
    <script src="/js/page/siswa/create.js"></script>
@endsection


