<?php
/**
 * Created by PhpStorm.
 * User: ocol
 * Date: 10/11/16
 * Time: 3:13
 */
?>

<div class="col-md-6">
    <div class="form-group">
        {!! Form::label('username', 'Nama User') !!}
        {!! Form::text('username', null, ['class' => 'form-control', 'placeholder' => 'Masukan Nama Pengguna']) !!}
    </div>

    <div class="form-group">
        {!! Form::label('email', 'Email') !!}
        {!! Form::text('email', null, ['class' => 'form-control', 'placeholder' => 'Masukan Alamat Email']) !!}
    </div>

    <div class="form-group">
        {!! Form::label('role', 'Grup') !!}
        {!!  Form::select('role', [], null, ['class' => 'form-control select2', 'style'=>"width: 100%;"])  !!}
    </div>

    <div class="form-group">
        {!! Form::label('fullname', 'Nama Lengkap') !!}
        {!! Form::text('fullname', null, ['class' => 'form-control', 'placeholder' => 'Masukan Nama Lengkap']) !!}
    </div>



    <div class="checkbox">
        <label>
            <input type="checkbox" name="ubahkatasandi" id="ubahkatasandi" value="" checked><strong class="f_warning">Mengubah Kata Sandi</strong>
        </label>
    </div>

    <div id="divSandi" class="form-group hidden">
        {!! Form::label('password', 'Kata Sandi Baru') !!}
        {!! Form::password('password', ['class' => 'form-control', 'placeholder' => 'Masukan Kata Sandi Baru']) !!}
    </div>

    <div id="divValidasiSandi" class="form-group hidden">
        {!! Form::label('validate_password', 'Validasi Kata Sandi Baru') !!}
        {!! Form::password('validate_password', ['class' => 'form-control', 'placeholder' => 'Masukan Validasi Kata Sandi Baru']) !!}
    </div>

    <div class="form-group">
        {!! Form::label('address', 'Alamat') !!}
        {!! Form::textarea('address', null, ['class' => 'form-control', 'placeholder' => 'Masukan Alamat']) !!}
    </div>
</div>
<div class="col-md-6">
    <div class="form-group">
        {!! Form::label('gsm', 'No. HP') !!}
        {!! Form::text('gsm', null, ['class' => 'form-control', 'placeholder' => 'Masukan No. HP', 'onkeypress' => 'return validasiAngka(event);']) !!}
    </div>

    <div class="form-group">
        {!! Form::label('phone', 'No. Telepon') !!}
        {!! Form::text('phone', null, ['class' => 'form-control', 'placeholder' => 'Masukan No. Telepon', 'onkeypress' => 'return validasiAngka(event);']) !!}
    </div>

    <div class="form-group">
        {!! Form::label('gender', 'Jenis Kelamin') !!}
        {!!  Form::select('gender', ['L'=>'Laki - Laki', 'P' => 'Perempuan'], 'L', ['class' => 'form-control select2', 'style'=>"width: 100%;"])  !!}
    </div>

    <div class="form-group">
        {!! Form::label('description', 'Deskripsi Tambahan') !!}
        {!! Form::textarea('description', null, ['class' => 'form-control', 'placeholder' => 'Masukan Deksipri Tambahan']) !!}
    </div>

    <div class="checkbox">
        <label>
            <input type="checkbox" name="active" id="active" value="">Aktif
        </label>
    </div>
</div>
