<?php
/**
 * Created by PhpStorm.
 * User: ocol
 * Date: 10/11/16
 * Time: 3:13
 */
?>

@extends('layouts.master')


@section('title_name')
    Ganti Password
@endsection

@section('active_page')
    <li class="active">Ganti Password</li>
@endsection

@section('content')
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-default">
                    <div class="box-header with-border">
                        <h3 class="box-title">Ganti Password User</h3>
                    </div>

                    <div class="box-body">

                        <div class="row">
                            <input type="hidden" name="token" id="token" value="{{ csrf_token() }}" />

                            <div class="col-md-12">
                                <div class="form-group">
                                    {!! Form::label('password-lama', 'Kata Sandi Lama') !!}
                                    {!! Form::password('password-lama', ['class' => 'form-control', 'placeholder' => 'Masukan Kata Sandi Saat Ini']) !!}
                                </div>

                                <div class="form-group">
                                    {!! Form::label('password-baru', 'Kata Sandi Baru') !!}
                                    {!! Form::password('password-baru', ['class' => 'form-control', 'placeholder' => 'Ketikkan Kata Sandi Baru']) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::label('validate_password', 'Validasi Kata Sandi Baru') !!}
                                    {!! Form::password('validate_password', ['class' => 'form-control', 'placeholder' => 'Ketikkan Kata Sandi Baru Sekali Lagi']) !!}
                                </div>
                            </div>


                        </div>
                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                        <a href="#" class="btn btn-primary btn-flat" id="simpan"><i class="fa fa-save"></i> Ganti Password</a>
                        <a href="/" class="btn btn-warning btn-flat" id="batal"><i class="fa fa-close"></i> Batal</a>
                    </div>

                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
@endsection

@section('footer_add')

@endsection

@section('custom_script')
    <script src="/js/pengguna/password.js"></script>
@endsection

