<?php
/**
 * Created by PhpStorm.
 * User: ocol
 * Date: 10/11/16
 * Time: 3:14
 */
?>

<div class="col-md-6">
    <div class="form-group">
        {!! Form::label('username-lihat', 'Nama User') !!}
        {!! Form::text('username-lihat', null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group">
        {!! Form::label('email-lihat', 'Email') !!}
        {!! Form::text('email-lihat', null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group">
        {!! Form::label('role-lihat', 'Grup User') !!}
        {!! Form::text('role-lihat', null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group">
        {!! Form::label('fullname-lihat', 'Nama Lengkap') !!}
        {!! Form::text('fullname-lihat', null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group">
        {!! Form::label('address-lihat', 'Alamat') !!}
        {!! Form::textarea('address-lihat', null, ['class' => 'form-control']) !!}
    </div>
</div>

<div class="col-md-6">
    <div class="form-group">
        {!! Form::label('gsm-lihat', 'No. HP') !!}
        {!! Form::text('gsm-lihat', null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group">
        {!! Form::label('phone-lihat', 'No. Telepon') !!}
        {!! Form::text('phone-lihat', null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group">
        {!! Form::label('gender-lihat', 'Jenis Kelamin') !!}
        {!! Form::text('gender-lihat', null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group">
        {!! Form::label('active-lihat', 'Status Aktif') !!}
        {!! Form::text('active-lihat', null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group">
        {!! Form::label('description-lihat', 'Deskripsi Tambahan') !!}
        {!! Form::textarea('description-lihat', null, ['class' => 'form-control']) !!}
    </div>
</div>



