<?php
/**
 * Created by PhpStorm.
 * User: ocol
 * Date: 10/11/16
 * Time: 2:10
 */
?>

@extends('layouts.master')

@section('title_name')
    Laporan Daftar Siswa
@endsection

@section('active_page')
    <li>Laporan</li>
    <li class="active">Daftar Siswa</li>
@endsection

@section('content')
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-default">
                    <div class="box-header with-border">
                        <h3 class="box-title">Laporan Daftar Siswa</h3>
                    </div>
                    <!-- /.box-header -->

                        <div class="col-md-12 daftar-tombol">
                                <a href="#" class="btn btn-primary btn-flat" id="cetakindividu"><i class="fa fa-print"></i> Cetak Individu</a>
                                <a href="#" class="btn btn-info btn-flat" id="cetakdaftar"><i class="fa fa-print"></i> Cetak Daftar Siswa</a>
                        </div>


                    <div class="box-body">
                        <table width="100%" class="table table-striped table-bordered table-hover" id="dataTableBuilder">
                            <thead>
                            <tr>
                                <th class="col-md-1 text-center"><input name="select_all" value="1" type="checkbox"></th>
                                <th class="col-md-2">NISN</th>
                                <th class="col-md-4">Nama</th>
                                <th class="col-md-2">JK</th>
                                <th class="col-md-3">Kelas</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>

                    </div>
                    <!-- /.box-body -->

                        <div class="box-footer">
                            <a href="#" class="btn btn-primary btn-flat" id="cetakindividubawah"><i class="fa fa-print"></i> Cetak Individu</a>
                            <a href="#" class="btn btn-info btn-flat" id="cetakdaftarbawah"><i class="fa fa-print"></i> Cetak Daftar Siswa</a>
                        </div>


                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    @endsection

    @section('header_top')
            <!-- DataTables -->
    <link href="/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="/datatables-responsive/dataTables.responsive.css" rel="stylesheet">
@endsection

@section('footer_add')
    <script src="/datatables/js/jquery.dataTables.min.js"></script>
    <script src="/datatables-plugins/dataTables.bootstrap.min.js"></script>
    <script src="/datatables-responsive/dataTables.responsive.js"></script>
@endsection

@section('custom_script')
    <script src="/js/laporan/daftar-siswa.js"></script>
@endsection



