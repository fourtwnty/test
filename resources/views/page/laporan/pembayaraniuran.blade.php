<?php
/**
 * Created by PhpStorm.
 * User: ocol
 * Date: 10/11/16
 * Time: 1:32
 */
?>


@extends('layouts.master')

@section('title_name')
    Cetak Laporan Pembayaran Iuran
@endsection

@section('active_page')
    <li>Laporan</li>
    <li class="active">Laporan Pembayaran Iuran</li>
@endsection

@section('content')
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-default">
                    <div class="box-header with-border">
                        <h3 class="box-title">Data Pembayaran Iuran Komite Siswa</h3>
                    </div>
                    <!-- /.box-header -->

                        <div class="col-md-12 daftar-tombol">
                            {!! Form::open(['url' => 'laporan/laporaniurandetailterseleksi', 'class'=>'pull-right', 'method'=>'POST', 'id' => 'formCetakLaporanDetailTerseleksi', 'style'=>"margin-right:10px;"]) !!}
                            <button type="submit" class="btn btn-primary btn-flat"><i class="fa fa-print"></i>
                                Cetak Laporan Detail Yang Terseleksi
                            </button>
                            {!! Form::close() !!}

                                {{--{!! Form::open(['url' => 'laporan/laporandaftarpenunggak', 'class' => 'pull-right', 'id' => 'formCetakDaftarPenunggak','style' => "margin-right: 5px;"]) !!}--}}

                                {!! Form::open(['url' => 'laporan/laporaniurandetail', 'class'=>'pull-right', 'method'=>'POST', 'id' => 'formCetakLaporanDetail', 'style'=>"margin-right:5px;"]) !!}
                                <button type="submit" class="btn btn-info btn-flat"><i class="fa fa-print"></i>
                                    Cetak Laporan Detail
                                </button>
                                {!! Form::close() !!}

                                {!! Form::open(['url' => 'laporan/laporaniuran', 'class'=>'pull-right', 'method'=>'POST', 'id' => 'formCetakLaporan', 'style'=>"margin-right:5px;"]) !!}
                                <button type="submit" class="btn btn-success btn-flat"><i class="fa fa-print"></i>
                                    Cetak Laporan
                                </button>
                                {!! Form::close() !!}
                        </div>


                    <div class="box-body">
                        <div class="col-lg-6">
                        <div class="form-group">
                            {!! Form::label('tahun_id', 'Tahun Ajaran') !!}
                            {!!  Form::select('tahun_id', [], null, ['class' => 'form-control select2', 'style'=>"width:100%;"])  !!}
                        </div>
                        </div>

                        <div class="col-lg-6">

                        {!! Form::label('start', 'Tanggal Pembayaran Iuran Komite') !!}
                        <div class="input-daterange input-group form-group" id="datepicker">
                            {!! Form::text('start', null, ['class'=>'form-control', 'id' => 'start', 'placeholder'=> 'DD/MM/YYYY']) !!}
                            <span class="input-group-addon">s/d</span>
                            {!! Form::text('end', null, ['class'=>'form-control', 'id'=>'end', 'placeholder'=> 'DD/MM/YYYY']) !!}
                        </div>
                        </div>

                        <table width="100%" class="table table-striped table-bordered table-hover" id="dataTableBuilder">
                            <thead>
                            <tr>
                                <th class="text-center col-md-1"><input name="select_all" value="1" type="checkbox"></th>
                                <th class="col-md-1">Kode</th>
                                <th class="col-md-1">Tgl.</th>
                                <th class="col-md-1">NISN</th>
                                <th class="col-md-3">Nama</th>
                                <th class="col-md-1">Total</th>
                                <th class="col-md-4">Keterangan</th>
                            </tr>
                            </thead>
                            <tfoot>
                            <tr>
                                <th colspan="6" style="text-align:right; font-size:18px; color: #9d1500;"></th>
                                <th></th>
                            </tr>
                            </tfoot>
                            <tbody>

                            </tbody>
                        </table>

                    </div>

                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    @endsection

    @section('header_top')
            <!-- DataTables -->
    <link href="/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="/datatables-responsive/dataTables.responsive.css" rel="stylesheet">
    <link href="/bootstrap-datepicker-master/bootstrap-datepicker3.css" rel="stylesheet">

    <link rel="stylesheet" href="/theme/plugins/select2/select2.min.css">
@endsection

@section('footer_add')
    <script src="/datatables/js/jquery.dataTables.min.js"></script>
    <script src="/datatables-plugins/dataTables.bootstrap.min.js"></script>
    <script src="/datatables-responsive/dataTables.responsive.js"></script>

    <script src="/bootstrap-datepicker-master/bootstrap-datepicker.min.js"></script>
    <script src="/theme/plugins/select2/select2.full.min.js"></script>
@endsection

@section('custom_script')
    <script src="/js/laporan/pembayaraniuran.js"></script>
@endsection



