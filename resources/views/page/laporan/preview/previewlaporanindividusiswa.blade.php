<!DOCTYPE html>
<html lang="in">
<head>
    <meta charset="utf-8">

    <title>Laporan Data Individu Siswa</title>
</head>

<body>

<style type="text/css">

    .tg {
        border-collapse: collapse;
        border-spacing: 0;
        border-color: #ccc;
        width: 100%;
    }

    .tg td {
        font-family: Arial;
        font-size: 12px;
        padding: 10px 5px;
        border-style: solid;
        border-width: 1px;
        overflow: hidden;
        word-break: normal;
        border-color: #ccc;
        color: #333;
        background-color: #fff;
    }

    .tg th {
        font-family: Arial;
        font-size: 14px;
        font-weight: normal;
        padding: 10px 5px;
        border-style: solid;
        border-width: 1px;
        overflow: hidden;
        word-break: normal;
        border-color: #ccc;
        color: #333;
        background-color: #f0f0f0;
    }

    .tg .tg-3wr7 {
        font-weight: bold;
        font-size: 12px;
        font-family: "Arial", Helvetica, sans-serif !important;;
        text-align: center
    }

    .tg .tg-ti5e {
        font-size: 10px;
        font-family: "Arial", Helvetica, sans-serif !important;;
        text-align: center
    }

    .tg .tg-rv4w {
        font-size: 10px;
        font-family: "Arial", Helvetica, sans-serif !important;
    }

    body {
        font-family: sans-serif;
        font-size: 10pt;
        margin: 0.5cm 0;
        text-align: justify;
    }

    #title {
        width: 100%;
        text-align: center;
        margin-top: 0px;
        padding: 0px;
    }

    #title .title-header {
        text-align: center;
        margin-top: 0px;
        font-weight: bold;
        font-size: 12pt;
    }

    #title .title-header-2 {
        text-align: center;
        margin-top: 0px;
        font-weight: bold;
        font-size: 13pt;
    }

    #title .title-header-3 {
        text-align: center;
        margin-top: 0px;
        font-weight: bold;
        font-size: 20px;
    }

    #title .title-header-4 {
        text-align: center;
        margin-top: 0px;
        font-weight: normal;
        font-size: 10px;
        font-style: italic;
    }

    #header .judul_laporan {
        text-align: center;
        margin-top: 0px;
        font-weight: bold;
        font-size: 18px;
    }

    #header .subjudul_laporan {
        text-align: center;
        margin-top: 0px;
        font-weight: bold;
        font-size: 14px;
    }

    #footer {
        position: fixed;
        left: 0;
        right: 0;
        color: #aaa;
        font-size: 0.9em;
    }

    #footer {
        bottom: 0;
        border-top: 0.1pt solid #aaa;
    }

    #footer table {
        width: 100%;
        border-collapse: collapse;
        border: none;
    }

    #footer td {
        padding: 0;
        width: 50%;
    }

    #diatas {
        position: fixed;
    }

    #dataTableBuilder {
        /*position: fixed;*/
        /*top: 200px;*/
        margin-top: 500px;
    }

    .page-number {
        text-align: right;
    }

    .page-number:before {
        content: "Halaman " counter(page);
    }

    hr {
        page-break-after: always;
        border: 0;
    }

    .garis-dua {
        border-top: 0.1pt solid #aaa;
    }

    .garis-satu {
        border-top: 3pt solid #000;
    }

    .logo-img {
        top: -10px;
    }

    .tg-transparent {
        border: none;
        background: transparent;
    }

    .tg-transparent > th {
        border: none;
        background: transparent;
    }

    .tg-transparent > td {
        border: none;
        background: transparent;
    }

    .tg .tg-3wr7 {
        font-weight: bold;
        font-size: 15px;
        font-family: "Arial", Helvetica, sans-serif !important;;
        text-align: left;
        width: 35%;
        padding: 10px;
    }

    .tg .tg-1wr7 {
        font-weight: bold;
        font-size: 15px;
        font-family: "Arial", Helvetica, sans-serif !important;;
        text-align: left;
        width: 5%;
        padding: 10px;
    }

    .tg .tg-rv4w {
        font-weight: normal;
        font-size: 14px;
        font-family: "Arial", Helvetica, sans-serif !important;;
        text-align: left;
        width: 60%;
        padding: 10px;
    }

</style>

<div id="diatas">
    <div id="title">
        <img class="logo-img" style="position: fixed; float: left; width: 85px;"
             src="{{ $srcimage }}"/>
        <table width="100%" cellpadding="0">
            <tr>
                <td class="title-header">{{ $namadinas }}</td>
            </tr>
            <tr>
                <td class="title-header-2">{{ $namakabupaten }}</td>
            </tr>
            <tr>
                <td class="title-header-3">{{ strtoupper($namasekolah) }}</td>
            </tr>
            <tr>
                <td class="title-header-4">{{ $namaalamat }}</td>
            </tr>
        </table>
        {{--<img style="position: fixed; margin-left: auto; margin-right: auto; float: right; width: 150px;" src="img/tutwuri.png" />--}}

        <table width="100%" style="margin-top: 10px">
            <tr>
                <td class="garis-satu"></td>
            </tr>
            <tr>
                <td class="garis-dua"></td>
            </tr>
        </table>
    </div>

    <div id="header">
        <table width="100%" style="margin-top: 10px">
            <tr>
                <td class="judul_laporan">LAPORAN DATA INDIVIDU SISWA</td>
            </tr>
            <tr>
                <td class="subjudul_laporan">Per Periode {{ date('d F Y') }}</td>
            </tr>
        </table>
    </div>
</div>

<div id="footer">
    <table>
        <tr>
            <td>Sistem Informasi Pembayaran Iuran Komite</td>
        </tr>
    </table>
</div>

@foreach($siswa as $value)
    <table id="dataTableBuilder" class="table table-striped table-bordered table-hover display select tg"
           cellspacing="2" width="100%" style="margin-top: 180px;">
        <thead>
        <tr class="tg-transparent">
            <th class="tg-3wr7"><strong>NISN</strong></th>
            <th class="tg-1wr7"><strong>:</strong></th>
            <td class="tg-rv4w">{{ $value->nisn }}</td>
        </tr>
        <tr class="tg-transparent">
            <th class="tg-3wr7"><strong>NAMA</strong></th>
            <th class="tg-1wr7"><strong>:</strong></th>
            <td class="tg-rv4w">{{ $value->nama }}</td>
        </tr>
        <tr class="tg-transparent">
            <th class="tg-3wr7"><strong>JENIS KELAMIN</strong></th>
            <th class="tg-1wr7"><strong>:</strong></th>
            <td class="tg-rv4w">@if($value->jk) Laki - Laki @else Perempuan @endif</td>
        </tr>
        <tr class="tg-transparent">
            <th class="tg-3wr7"><strong>TEMPAT, TGL. LAHIR</strong></th>
            <th class="tg-1wr7"><strong>:</strong></th>
            <td class="tg-rv4w">{{ $value->tempat_lahir.', '.$value->tgl_lahir->format('d F Y') }}</td>
        </tr>
        <tr class="tg-transparent">
            <th class="tg-3wr7"><strong>ALAMAT</strong></th>
            <th class="tg-1wr7"><strong>:</strong></th>
            <td class="tg-rv4w">{{ $value->alamat }}</td>
        </tr>
        <tr class="tg-transparent">
            <th class="tg-3wr7"><strong>PROGRAM STUDI/KELAS</strong></th>
            <th class="tg-1wr7"><strong>:</strong></th>
            <td class="tg-rv4w">{{ $value->kelas_name }}</td>
        </tr>

        </thead>
    </table>
    <hr>
@endforeach

</body>
</html>