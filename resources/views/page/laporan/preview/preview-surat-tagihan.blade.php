<?php
/**
 * Created by PhpStorm.
 * User: ocol
 * Date: 10/11/16
 * Time: 1:16
 */
?>

        <!DOCTYPE html>
<html lang="in">
<head>
    <meta charset="utf-8">

    <title>Surat Tagihan Pembayaran Iuran Komite</title>
</head>

<body>

<style type="text/css">

    .tg {
        border-collapse: collapse;
        border-spacing: 0;
        border-color: #ccc;
        width: 100%;
    }

    .tg td {
        font-family: Arial;
        font-size: 12px;
        /*padding: 10px 5px;*/
        border-style: solid;
        border-width: 1px;
        overflow: hidden;
        word-break: normal;
        border-color: #ccc;
        color: #333;
        background-color: #fff;
    }

    .tg th {
        font-family: Arial;
        font-size: 14px;
        font-weight: normal;
        /*padding: 10px 5px;*/
        border-style: solid;
        border-width: 1px;
        overflow: hidden;
        word-break: normal;
        border-color: #ccc;
        color: #333;
        background-color: #f0f0f0;
    }

    .tg .tg-3wr7 {
        font-weight: bold;
        font-size: 12px;
        font-family: "Arial", Helvetica, sans-serif !important;;
        text-align: center
    }

    .tg .tg-ti5e {
        font-size: 10px;
        font-family: "Arial", Helvetica, sans-serif !important;;
        text-align: center
    }

    .tg .tg-rv4w {
        font-size: 10px;
        font-family: "Arial", Helvetica, sans-serif !important;
    }

    body {
        /*font-family: sans-serif;*/
        /*font-size: 10pt;*/
        font-family: Arial;
        font-size: 12pt;
        margin: 0.5cm 0;
        text-align: justify;

        letter-spacing: 0.5pt;
    }

    #title {
        width: 100%;
        text-align: center;
        margin-top: 0px;
        padding: 0px;
        /*border: solid 1px;*/
    }

    #title .title-header {
        text-align: center;
        margin-top: 0px;
        font-weight: bold;
        font-size: 12pt;
    }

    #title .title-header-2 {
        text-align: center;
        margin-top: 0px;
        font-weight: bold;
        font-size: 13pt;
    }

    #title .title-header-3 {
        text-align: center;
        margin-top: 0px;
        font-weight: bold;
        font-size: 20px;
    }

    #title .title-header-4 {
        text-align: center;
        margin-top: 0px;
        font-weight: normal;
        font-size: 10px;
        font-style: italic;
    }

    #header .judul_laporan {
        text-align: center;
        margin-top: 0px;
        font-weight: bold;
        font-size: 18px;
    }

    #header .subjudul_laporan {
        text-align: center;
        margin-top: 0px;
        font-weight: bold;
        font-size: 14px;
    }

    #footer {
        position: fixed;
        left: 0;
        right: 0;
        color: #aaa;
        font-size: 0.9em;
    }

    #footer {
        bottom: 0;
        border-top: 0.1pt solid #aaa;
    }

    #footer table {
        width: 100%;
        border-collapse: collapse;
        border: none;
    }

    #footer td {
        padding: 0;
        width: 50%;
    }

    #diatas {
        position: fixed;
    }

    #dataTableBuilder {
        margin-top: 500px;
    }

    .page-number {
        text-align: right;
    }

    .page-number:before {
        content: "Halaman " counter(page);
    }

    hr {
        page-break-after: always;
        border: 0;
    }

    .garis-dua {
        border-top: 0.1pt solid #aaa;
    }

    .garis-satu {
        border-top: 3pt solid #000;
    }

    .logo-img {
        top: -10px;
    }

    .tg-transparent {
        border: none;
        background: transparent;
    }

    .tg-transparent > th {
        border: none;
        background: transparent;
    }

    .tg-transparent > td {
        border: none;
        background: transparent;
    }

    .tg .tg-3wr7 {
        font-weight: bold;
        font-size: 15px;
        font-family: "Arial", Helvetica, sans-serif !important;;
        text-align: left;
        width: 60%;
        padding: 10px;
    }

    .tg .tg-1wr7 {
        font-weight: bold;
        font-size: 15px;
        font-family: "Arial", Helvetica, sans-serif !important;;
        text-align: left;
        width: 5%;
        padding: 10px;
    }

    .tg .tg-rv4w {
        font-weight: normal;
        font-size: 14px;
        font-family: "Arial", Helvetica, sans-serif !important;;
        text-align: left;
        width: 40%;
        padding: 10px;
    }

    .tg .space-4 {
        padding-left: 30px;
    }

    .tg .space-8 {
        padding-left: 60px;
    }

    .kepada {
        padding-top: 0px;
        padding-bottom: 0px;
    }

    .tg .nama {
        font-weight: bold;
    }

    table .titik-titik{
        border-bottom: 0.1pt dashed #333;
    }

    .tg .keterangan-harga {
        width: 80%;
    }

    .tg .rp-harga {
        width: 5%;
    }

    .tg .harga {
        width: 15%;
    }

</style>

<div id="diatas">
    <div id="title">
        <img class="logo-img" style="position: fixed; float: left; width: 85px;"
             src="{{ $srcimage }}"/>

        <table width="100%" cellpadding="0">
            <tr>
                <td class="title-header">{{ $namadinas }}</td>
            </tr>
            <tr>
                <td class="title-header-2">{{ $namakabupaten }}</td>
            </tr>
            <tr>
                <td class="title-header-3">{{ strtoupper($namasekolah) }}</td>
            </tr>
            <tr>
                <td class="title-header-4">{{ $namaalamat }}</td>
            </tr>
        </table>

        <table width="100%" style="margin-top: 10px">
            <tr>
                <td class="garis-satu"></td>
            </tr>
            <tr>
                <td class="garis-dua"></td>
            </tr>
        </table>
    </div>
</div>

<div id="footer">
    <table>
        <tr>
            <td>Sistem Informasi Pembayaran Iuran Komite</td>
        </tr>
    </table>
</div>

@foreach($penunggak as $value)
    <table id="dataTableBuilder" class="table table-striped table-bordered table-hover display select tg"
           cellspacing="2" width="100%" style="margin-top: 100px;">
        <thead>
        <tr class="tg-transparent kepada">
            <th rowspan="5"></th>
            <td class="tg-rv4w space-4">Kepada</td>
        </tr>
        <tr class="tg-transparent kepada">
            <td class="tg-rv4w">Yth. Bapak/Ibu Wali Murid</td>
        </tr>
        <tr class="tg-transparent kepada">
            <td class="tg-rv4w space-4"><span class="nama">{{ $value->name_siswa }}</span></td>
        </tr>
        <tr class="tg-transparent kepada">
            <td class="tg-rv4w space-4">Di</td>
        </tr>
        <tr class="tg-transparent kepada">
            <td class="tg-rv4w space-8">Tempat</td>
        </tr>

        <tr class="tg-transparent">
            <th colspan="2" style="padding: 10px"></th>
        </tr>

    </table>

    <p>Dengan hormat,</p>
    <p>
        Demi kelancaran kegiatan belajar mengajar tahun pelajaran {{ $tahunsekarang }} di {{ $namasekolah }},
        maka kami mohon kiranya Bapak/Ibu dapat melunasi tunggakan anak tersebut sebagai berikut:
    </p>

    <table id="dataTableBuilder2" class="table table-striped table-bordered table-hover display select tg"
           cellspacing="2" width="100%" style="padding-left: 10px; padding-right: 10px;">
        <tr class="tg-transparent">
            <td class="tg-rv4w" class="keterangan-harga" style="width: 70%;">1. Komite & OSIS bulan, <strong>{{ $value->string_tunggakan_bulan }}</strong></td>
            <td class="tg-rv4w" class="rp-harga" style="width: 5%;">Rp. </td>
            <td class="tg-rv4w" class="harga" style="text-align: right; width: 25%;">{{ number_format($value->total, 0, ',', '.') }}</td>
        </tr>
        <tr class="tg-transparent">
            <td class="tg-rv4w" class="keterangan-harga" style="width: 70%;">2. .....................................................................................</td>
            <td class="tg-rv4w" class="rp-harga" style="width: 5%;">Rp. </td>
            <td class="tg-rv4w" class="harga" style="text-align: right; width: 25%;"></td>
        </tr>
        <tr class="tg-transparent">
            <td class="tg-rv4w" class="keterangan-harga" style="width: 70%;">3. .....................................................................................</td>
            <td class="tg-rv4w" class="rp-harga" style="width: 5%;">Rp. </td>
            <td class="tg-rv4w" class="harga" style="text-align: right; width: 25%;"></td>
        </tr>
        <tr class="tg-transparent">
            <td class="tg-rv4w" class="keterangan-harga" style="width: 70%; text-align: right; padding-right: 10px;">Jumlah</td>
            <td class="tg-rv4w" class="rp-harga" style="width: 5%;">Rp. </td>
            <td class="tg-rv4w" class="harga" style="text-align: right; width: 25%;">{{ number_format($value->total, 0, ',', '.') }}</td>
        </tr>
    </table>
    <br>
    <p>
        Atas perhatiannya kami ucapkan terima kasih.
    </p>

    <table id="dataTableBuilder5" class="table table-striped table-bordered table-hover display select tg"
           cellspacing="2" width="100%">
        <thead>
        <tr class="tg-transparent kepada">
            <th rowspan="5"></th>
            <td class="tg-rv4w space-4" style="text-align: center;">{{ $kota }}, {{ date('d-m-Y') }}<br>{{ $pengesah->display_name }}</td>
        </tr>
        <tr class="tg-transparent kepada">
            <td class="tg-rv4w space-4" style="text-align: center; padding-top: 40px; padding-bottom: 40px;"><span class="nama"></span></td>
        </tr>
        <tr class="tg-transparent kepada">
            <td class="tg-rv4w space-4" style="text-align: center;"><span class="nama"><u>{{ $pengesah->nilai }}</u></span><br>NIP. {{ $pengesah->nip }}</td>
        </tr>

    </table>
    <hr>
@endforeach

</body>
</html>
