<?php
/**
 * Created by PhpStorm.
 * User: ocol
 * Date: 10/11/16
 * Time: 2:20
 */
?>

@extends('layouts.master')

@section('title_name')
    Cetak Laporan Siswa Per Kelas
@endsection

@section('active_page')
    <li>Laporan</li>
    <li class="active">Laporan Siswa Per Kelas</li>
@endsection

@section('content')
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-default">
                    <div class="box-header with-border">
                        <h3 class="box-title">Laporan Siswa Per Kelas</h3>
                    </div>
                    <!-- /.box-header -->

                    <div class="col-md-12 daftar-tombol">
                        {{--                        {!! link_to('#', $title='Cetak Laporan Data Siswa Per Program Studi / Jurusan', $attributes=['id'=>'cetakprogram', 'class'=>'btn btn-primary pull-right']) !!}--}}
                        <a href="#" class="btn btn-primary btn-flat" id="cetak"><i class="fa fa-print"></i> Cetak Laporan Data Siswa Per Kelas</a>
                        {{--<a href="#" class="btn btn-info btn-flat" id="cetakdaftar"><i class="fa fa-print"></i> Cetak Daftar Siswa</a>--}}
                    </div>

                    <div class="box-body">

                        <div class="form-group">
                            {!! Form::label('kelas_id', 'Kelas Siswa') !!}
                            {!!  Form::select('kelas_id', [], null, ['class' => 'form-control select2', 'style'=> 'width:100%;','onchange'=> "reloadTable();"])  !!}
                        </div>

                        <table width="100%" class="table table-striped table-bordered table-hover" id="dataTableBuilder">
                            <thead>
                            <tr>
                                <th class="col-md-2">NISN</th>
                                <th class="col-md-4">Nama</th>
                                <th class="col-md-2">JK</th>
                                <th class="col-md-3">Kelas</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>

                    </div>


                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    @endsection

    @section('header_top')
            <!-- DataTables -->
    <link href="/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="/datatables-responsive/dataTables.responsive.css" rel="stylesheet">

    <link rel="stylesheet" href="/theme/plugins/select2/select2.min.css">
@endsection

@section('footer_add')
    <script src="/datatables/js/jquery.dataTables.min.js"></script>
    <script src="/datatables-plugins/dataTables.bootstrap.min.js"></script>
    <script src="/datatables-responsive/dataTables.responsive.js"></script>

    <script src="/theme/plugins/select2/select2.full.min.js"></script>
@endsection

@section('custom_script')
    <script src="/js/laporan/siswa-per-kelas.js"></script>
@endsection




