<?php
/**
 * Created by PhpStorm.
 * User: ocol
 * Date: 09/11/16
 * Time: 23:49
 */
?>

@extends('layouts.master')

@section('title_name')
    Daftar Penunggak
@endsection

@section('active_page')
    <li>Laporan</li>
    <li class="active">Daftar Penunggak</li>
@endsection

@section('content')
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-default">
                    <div class="box-header with-border">
                        <h3 class="box-title">Daftar Penunggak Iuran Komite</h3>
                    </div>
                            <!-- /.box-header -->
                    @if(Auth::user()->can(['cetak_surat_tagihan', 'cetak_laporan_penunggak']))
                        <div class="col-md-12 daftar-tombol">
                            @if(Auth::user()->can('cetak_surat_tagihan'))
                                <a href="#" class="btn btn-primary btn-flat" id="surattagihan"><i class="fa fa-print"></i> Surat Tagihan</a>
{{--                            {!! link_to('#', $title='Surat Tagihan', $attributes=['id'=>'surattagihan', 'class'=>'btn btn-primary pull-right']) !!}--}}
                            @endif
                            @if(Auth::user()->can('cetak_laporan_penunggak'))
                                    <a href="#" class="btn btn-info btn-flat" id="laporanpenunggak"><i class="fa fa-print"></i> Laporan Penunggak</a>
                            @endif
                        </div>
                    @endif

                    <div class="box-body">
                        <table width="100%" class="table table-striped table-bordered table-hover" id="dataTableBuilder">
                            <thead>
                            <tr>
                                <th class="col-md-1 text-center"><input name="select_all" value="1" type="checkbox"></th>
                                <th class="col-md-2">NISN</th>
                                <th class="col-md-3">Nama</th>
                                <th class="col-md-3">Total Tunggakan</th>
                                <th class="col-md-3">Keterangan</th>
                            </tr>
                            </thead>

                            <tfoot>
                            <tr>
                                <th colspan="2" style="text-align:right; font-size:22px; color: #9d1500;">Total:</th>
                                <th colspan="2" style="text-align:right; font-size:22px; color: #9d1500;">Total:</th>
                                <th style="text-align:right; font-size:22px; color: #9d1500;"></th>
                            </tr>
                            </tfoot>

                            <tbody>

                            </tbody>
                        </table>

                    </div>
                    <!-- /.box-body -->
                    @if(Auth::user()->can(['cetak_surat_tagihan', 'cetak_laporan_penunggak']))
                        <div class="col-md-12 daftar-tombol">
                            @if(Auth::user()->can('cetak_surat_tagihan'))
                                <a href="#" class="btn btn-primary btn-flat" id="surattagihanbawah"><i class="fa fa-print"></i> Surat Tagihan</a>
                                {{--                            {!! link_to('#', $title='Surat Tagihan', $attributes=['id'=>'surattagihan', 'class'=>'btn btn-primary pull-right']) !!}--}}
                            @endif
                            @if(Auth::user()->can('cetak_laporan_penunggak'))
                                <a href="#" class="btn btn-info btn-flat" id="laporanpenunggakbawah"><i class="fa fa-print"></i> Laporan Penunggak</a>
                            @endif
                        </div>
                    @endif

                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
@endsection

@section('header_top')
        <!-- DataTables -->
    <link href="/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="/datatables-responsive/dataTables.responsive.css" rel="stylesheet">
@endsection

@section('footer_add')
    <script src="/datatables/js/jquery.dataTables.min.js"></script>
    <script src="/datatables-plugins/dataTables.bootstrap.min.js"></script>
    <script src="/datatables-responsive/dataTables.responsive.js"></script>
@endsection

@section('custom_script')
    <script src="/js/laporan/daftar-penunggak.js"></script>
@endsection


