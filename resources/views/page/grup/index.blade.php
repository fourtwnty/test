<?php
/**
 * Created by PhpStorm.
 * User: ocol
 * Date: 10/11/16
 * Time: 2:51
 */
?>

@extends('layouts.master')

@section('title_name')
    Grup Pengguna
@endsection

@section('active_page')
    <li class="active">Grup Pengguna</li>
@endsection

@section('content')
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-default">
                    <div class="box-header with-border">
                        <h3 class="box-title">Daftar Grup Pengguna</h3>
                    </div>

                    @include('page.grup.modal')
                            <!-- /.box-header -->
                    @if(Auth::user()->can('tambah_grup'))
                        <div class="col-md-12 daftar-tombol">
                            <a href="/grup/create" class="btn btn-primary btn-flat"><i class="fa fa-plus"></i> Data Baru</a>
                            {{--<a href="/pengaturan/program/create" class="btn btn-primary pull-right">Data Baru</a>--}}
                        </div>
                    @endif

                    <div class="box-body">



                        <table width="100%" class="table table-striped table-bordered table-hover" id="dataTableBuilder">
                            <thead>
                            <tr>
                                <td>Nama</td>
                                <td>Nama Yang Dilihat</td>
                                <td>Deskripsi</td>
                                <td>Aksi</td>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>

                    </div>
                    <!-- /.box-body -->
                    @if(Auth::user()->can('tambah_grup'))
                        <div class="box-footer">
                            <a href="/grup/create" class="btn btn-primary btn-flat"><i class="fa fa-plus"></i> Data Baru</a>
                        </div>
                    @endif

                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
@endsection

@section('header_top')
        <!-- DataTables -->
    <link href="/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="/datatables-responsive/dataTables.responsive.css" rel="stylesheet">

    <link href="/bootstrap-dualbox/bootstrap-duallistbox.css" rel="stylesheet">
@endsection

@section('footer_add')
    <script src="/datatables/js/jquery.dataTables.min.js"></script>
    <script src="/datatables-plugins/dataTables.bootstrap.min.js"></script>
    <script src="/datatables-responsive/dataTables.responsive.js"></script>

    <script src="/bootstrap-dualbox/jquery.bootstrap-duallistbox.js"></script>
@endsection

@section('custom_script')
    <script src="/js/grup/index.js"></script>
@endsection


