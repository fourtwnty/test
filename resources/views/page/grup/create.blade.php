<?php
/**
 * Created by PhpStorm.
 * User: ocol
 * Date: 10/11/16
 * Time: 2:52
 */
?>

@extends('layouts.master')

@section('title_name')
    Tambah Grup Pengguna
@endsection

@section('active_page')
    <li class="active">Grup Pengguna Baru</li>
@endsection

@section('content')
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-default">
                    <div class="box-header with-border">
                        <h3 class="box-title">Tambah Grup Pengguna Baru</h3>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <input type="hidden" name="token" id="token" value="{{ csrf_token() }}" />

                            <div class="col-md-12">

                                @include('page.grup.form')
                            </div>


                        </div>
                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                        <a href="#" class="btn btn-primary btn-flat" id="simpan"><i class="fa fa-save"></i> Simpan Data</a>
                        <a href="/grup" class="btn btn-warning btn-flat" id="batal"><i class="fa fa-close"></i> Batal</a>
                    </div>

                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
@endsection

@section('header_top')
    <link href="/bootstrap-dualbox/bootstrap-duallistbox.css" rel="stylesheet">
@endsection

@section('footer_add')
    <script src="/bootstrap-dualbox/jquery.bootstrap-duallistbox.js"></script>
@endsection

@section('custom_script')
    <script src="/js/grup/create.js"></script>
@endsection


