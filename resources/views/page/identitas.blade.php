<?php
/**
 * Created by PhpStorm.
 * User: ocol
 * Date: 08/11/16
 * Time: 18:52
 */
?>

@extends('layouts.master')

@section('header_add')
@endsection

@section('title_name')
    Identitas
@endsection

@section('active_page')
    <li class="active">Identitas</li>
@endsection

@section('content')
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-default">
                    <div class="box-header with-border">
                        <h3 class="box-title">Identitas Sekolah</h3>
                    </div>

                    {!! Form::open(['url' => '/identitas/', 'method'=>'PUT', 'files'=>true, 'enctype'=>"multipart/form-data", 'id'=>'form-identitas']) !!}

                    <div class="box-body">

                        @if (Session::has('error'))
                        <div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4><i class="icon fa fa-ban"></i> Kesalahan!</h4>
                            {{ Session::get('error') }}
                        </div>
                        @endif

                        @if (Session::has('message'))
                        <div class="alert alert-success alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <h4><i class="icon fa fa-check"></i> Informasi!</h4>
                            {{ Session::get('message') }}
                        </div>
                        @endif

                        <div class="row">
                            <input type="hidden" name="token" id="token" value="{{ csrf_token() }}" />
                            <input type="hidden" name="id" id="id" value="" />

                            {{--<input type="hidden" name="logo" id="logo" value="" />--}}

                            <div class="col-md-12">
                                {{--{!! Form::open(['url'=>'/master/barang/uploadgambar', 'files'=>true, 'enctype'=>"multipart/form-data"]) !!}--}}
                                {!! Form::label('logo-baru', 'Logo') !!}
                                <div class="form-group">
                                    <img id="img-profile-logo" class="profile-user-img img-responsive" src="/images/uploads/identitas/default.png" alt="Logo Sekolah">
                                    {!! Form::file('logo-baru', ['id'=>'logo-baru','placeholder' => '']) !!}
                                </div>

                                {!! Form::hidden('logo', null, ['id'=>'logo']) !!}

                                <div class="form-group">

                                    {{--{!! Form::label('nama', 'Nama Sekolah') !!}--}}
                                    {{--{!! Form::text('nama', null, ['class' => 'form-control', 'placeholder' => 'Inputkan nama sekolah']) !!}--}}
                                </div>


                                <div class="form-group">
                                    {!! Form::label('nama', 'Nama Sekolah') !!}
                                    {!! Form::text('nama', null, ['class' => 'form-control', 'placeholder' => 'Inputkan nama sekolah']) !!}
                                </div>

                                <div class="form-group">
                                    {!! Form::label('alamat', 'Alamat') !!}
                                    {!! Form::text('alamat', null, ['class' => 'form-control', 'placeholder' => 'Inputkan alamat sekolah']) !!}
                                </div>

                                <div class="form-group">
                                    {!! Form::label('telp', 'Telepon') !!}
                                    {!! Form::text('telp', null, ['class' => 'form-control', 'placeholder' => 'Inputkan nomor telepon', 'onkeypress' => 'return validasiAngka(event);']) !!}
                                </div>

                                <div class="form-group">
                                    {!! Form::label('fax', 'Fax') !!}
                                    {!! Form::text('fax', null, ['class' => 'form-control', 'placeholder' => 'Inputkan nomor fax', 'onkeypress' => 'return validasiAngka(event);']) !!}
                                </div>

                                    {{--protected $fillable = ['nama', 'alamat', 'telp', 'fax',--}}
                                    {{--'logo', 'dinas', 'kota', 'kelurahan',--}}
                                    {{--'kecamatan', 'kabupaten',--}}
                                    {{--];--}}
                                <div class="form-group">
                                    {!! Form::label('dinas', 'Dinas') !!}
                                    {!! Form::text('dinas', null, ['class' => 'form-control', 'placeholder' => 'Inputkan nama dinas']) !!}
                                </div>

                                <div class="form-group">
                                    {!! Form::label('kota', 'Kota') !!}
                                    {!! Form::text('kota', null, ['class' => 'form-control', 'placeholder' => 'Inputkan nama kota']) !!}
                                </div>

                                <div class="form-group">
                                    {!! Form::label('kelurahan', 'Kelurahan') !!}
                                    {!! Form::text('kelurahan', null, ['class' => 'form-control', 'placeholder' => 'Inputkan nama kelurahan']) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::label('kecamatan', 'Kecamatan') !!}
                                    {!! Form::text('kecamatan', null, ['class' => 'form-control', 'placeholder' => 'Inputkan nama kecamatan']) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::label('kabupaten', 'Kabupaten') !!}
                                    {!! Form::text('kabupaten', null, ['class' => 'form-control', 'placeholder' => 'Inputkan nama kabupaten']) !!}
                                </div>
                            </div>


                        </div>
                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                        @if(Auth::user()->can('ubah_identitas'))
                            <button type="submit" id="simpan" name="simpan" class="btn btn-primary btn-flat"><i class="fa fa-save"></i>
                                Simpan Identitas
                            </button>
{{--                        {!! Form::submit('Simpan Identitas', ['id'=>'simpan', 'class'=>'btn btn-primary btn-flat']) !!}--}}
                        @endif
{{--                        {!! link_to('#', $title='Simpan Identitas', $attributes=['id'=>'simpan', 'class'=>'btn btn-success']) !!}--}}
                            <a href="#" class="btn btn-warning btn-flat" id="batal"><i class="fa fa-close"></i> Batal</a>
{{--                        {!! link_to('#', $title='Batal', $attributes=['id'=>'batal', 'class'=>'btn btn-primary btn-flat btn-warning']) !!}--}}
                    </div>

                    {!! Form::close() !!}

                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
@endsection

@section('footer_add')

@endsection

@section('custom_script')
    <script src="/js/page/identitas.js"></script>
@endsection
