<?php
/**
 * Created by PhpStorm.
 * User: ocol
 * Date: 18/09/16
 * Time: 15:47
 */
?>

@extends('layouts.master')

@section('header_top')
<!-- DataTables -->
<link href="/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">

<!-- DataTables Responsive CSS -->
<link href="/datatables-responsive/dataTables.responsive.css" rel="stylesheet">
@endsection

@section('title_name')
    Pengesah
@endsection

@section('active_page')
    <li class="active">Pengesah</li>
@endsection

@section('content')
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-default">
                    <div class="box-header with-border">
                        <h3 class="box-title">Pengesah Laporan</h3>
                    </div>

                    @include('page.pengesah.modal')
                            <!-- /.box-header -->
                    <div class="box-body">

                        <table width="100%" class="table table-striped table-bordered table-hover" id="dataTableBuilder">
                                <thead>
                                <tr>
                                    <th>Nama</th>
                                    <th>Nama Yang Dilihat</th>
                                    <th>Nilai</th>
                                    <th>NIP</th>
                                    <th>Aksi</th>
                                </tr>
                                </thead>
                                <tbody>

                                </tbody>

                            </table>

                    </div>
                    <!-- /.box-body -->

                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
@endsection

@section('footer_add')
    <script src="/datatables/js/jquery.dataTables.min.js"></script>
    <script src="/datatables-plugins/dataTables.bootstrap.min.js"></script>
    <script src="/datatables-responsive/dataTables.responsive.js"></script>
@endsection

@section('custom_script')
    <script src="/js/page/pengesah.js"></script>
@endsection
