<?php
/**
 * Created by PhpStorm.
 * User: ocol
 * Date: 18/09/16
 * Time: 15:53
 */
?>

<?php
/**
 * Created by PhpStorm.
 * User: ocol
 * Date: 18/09/16
 * Time: 15:53
 */
?>

<div class="modal fade" id="modalUbah" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Atur Data Pengesah Laporan</h4>
            </div>
            <div class="modal-body">
                <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
                <input type="hidden" id="id">
                <form role="form">
                    <fieldset>
                        <div class="form-group">
                            {!! Form::label('name', 'Nama Pengesah') !!}
                            {!! Form::text('name', null, ['class' => 'form-control', 'disabled']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('display_name', 'Nama Yang Akan Terlihat') !!}
                            {!! Form::text('display_name', null, ['class' => 'form-control', 'placeholder' => 'Masukan Nama Yang Akan Terlihat Dilaporan']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('nilai', 'Nilai Pengesah') !!}
                            {!! Form::text('nilai', null, ['class' => 'form-control', 'placeholder' => 'Masukan Nilai Dari Pengesah']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('nip', 'NIP') !!}
                            {!! Form::text('nip', null, ['class' => 'form-control', 'placeholder' => 'Masukan NIP dari Pengesah']) !!}
                        </div>
                    </fieldset>
                </form>
            </div>
            <div class="modal-footer">
                <a href="#" class="btn btn-primary btn-flat" id="simpan"><i class="fa fa-save"></i> Simpan Data</a>
                <button type="button" class="btn btn-default btn-flat" data-dismiss="modal"><i class="fa fa-close"></i> Batal</button>
                {{--<button type="button" class="btn btn-primary">Save changes</button>--}}

{{--                {!! link_to('#', $title='Simpan Data', $attributes=['id'=>'simpan', 'class'=>'btn btn-primary']) !!}--}}
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
