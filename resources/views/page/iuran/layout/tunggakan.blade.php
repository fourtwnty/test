<?php
/**
 * Created by PhpStorm.
 * User: ocol
 * Date: 09/11/16
 * Time: 21:09
 */
?>

<table width="100%" class="table table-striped table-bordered table-hover" id="dataTableBuilderTunggakan">
    <thead>
    <tr>
        <th class="col-md-1"><input name="select_all" value="1" type="checkbox"></th>
        <th class="col-md-8">Bulan</th>
        <th class="col-md-3">Harga</th>
    </tr>
    </thead>

    <tfoot>
    <tr>
        <th colspan="2" style="text-align:right; font-size:22px; color: #9d1500;">Total:</th>
        <th style="text-align:right; font-size:22px; color: #9d1500;"></th>
    </tr>
    </tfoot>

    <tbody>

    </tbody>
</table>
