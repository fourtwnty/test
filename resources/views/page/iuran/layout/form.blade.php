<?php
/**
 * Created by PhpStorm.
 * User: ocol
 * Date: 09/11/16
 * Time: 21:09
 */
?>

<div class="form-group">
    {!!  Form::hidden('nisn', null, ['class' => 'form-control nisn-asli'])  !!}
</div>

<div class="form-group">
    {!!  Form::hidden('kodetransaksi', null, ['class' => 'form-control kodetransaksi'])  !!}
</div>

{!! Form::label('nisn-cari', 'NISN') !!} <br/>
<div class="form-group input-group">
    {!! Form::text('nisn-cari', null, ['class' => 'form-control', 'placeholder' => 'Masukan NISN', 'onkeypress'=>"return validasiAngka(event);"]) !!}
    {{--<input type="text" class="form-control">--}}
    <span class="input-group-btn">
            <a class="btn btn-default" type="button" data-toggle="modal" data-target="#modalCari" id="carisiswa">
                <i class="fa fa-search"></i>
            </a>
        </span>
</div>

<div class="form-group">
    {!! Form::label('nama', 'Nama') !!}
    {!! Form::text('nama', null, ['class' => 'form-control hasil-cari', 'placeholder' => 'Nama Siswa', 'disabled']) !!}
</div>

<div class="form-group">
    {!!  Form::label('daftar-tunggakan', 'Daftar Tunggakan Siswa')  !!}
    @include('page.iuran.layout.tunggakan')
</div>
