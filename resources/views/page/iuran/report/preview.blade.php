<?php
/**
 * Created by PhpStorm.
 * User: ocol
 * Date: 09/11/16
 * Time: 21:10
 */
?>

<html>
<head>
    <title>Nota Bayar Iuran</title>
    <style type="text/css">
        body {
            font-size: 10pt;
        }

        .page-wrap {
            /*width: 700px;*/
            width: 100%;
            margin: 0 auto;
        }
        .center-only {
            text-align: center;
            margin: 0 auto;
            /*width: 30em;*/
            width: 100%;
        }
        .center-justified {
            text-align: justify;
            margin: 0 auto;
            /*width: 30em;*/
            width: 100%;
        }
        table.outline-table {
            border: 1px solid;
            border-spacing: 0;
        }
        tr.border-bottom td, td.border-bottom {
            border-bottom: 1px solid;
        }
        tr.border-top td, td.border-top {
            border-top: 1px solid;
        }
        tr.border-right td, td.border-right {
            border-right: 1px solid;
        }
        tr.border-right td:last-child {
            border-right: 0px;
        }
        tr.center td, td.center {
            text-align: center;
            vertical-align: text-top;
        }
        td.pad-left {
            padding-left: 5px;
        }
        tr.right-center td, td.right-center {
            text-align: right;
            padding-right: 50px;
        }
        tr.right td, td.right {
            text-align: right;
        }
        .grey-dark {
            /*background:grey;*/
            background-color: #dcdcdc;
        }
        .grey-light {
            /*background:grey;*/
            background-color: #f1f1f1;
        }
        td>span.span-border-top{
            border-top: 1px solid;
        }
        td>span.span-border-bottom{
            border-bottom: 1px solid;
        }
        td>span.span-border-bottom{
            border-bottom: none;
        }
    </style>
</head>
<body>
<div class="page-wrap">
    <table width="100%">
        <tbody>
        <tr>
            <td width="15%">
                {{--<img src="img/tutwuri.png" width="70px"> <!-- your logo here -->--}}
                <img src="{{ $srcimage }}" width="70px"> <!-- your logo here -->
            </td>
            <td width="40%" align="left">
                <strong style="font-size: 14pt;">Pembayaran Iuran Komite</strong><br><br>
                <small>Tanggal:</small> <br>{{ $tgl_transaksi }}<br>
                {{--<small>Iuran ID:</small> <br>{{ $iuran->id }}<br>--}}
                NISN: <br> <strong style="font-size: 10pt;">{{ $iuran->nisn_siswa }}</strong><br>
            </td>
            <td width="45%" align="right">
                <strong style="font-size: 13pt;">{{ $iuran->id }}</strong><br><br>
                <small>Nama:</small> <br>{{ $iuran->name_siswa }}<br>
            </td>
        </tr>
        <tr>
            <td colspan="3">&nbsp;</td>
        </tr>

        </tbody>
    </table>
    {{--<p>&nbsp;</p>--}}
    <table width="100%" class="outline-table">
        <tbody>
        <tr class="border-bottom border-right grey-dark">
            <td colspan="2" class="center"><strong>Detail Pembayaran Iuran Komite</strong></td>
        </tr>

        <tr class="border-bottom border-top border-right center grey-light">
            <td width="70%"><strong>Bulan</strong></td>
            <td width="30%"><strong>Harga</strong></td>
        </tr>

        @foreach($iuran->iurandetail() as $key => $value)
            <tr class="border-bottom border-right">
                {{ $value->loadTambahan() }}
                <td width="70%"> {{ $value->namabulantahun }}</td>
                <td width="30%" class="right">{{ number_format($value->harga, 0, ',', '.') }}</td>
            </tr>
        @endforeach

        <tr class="border-bottom border-right center">
            <td width="70%">&nbsp;</td>
            <td width="30%"></td>
        </tr>

        <tr class="border-right">
            <td class="center border-top"><strong>Total</strong></td>
            <td class="right border-top"><strong>{{ number_format($iuran->total, 0, ',', '.')}}</strong></td>
        </tr>
        </tbody>
    </table>
    <p>&nbsp;</p>


    <table width="100%">
        <tbody>
        <tr>
            <td width="50%" align="center">
                &nbsp;<br>
                Penyetor / Tanda Tangan<br>
                &nbsp;<br>
                &nbsp;<br>
                &nbsp;<br>
                <span class="span-border-bottom">( . . . . . . . . . . . . . . . )</span><br>
                &nbsp;
            </td>
            <td width="50%" align="center">
                {{ $kota }}, {{ date('d/m/Y ') }}<br>
                Bendahara Komite / Tanda Tangan<br>
                &nbsp;<br>
                &nbsp;<br>
                &nbsp;<br>
                <span class="span-border-bottom-name">{{ strtoupper($pengesah->nilai) }}</span><br>
                {{ $pengesah->nip }}
            </td>
        </tr>
        </tbody>
    </table>
</div>
</body>
</html>
