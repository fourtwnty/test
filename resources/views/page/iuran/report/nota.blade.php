<?php
/**
 * Created by PhpStorm.
 * User: ocol
 * Date: 09/11/16
 * Time: 21:10
 */
?>

@extends('layouts.master')

@section('title_name')
    Nota Pembayaran Iuran
@endsection

@section('active_page')
    <li>Bayar Iuran</li>
    <li class="active">Nota Pembayaran Iuran</li>
@endsection

@section('content')
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-default">
                    <div class="box-header with-border">
                        <h3 class="box-title">Daftar Pembayaran Iuran Komite</h3>
                    </div>

                    @include('layouts.modalhapus')
                            <!-- /.box-header -->
                    @if(Auth::user()->can('bayar_iuran'))
                        <div class="col-md-12 daftar-tombol">
                            <a href="/iuran/create" class="btn btn-primary btn-flat"><i class="fa fa-hand-o-left"></i> Kembali</a>
                        </div>
                    @endif

                    <iframe name="myframe" src="/iuran/previewnota/{{ $kode }}" height="500" width="100%">Your browser does not support frames.</iframe>

                    <!-- /.box-body -->
                    @if(Auth::user()->can('bayar_iuran'))
                        <div class="box-footer">
                            <a href="/iuran/create" class="btn btn-primary btn-flat"><i class="fa fa-hand-o-left"></i> Kembali</a>
                        </div>
                    @endif

                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
@endsection


