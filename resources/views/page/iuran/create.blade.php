<?php
/**
 * Created by PhpStorm.
 * User: ocol
 * Date: 09/11/16
 * Time: 21:09
 */
?>

@extends('layouts.master')

@section('title_name')
    Bayar Iuran
@endsection

@section('active_page')
    <li class="active">Bayar Iuran</li>
@endsection

@section('content')
    @include('page.pengaturan.mulaiiuran.modal-cari')

    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-default">
                    <div class="box-header with-border">
                        <h3 class="box-title">Bayar Iuran</h3>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <input type="hidden" name="token" id="token" value="{{ csrf_token() }}" />

                            <div class="col-md-12">
                                @include('page.iuran.layout.form')
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                        <a href="#" class="btn btn-primary btn-flat" id="simpan"><i class="fa fa-save"></i> Simpan Data</a>
                        <a href="/iuran" class="btn btn-warning btn-flat" id="batal"><i class="fa fa-close"></i> Batal</a>
                    </div>

                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
@endsection

@section('header_top')
        <!-- DataTables -->
    <link href="/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="/datatables-responsive/dataTables.responsive.css" rel="stylesheet">
@endsection

@section('footer_add')
    <script src="/datatables/js/jquery.dataTables.min.js"></script>
    <script src="/datatables-plugins/dataTables.bootstrap.min.js"></script>
    <script src="/datatables-responsive/dataTables.responsive.js"></script>
@endsection

@section('custom_script')
    <script src="/js/page/iuran/create.js"></script>
@endsection


