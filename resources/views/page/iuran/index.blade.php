<?php
/**
 * Created by PhpStorm.
 * User: ocol
 * Date: 09/11/16
 * Time: 21:08
 */
?>

@extends('layouts.master')

@section('title_name')
    Iuran Komite
@endsection

@section('active_page')
    <li class="active">Iuran Komite</li>
@endsection

@section('content')
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-default">
                    <div class="box-header with-border">
                        <h3 class="box-title">Daftar Pembayaran Iuran Komite</h3>
                    </div>

                    <input type="hidden" name="token" id="token" value="{{ csrf_token() }}" />
                    @include('layouts.modalhapus')
                            <!-- /.box-header -->
                    @if(Auth::user()->can('bayar_iuran'))
                        <div class="col-md-12 daftar-tombol">
                            <a href="/iuran/create" class="btn btn-primary btn-flat"><i class="fa fa-plus"></i> Bayar Iuran</a>
                        </div>
                    @endif

                    <div class="box-body">
                    <table width="100%" class="table table-striped table-bordered table-hover" id="dataTableBuilder">
                        <thead>
                        <tr>
                            <td>Kode</td>
                            <td>Tanggal</td>
                            <td>NISN</td>
                            <td>Nama</td>
                            <td>Total</td>
                            <td>Keterangan</td>
                            <td>Aksi</td>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                        </div>

                    <!-- /.box-body -->
                    @if(Auth::user()->can('bayar_iuran'))
                        <div class="box-footer">
                            <a href="/iuran/create" class="btn btn-primary btn-flat"><i class="fa fa-plus"></i> Bayar Iuran</a>
                        </div>
                    @endif

                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
@endsection

@section('header_top')
        <!-- DataTables -->
    <link href="/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="/datatables-responsive/dataTables.responsive.css" rel="stylesheet">
@endsection

@section('footer_add')
    <script src="/datatables/js/jquery.dataTables.min.js"></script>
    <script src="/datatables-plugins/dataTables.bootstrap.min.js"></script>
    <script src="/datatables-responsive/dataTables.responsive.js"></script>
@endsection

@section('custom_script')
    <script src="/js/page/iuran/index.js"></script>
@endsection


