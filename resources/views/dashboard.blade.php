<?php
/**
 * Created by PhpStorm.
 * User: ocol
 * Date: 08/11/16
 * Time: 0:55
 */
?>

@extends('layouts.master')

@section('header_add')
    {{--<link rel="stylesheet" href="/theme/plugins/iCheck/flat/blue.css">--}}
    {{--<!-- Morris chart -->--}}
    {{--<link rel="stylesheet" href="/theme/plugins/morris/morris.css">--}}
    {{--<!-- jvectormap -->--}}
    {{--<link rel="stylesheet" href="/theme/plugins/jvectormap/jquery-jvectormap-1.2.2.css">--}}
    {{--<!-- Date Picker -->--}}
    {{--<link rel="stylesheet" href="/theme/plugins/datepicker/datepicker3.css">--}}
    {{--<!-- Daterange picker -->--}}
    {{--<link rel="stylesheet" href="/theme/plugins/daterangepicker/daterangepicker.css">--}}
    {{--<!-- bootstrap wysihtml5 - text editor -->--}}
    {{--<link rel="stylesheet" href="/theme/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">--}}
@endsection

@section('title_name')
    Dashboard
@endsection

@section('active_page')
    <li class="active">Dashboard</li>
@endsection

@section('content')

    <!-- Main content -->
<section class="content">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-aqua">
                    <div class="inner">
                        <h3 id="j-pengguna">150</h3>

                        <p>Pengguna Terdaftar</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-bag"></i>
                    </div>
                    <a href="/pengguna" class="small-box-footer" id="more-pengguna">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-green">
                    <div class="inner">
                        <h3 id="j-lunas">53<sup style="font-size: 20px">%</sup></h3>

                        <p>Lunas</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-stats-bars"></i>
                    </div>
                    <a href="/laporan/daftarpembayaran" class="small-box-footer" id="more-lunas">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-yellow">
                    <div class="inner">
                        <h3 id="j-penunggak">44</h3>

                        <p>Penunggak</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-person-add"></i>
                    </div>
                    <a href="/laporan/daftarpenunggak" class="small-box-footer" id="more-penunggak">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-red">
                    <div class="inner">
                        <h3 id="j-siswa">65</h3>

                        <p>Total Siswa</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-pie-graph"></i>
                    </div>
                    <a href="/siswa" class="small-box-footer" id="more-siswa">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
        </div>
        <!-- /.row -->

        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-aqua">
                    <div class="inner">
                        <h3 id="j-program">150</h3>

                        <p>Program Studi</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-bag"></i>
                    </div>
                    <a href="/pengaturan/program" class="small-box-footer" id="more-program">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-yellow">
                    <div class="inner">
                        <h3 id="j-kelas">44</h3>

                        <p>Kelas</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-person-add"></i>
                    </div>
                    <a href="/pengaturan/kelas" class="small-box-footer" id="more-kelas">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->

            <!-- ./col -->
        </div>
        <!-- /.row -->

    </section>
    <!-- /.content -->
@endsection

@section('footer_add')
    {{--<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>--}}
    {{--<script src="/theme/plugins/morris/morris.min.js"></script>--}}
    {{--<!-- Sparkline -->--}}
    {{--<script src="/theme/plugins/sparkline/jquery.sparkline.min.js"></script>--}}
    {{--<!-- jvectormap -->--}}
    {{--<script src="/theme/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>--}}
    {{--<script src="/theme/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>--}}
    {{--<!-- jQuery Knob Chart -->--}}
    {{--<script src="/theme/plugins/knob/jquery.knob.js"></script>--}}
    {{--<!-- daterangepicker -->--}}
    {{--<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>--}}
    {{--<script src="/theme/plugins/daterangepicker/daterangepicker.js"></script>--}}
    {{--<!-- datepicker -->--}}
    {{--<script src="/theme/plugins/datepicker/bootstrap-datepicker.js"></script>--}}
    {{--<!-- Bootstrap WYSIHTML5 -->--}}
    {{--<script src="/theme/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>--}}
    {{--<!-- Slimscroll -->--}}
    {{--<script src="/theme/plugins/slimScroll/jquery.slimscroll.min.js"></script>--}}
    {{--<!-- FastClick -->--}}
    {{--<script src="/theme/plugins/fastclick/fastclick.js"></script>--}}
@endsection

@section('custom_script')
        <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    {{--<script src="/theme/dist/js/pages/dashboard.js"></script>--}}
    <!-- AdminLTE for demo purposes -->
    <script src="/theme/dist/js/demo.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="/js/home.js"></script>
@endsection