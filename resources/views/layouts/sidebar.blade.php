<?php
/**
 * Created by PhpStorm.
 * User: ocol
 * Date: 08/11/16
 * Time: 13:32
 */
?>
        <!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <span><i class="fa fa-user-circle fa-2x" style="color: #ffffff;"></i></span>
{{--                <img src="/{{ env('IMG_DIR') }}/{{ env('IMG_USER_DIR') }}/{{ Auth::user()->gambar }}" class="img-circle" alt="User Image">--}}
            </div>
            <div class="pull-left info">
                <p>{{ Auth::user()->fullname }}</p>
                {{--<a href="#"><i class="fa fa-circle text-success"></i> Online</a>--}}
            </div>
        </div>
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            <li>
                <a href="/"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a>
            </li>

            @if(Auth::user()->can(['lihat_siswa', 'tambah_siswa']))
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-child"></i>
                        <span>Siswa</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        @if(Auth::user()->can('tambah_siswa'))
                            <li><a href="/siswa/create"><i class="fa fa-circle-o"></i> Tambah Siswa Baru</a></li>
                        @endif
                        @if(Auth::user()->can('lihat_siswa'))
                            <li><a href="/siswa"><i class="fa fa-circle-o"></i> Daftar Siswa</a></li>
                        @endif
                    </ul>
                </li>
            @endif

            @if(Auth::user()->can(['bayar_iuran', 'lihat_iuran']))
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-money"></i>
                        <span>Iuran Siswa</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        @if(Auth::user()->can('bayar_iuran'))
                            <li><a href="/iuran/create"><i class="fa fa-circle-o"></i> Bayar Iuran</a></li>
                        @endif
                        @if(Auth::user()->can('lihat_iuran'))
                            <li><a href="/iuran"><i class="fa fa-circle-o"></i> Daftar Pembayaran</a></li>
                        @endif
                    </ul>
                </li>
            @endif

            @if(Auth::user()->can(['daftar_penunggak','laporan_daftarsiswa', 'laporan_pembayaraniuran']))
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-bar-chart-o"></i> <span>Laporan</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    @if(Auth::user()->can('daftar_penunggak'))
                        <li><a href="/laporan/daftarpenunggak"><i class="fa fa-circle-o"></i> Daftar Penunggak</a></li>
                    @endif

                    @if(Auth::user()->can('laporan_daftarsiswa'))
                        <li>
                            <a href="#"><i class="fa fa-circle-o"></i> Laporan Siswa
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="/laporan/daftarsiswa"><i class="fa fa-circle-o"></i> Keseluruhan</a></li>
                                <li><a href="/laporan/siswaperprogram"><i class="fa fa-circle-o"></i> Per Program Studi</a></li>
                                <li><a href="/laporan/siswaperkelas"><i class="fa fa-circle-o"></i> Per Kelas</a></li>

                            </ul>
                        </li>
                    @endif

                    @if(Auth::user()->can('laporan_pembayaraniuran'))
                        <li><a href="/laporan/daftarpembayaran"><i class="fa fa-circle-o"></i> Pembayaran Iuran</a></li>
                    @endif
                </ul>
            </li>
            @endif

            @if(Auth::user()->can(['lihat_tarif', 'lihat_mulaiiuran', 'lihat_pengesah', 'lihat_tahun', 'lihat_program', 'lihat_kelas']))
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-cog"></i>
                        <span>Pengaturan</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        @if(Auth::user()->can('lihat_tarif'))
                            <li><a href="/pengaturan/tarif"><i class="fa fa-circle-o"></i> Tarif Iuran</a></li>
                        @endif
                        @if(Auth::user()->can('lihat_mulaiiuran'))
                            <li><a href="/pengaturan/mulaiiuran"><i class="fa fa-circle-o"></i> Mulai Iuran Siswa</a></li>
                        @endif
                        @if(Auth::user()->can('lihat_tahun'))
                            <li><a href="/pengaturan/tahun"><i class="fa fa-circle-o"></i> Tahun Ajaran</a></li>
                        @endif
                        @if(Auth::user()->can('lihat_program'))
                            <li><a href="/pengaturan/program"><i class="fa fa-circle-o"></i> Program</a></li>
                        @endif
                        @if(Auth::user()->can('lihat_kelas'))
                            <li><a href="/pengaturan/kelas"><i class="fa fa-circle-o"></i> Kelas</a></li>
                        @endif
                    </ul>
                </li>
            @endif

            @if(Auth::user()->can(['lihat_user', 'tambah_user']))
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-user"></i>
                        <span>Pengguna Aplikasi</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        @if(Auth::user()->can('tambah_user'))
                            <li><a href="/pengguna/create"><i class="fa fa-circle-o"></i> Tambah Pengguna</a></li>
                        @endif
                        @if(Auth::user()->can('lihat_user'))
                            <li><a href="/pengguna"><i class="fa fa-circle-o"></i> Daftar Pengguna</a></li>
                        @endif
                    </ul>
                </li>
            @endif

            @if(Auth::user()->can(['lihat_grup', 'tambah_grup']))
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-users"></i>
                        <span>Grup Pengguna</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        @if(Auth::user()->can('tambah_grup'))
                            <li><a href="/grup/create"><i class="fa fa-circle-o"></i> Tambah Grup Baru</a></li>
                        @endif
                        @if(Auth::user()->can('lihat_grup'))
                            <li><a href="/grup"><i class="fa fa-circle-o"></i> Daftar Grup Pengguna</a></li>
                        @endif
                    </ul>
                </li>
            @endif

            <li class="header">TOOLS</li>
            @if(Auth::user()->can('lihat_identitas'))
                <li><a href="/identitas"><i class="fa fa-circle-o text-green"></i> <span>Identitas</span></a></li>
            @endif
            @if(Auth::user()->can('lihat_pengesah'))
                <li><a href="/pengesah"><i class="fa fa-circle-o text-red"></i> <span>Pengesah</span></a></li>
            @endif
            <li><a href="gantipassword"><i class="fa fa-circle-o text-yellow"></i> <span>Ganti Password</span></a></li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
