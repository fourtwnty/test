<?php
/**
 * Created by PhpStorm.
 * User: ocol
 * Date: 08/11/16
 * Time: 0:54
 */
?>

<!-- Header Navbar: style can be found in header.less -->
<nav class="navbar navbar-static-top">
    <!-- Sidebar toggle button-->
    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
    </a>

    <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
            <!-- User Account: style can be found in dropdown.less -->
            <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
{{--                    <img src="/{{ env('IMG_DIR') }}/{{ env('IMG_USER_DIR') }}/{{ Auth::user()->gambar }}" class="user-image" alt="User Image">--}}
                    <span><i class="fa fa-user-circle fa-lg" aria-hidden="true"></i></span>
                    <span class="hidden-xs">{{ Auth::user()->fullname }}</span>
                </a>
                <ul class="dropdown-menu">
                    <!-- User image -->
                    <li class="user-header">
                        <span><i class="fa fa-user-circle fa-5x" style="color: #ffffff;"></i></span>
{{--                        <img src="/{{ env('IMG_DIR') }}/{{ env('IMG_USER_DIR') }}/{{ Auth::user()->gambar }}" class="img-circle" alt="User Image">--}}

                        <p>
                            {{ Auth::user()->fullname }} - {{ Auth::user()->role()->display_name }}
                            <small>User since {{Auth::user()->created_at->format('M')}}. {{Auth::user()->created_at->format('Y')}}</small>
                        </p>
                    </li>
                    <!-- Menu Footer-->
                    <li class="user-footer">
                        <div class="pull-left">
                            <a href="gantipassword" class="btn btn-default btn-flat">Ganti Password</a>
                        </div>
                        <div class="pull-right">
                            <a href="{{ url('/logout') }}" class="btn btn-default btn-flat"
                               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                Logout
                            </a>
                            <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </div>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
</nav>
