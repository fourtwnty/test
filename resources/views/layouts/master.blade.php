<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{{ config('app.name') }} | @yield('title_name')</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="/theme/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="/font-awesome-4.7.0/css/font-awesome.min.css">

    @yield('header_top')

    <!-- Theme style -->
    <link rel="stylesheet" href="/theme/dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="/theme/dist/css/skins/_all-skins.min.css">
    <!-- iCheck -->

    <!-- Pace style -->
    <link rel="stylesheet" href="/theme/plugins/pace/pace.min.css">

    @yield('header_add')

    <link rel="stylesheet" href="/css/custom.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="/js/html5shiv.min.js"></script>
    <script src="/js/respond.min.js"></script>
    <!--<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>-->
    <!--<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>-->
    <![endif]-->
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    @include('layouts.header')
    @include('layouts.sidebar')

            <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1 id="header-title">
                {{--SMPN 1--}}
                {{--<small>{{ date('d F Y') }}</small>--}}
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                @yield('active_page')
            </ol>
        </section>
        @yield('content')
    </div>

    @include('layouts.footer')
</div>
<!-- ./wrapper -->

<!-- jQuery 2.2.3 -->
<script src="/theme/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- jQuery UI 1.11.4 -->
{{--<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>--}}
<script src="/jquery-ui-1.12.1.custom/jquery-ui.min.js"></script>

<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
    $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.6 -->
<script src="/theme/bootstrap/js/bootstrap.min.js"></script>
<!-- PACE -->
<script src="/theme/plugins/pace/pace.min.js"></script>

@yield('footer_add')

<!-- AdminLTE App -->
<script src="/theme/dist/js/app.min.js"></script>
<script src="/js/functions.js"></script>

@yield('custom_script')

<script>
    $(function() {
        //var path = window.location.href;
        //
        //$('nav a').each(function () {
        //    if (this.href === path) {
        //        $(this).addClass('active');
        //    }
        //});

        var ada = false;
        $('.treeview-menu a').each(function () {
            if (this.href === location.href) {
                $(this).parent('li').addClass('active');
                $(this).parent('li').parent('ul').parent('li').addClass('active');
            }
        });

//        $('.treeview-menu a[href~="' + location.href + '"]').parent('li').addClass('active');
    });
</script>
</body>
</html>
