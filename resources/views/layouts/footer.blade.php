<?php
/**
 * Created by PhpStorm.
 * User: ocol
 * Date: 08/11/16
 * Time: 0:54
 */
?>

<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Version</b> {{config('app.version')}}
    </div>
    <strong>Copyright &copy; 2016-{{ date('Y') }} <a href="http://ilkom.unila.ac.id">Computer Science UNIVERSITY of LAMPUNG</a>.</strong> All rights
    reserved.
</footer>
