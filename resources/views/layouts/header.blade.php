<?php
/**
 * Created by PhpStorm.
 * User: ocol
 * Date: 08/11/16
 * Time: 0:53
 */
?>

<header class="main-header">
    <!-- Logo -->
    <a href="/" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>{{ str_split(strtoupper(config('app.name')))[0] }}</b>{{ str_split(strtoupper(config('app.name')))[2] }}</span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>{{ config('app.name') }}</b></span>
    </a>
    @include('layouts.navbar')
</header>
