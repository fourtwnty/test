<?php

use App\Models\Pengesah;
use Illuminate\Database\Seeder;

class PengesahTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $pengesahs = [
            // User
            'kepala_sekolah' => [
                'name'=>'kepala_sekolah',
                'display_name'=>'Kepala Royesta Lab',
                'nilai'=>'Roy Royesta, S.Kom',
                'nip'=>'197112271995122012'
            ],
            'bendahara_komite' => [
                'name'=>'bendahara_komite',
                'display_name'=>"Bendahara Komite & Osis",
                'nilai'=>'Serly Mengkido, S.Sos',
                'nip'=>'197002042005022002'
            ],
        ];

        foreach($pengesahs as $pengesah) {
            $data = null;
            $data = Pengesah::where('name', $pengesah['name'])->first();
            if ($data == null) {
                $data = new Pengesah();
                $data->name = $pengesah['name'];
                $data->display_name = $pengesah['display_name'];
                $data->nilai = $pengesah['nilai'];
                $data->nip = $pengesah['nip'];
                $data->save();
            }
        }
    }
}
