<?php

use App\Models\Bulan;
use Illuminate\Database\Seeder;

class BulanTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $bulans = [
            'Juli' => [
                'name' => 'Juli',
            ],
            'Agustus' => [
                'name' => 'Agustus',
            ],
            'September' => [
                'name' => 'September',
            ],
            'Oktober' => [
                'name' => 'Oktober',
            ],
            'November' => [
                'name' => 'November',
            ],
            'Desember' => [
                'name' => 'Desember',
            ],
            'Januari' => [
                'name' => 'Januari',
            ],
            'Februari' => [
                'name' => 'Februari',
            ],
            'Maret' => [
                'name' => 'Maret',
            ],
            'April' => [
                'name' => 'April',
            ],
            'Mei' => [
                'name' => 'Mei',
            ],
            'Juni' => [
                'name' => 'Juni',
            ],
        ];

        foreach($bulans as $bulan) {
            $data = null;
            $data = Bulan::where('name', $bulan['name'])->first();
            if ($data == null) {
                $data = new Bulan;
                $data->name = $bulan['name'];
                $data->save();
            }
        }
    }
}
