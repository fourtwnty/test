<?php

use App\Models\Permission;
use Illuminate\Database\Seeder;
use App\Models\Role;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $ownerRole = Role::where('name', 'owner')->first();
        $adminRole = Role::where('name', 'admin')->first();

        $permissions = [
            // Identitas
            'lihat_identitas' => [
                'name' => 'lihat_identitas',
                'display_name' => 'Lihat Identitas Sekolah',
                'description' => 'Melihat Identitas Sekolah'
            ],
            'ubah_identitas' => [
                'name' => 'ubah_identitas',
                'display_name' => 'Ubah Identitas Sekolah',
                'description' => 'Mengubah Identitas Sekolah'
            ],

            // User
            'lihat_user' => [
                'name' => 'lihat_user',
                'display_name' => 'Lihat Pengguna',
                'description' => 'Melihat Daftar Pengguna Aplikasi'
            ],
            'tambah_user' => [
                'name' => 'tambah_user',
                'display_name' => 'Tambah Pengguna',
                'description' => 'Menambah Pengguna Aplikasi Baru'
            ],
            'ubah_user' => [
                'name' => 'ubah_user',
                'display_name' => 'Ubah Data Pengguna',
                'description' => 'Mengubah Data Pengguna Aplikasi'
            ],
            'hapus_user' => [
                'name' => 'hapus_user',
                'display_name' => 'Hapus Data Pengguna',
                'description' => 'Menghapus Data Pengguna Aplikasi'
            ],

            // GRUP

            'lihat_grup' => [
                'name' => 'lihat_grup',
                'display_name' => 'Lihat Grup Pengguna',
                'description' => 'Melihat Daftar Grup Pengguna Aplikasi'
            ],
            'tambah_grup' => [
                'name' => 'tambah_grup',
                'display_name' => 'Tambah Grup Pengguna',
                'description' => 'Menambah Grup Pengguna Aplikasi Baru'
            ],
            'ubah_grup' => [
                'name' => 'ubah_grup',
                'display_name' => 'Ubah Data Grup Pengguna',
                'description' => 'Mengubah Data Grup Pengguna Aplikasi'
            ],
            'hapus_grup' => [
                'name' => 'hapus_grup',
                'display_name' => 'Hapus Data Grup Pengguna',
                'description' => 'Menghapus Data Grup Pengguna Aplikasi'
            ],

            // Owner

            'lihat_owner' => [
                'name' => 'lihat_owner',
                'display_name' => 'Lihat Grup Pembuat Software',
                'description' => 'Melihat Grup Pembuat Software'
            ],

            // Siswa

            'lihat_siswa' => [
                'name' => 'lihat_siswa',
                'display_name' => 'Lihat Siswa',
                'description' => 'Melihat Daftar Siswa'
            ],
            'tambah_siswa' => [
                'name' => 'tambah_siswa',
                'display_name' => 'Tambah Siswa Baru',
                'description' => 'Menambah Siswa Baru'
            ],
            'ubah_siswa' => [
                'name' => 'ubah_siswa',
                'display_name' => 'Ubah Siswa',
                'description' => 'Mengubah Data Siswa'
            ],
            'hapus_siswa' => [
                'name' => 'hapus_siswa',
                'display_name' => 'Hapus Siswa',
                'description' => 'Menghapus Data Siswa'
            ],

            // tarif
            'lihat_tarif' => [
                'name' => 'lihat_tarif',
                'display_name' => 'Lihat Tarif Iuran',
                'description' => 'Melihat Tarif Iuran'
            ],
            'tambah_tarif' => [
                'name' => 'tambah_tarif',
                'display_name' => 'Tambah Tarif Iuran',
                'description' => 'Menambah Tarif Iuran Baru'
            ],
            'ubah_tarif' => [
                'name' => 'ubah_tarif',
                'display_name' => 'Ubah Tarif Iuran',
                'description' => 'Mengubah Data Tarif Iuran'
            ],
            'hapus_tarif' => [
                'name' => 'hapus_tarif',
                'display_name' => 'Hapus Tarif Iuran',
                'description' => 'Menghapus Data Tarif Iuran'
            ],

            // mulai iuran
            'lihat_mulaiiuran' => [
                'name' => 'lihat_mulaiiuran',
                'display_name' => 'Lihat Mulai Iuran Siswa',
                'description' => 'Melihat Data Mulai Iuran Siswa'
            ],
            'tambah_mulaiiuran' => [
                'name' => 'tambah_mulaiiuran',
                'display_name' => 'Tambah  Mulai Iuran Siswa',
                'description' => 'Menambah Data Mulai Iuran Siswa Baru'
            ],
            'ubah_mulaiiuran' => [
                'name' => 'ubah_mulaiiuran',
                'display_name' => 'Ubah Mulai Iuran Siswa',
                'description' => 'Mengubah Data Mulai Iuran Siswa'
            ],
//            'hapus_mulaiiuran' => [
//                'name' => 'hapus_mulaiiuran',
//                'display_name' => 'Hapus Mulai Iuran Siswa',
//                'description' => 'Menghapus Data Mulai Iuran Siswa'
//            ],

            // Iuran
            'lihat_iuran' => [
                'name' => 'lihat_iuran',
                'display_name' => 'Lihat Pembayaran Iuran',
                'description' => 'Melihat Daftar Pembayaran Iuran'
            ],
            'bayar_iuran' => [
                'name' => 'bayar_iuran',
                'display_name' => 'Bayar Iuran',
                'description' => 'Membayara Iuran Siswa'
            ],
            'hapus_iuran' => [
                'name' => 'hapus_iuran',
                'display_name' => 'Hapus Iuran',
                'description' => 'Menghapus Iuran Siswa'
            ],
            'cetak_iuran' => [
                'name' => 'cetak_iuran',
                'display_name' => 'Cetak Pembayaran Iuran',
                'description' => 'Mencetak Nota Pembayaran Iuran'
            ],

            'daftar_penunggak' => [
                'name' => 'daftar_penunggak',
                'display_name' => 'Lihat Daftar Penunggak',
                'description' => 'Melihat Daftar Penunggak'
            ],
            'cetak_surat_tagihan' => [
                'name' => 'cetak_surat_tagihan',
                'display_name' => 'Cetak Surat Tagihan Penunggak',
                'description' => 'Mencetak Surat Tagihan untuk Penunggak'
            ],
            'cetak_laporan_penunggak' => [
                'name' => 'cetak_laporan_penunggak',
                'display_name' => 'Cetak Laporan Penunggak',
                'description' => 'Mencetak Laporan Penunggak'
            ],
//            'buku_iuran' => [
//                'name' => 'buku_iuran',
//                'display_name' => 'Cetak Buku Iuran Komite',
//                'description' => 'Mencetak Buku Iuran Komite'
//            ],


            // tahun ajaran
            'lihat_tahun' => [
                'name' => 'lihat_tahun',
                'display_name' => 'Lihat Tahun Ajaran',
                'description' => 'Melihat Daftar Tahun Ajaran'
            ],
            'tambah_tahun' => [
                'name' => 'tambah_tahun',
                'display_name' => 'Tambah Tahun Ajaran',
                'description' => 'Menambahkan Data Tahun Ajaran Baru'
            ],
            'ubah_tahun' => [
                'name' => 'ubah_tahun',
                'display_name' => 'Ubah Tahun Ajaran',
                'description' => 'Mengubah Data Tahun Ajaran'
            ],
            'hapus_tahun' => [
                'name' => 'hapus_tahun',
                'display_name' => 'Hapus Tahun Ajaran',
                'description' => 'Menghapus Data Tahun Ajaran'
            ],

            // program studi
            'lihat_program' => [
                'name' => 'lihat_program',
                'display_name' => 'Lihat Program Studi',
                'description' => 'Melihat Daftar Program Studi'
            ],
            'tambah_program' => [
                'name' => 'tambah_program',
                'display_name' => 'Tambah Program Studi',
                'description' => 'Menambahkan Data Program Studi Baru'
            ],
            'ubah_program' => [
                'name' => 'ubah_program',
                'display_name' => 'Ubah Program Studi',
                'description' => 'Mengubah Data Program Studi'
            ],
            'hapus_program' => [
                'name' => 'hapus_program',
                'display_name' => 'Hapus Program Studi',
                'description' => 'Menghapus Data Program Studi'
            ],

            // kelas
            'lihat_kelas' => [
                'name' => 'lihat_kelas',
                'display_name' => 'Lihat Kelas',
                'description' => 'Melihat Daftar Kelas'
            ],
            'tambah_kelas' => [
                'name' => 'tambah_kelas',
                'display_name' => 'Tambah Kelas',
                'description' => 'Menambahkan Data Kelas Baru'
            ],
            'ubah_kelas' => [
                'name' => 'ubah_kelas',
                'display_name' => 'Ubah Kelas',
                'description' => 'Mengubah Data Kelas'
            ],
            'hapus_kelas' => [
                'name' => 'hapus_kelas',
                'display_name' => 'Hapus Kelas',
                'description' => 'Menghapus Data Kelas'
            ],

            // laporan
            'laporan_daftarsiswa' => [
                'name' => 'laporan_daftarsiswa',
                'display_name' => 'Lihat Laporan Siswa',
                'description' => 'Melihat Laporan Daftar Siswa'
            ],


            'laporan_pembayaraniuran' => [
                'name' => 'laporan_pembayaraniuran',
                'display_name' => 'Lihat Laporan Iuran',
                'description' => 'Melihat Laporan Pembayaran Iuran'
            ],

            // lain - lain
            'lihat_pengesah' => [
                'name' => 'lihat_pengesah',
                'display_name' => 'Lihat Daftar Pengesah',
                'description' => 'Melihat Daftar Pengesah Laporan'
            ],
            'atur_pengesah' => [
                'name' => 'atur_pengesah',
                'display_name' => 'Atur Data Pengesah',
                'description' => 'Mengatur Data Pengesah Laporan'
            ],
        ];

        foreach($permissions as $perm) {
            $access = null;
            $access = Permission::where('name', $perm['name'])->first();
            if ($access == null) {
                $access = new Permission;
                $access->name = $perm['name'];
                $access->display_name = $perm['display_name'];
                $access->description = $perm['description'];
                $access->save();

                $ownerRole->attachPermission($access);

                if ($perm['name'] != 'lihat_owner') {
                    $adminRole->attachPermission($access);
                }
            }
        }

//        DB::table('permissions')->delete();

        // GRUP DAN PENGGUNA
//        $lihatpengguna = Permission::where('name', 'lihat_pengguna')->first();
//        if ($lihatpengguna == null) {
//            $lihatpengguna = new Permission;
//            $lihatpengguna->name = 'lihat_pengguna';
//            $lihatpengguna->display_name = 'Lihat Pengguna';
//            $lihatpengguna->description = 'Melihat Daftar Pengguna Aplikasi';
//            $lihatpengguna->save();
//
//            $ownerRole->attachPermission($lihatpengguna);
//            $adminRole->attachPermission($lihatpengguna);
//        }
//
//        $tambahpengguna = Permission::where('name', 'add_user')->first();
//        if ($tambahpengguna == null) {
//            $tambahpengguna = new Permission;
//            $tambahpengguna->name = 'add_user';
//            $tambahpengguna->display_name = 'Tambah Pengguna';
//            $tambahpengguna->description = 'Menambah Pengguna Baru';
//            $tambahpengguna->save();
//
//            $ownerRole->attachPermission($tambahpengguna);
//            $adminRole->attachPermission($tambahpengguna);
//        }
//
//        $ubahpengguna = Permission::where('name', 'change_user')->first();
//        if ($ubahpengguna == null) {
//            $ubahpengguna = new Permission;
//            $ubahpengguna->name = 'change_user';
//            $ubahpengguna->display_name = 'Ubah Pengguna';
//            $ubahpengguna->description = 'Mengubah Pengguna Aplikasi';
//            $ubahpengguna->save();
//
//            $ownerRole->attachPermission($ubahpengguna);
//            $adminRole->attachPermission($ubahpengguna);
//        }
//
//        $hapuspengguna = Permission::where('name', 'hapus_pengguna')->first();
//        if ($hapuspengguna == null) {
//            $hapuspengguna = new Permission;
//            $hapuspengguna->name = 'hapus_pengguna';
//            $hapuspengguna->display_name = 'Hapus Pengguna';
//            $hapuspengguna->description = 'Menghapus Pengguna Aplikasi';
//            $hapuspengguna->save();
//
//            $ownerRole->attachPermission($hapuspengguna);
//            $adminRole->attachPermission($hapuspengguna);
//        }
//
//        $lihatgrup = Permission::where('name', 'lihat_grup')->first();
//        if ($lihatgrup == null) {
//            $lihatgrup = new Permission;
//            $lihatgrup->name = 'lihat_grup';
//            $lihatgrup->display_name = 'Lihat Grup Pengguna';
//            $lihatgrup->description = 'Melihat Grup Pengguna Aplikasi';
//            $lihatgrup->save();
//
//            $ownerRole->attachPermission($lihatgrup);
//            $adminRole->attachPermission($lihatgrup);
//        }
//
//        $tambahgrup = Permission::where('name', 'tambah_grup')->first();
//        if ($tambahgrup == null) {
//            $tambahgrup = new Permission;
//            $tambahgrup->name = 'tambah_grup';
//            $tambahgrup->display_name = 'Tambah Grup Pengguna';
//            $tambahgrup->description = 'Menambah Grup Pengguna Baru';
//            $tambahgrup->save();
//
//            $ownerRole->attachPermission($tambahgrup);
//            $adminRole->attachPermission($tambahgrup);
//        }
//
//        $ubahgrup = Permission::where('name', 'ubah_grup')->first();
//        if ($ubahgrup == null) {
//            $ubahgrup = new Permission;
//            $ubahgrup->name = 'ubah_grup';
//            $ubahgrup->display_name = 'Ubah Grup Pengguna';
//            $ubahgrup->description = 'Mengubah Grup Pengguna Aplikasi';
//            $ubahgrup->save();
//
//            $ownerRole->attachPermission($ubahgrup);
//            $adminRole->attachPermission($ubahgrup);
//        }
//
//        $hapusgrup = Permission::where('name', 'hapus_grup')->first();
//        if ($hapusgrup == null) {
//            $hapusgrup = new Permission;
//            $hapusgrup->name = 'hapus_grup';
//            $hapusgrup->display_name = 'Hapus Grup Pengguna';
//            $hapusgrup->description = 'Menghapus Grup Pengguna Aplikasi';
//            $hapusgrup->save();
//
//            $ownerRole->attachPermission($hapusgrup);
//            $adminRole->attachPermission($hapusgrup);
//        }
//
//        $lihatowner = Permission::where('name', 'lihat_owner')->first();
//        if ($lihatowner == null) {
//            $lihatowner = new Permission;
//            $lihatowner->name = 'lihat_owner';
//            $lihatowner->display_name = 'Lihat Grup Pembuat Software';
//            $lihatowner->description = 'Melihat Grup Pembuat Software';
//            $lihatowner->save();
//
//            $ownerRole->attachPermission($lihatowner);
//        }
//
//        // GRUP DAN PENGGUNA - END
//
//
//        /* SISWA */
//
//        $lihatSiswa = Permission::where('name', 'lihat_siswa')->first();
//        if ($lihatSiswa == null) {
//            $lihatSiswa = new Permission;
//            $lihatSiswa->name = 'lihat_siswa';
//            $lihatSiswa->display_name = 'Lihat Siswa';
//            $lihatSiswa->description = 'Melihat Daftar Siswa';
//            $lihatSiswa->save();
//
//            $ownerRole->attachPermission($lihatSiswa);
//            $adminRole->attachPermission($lihatSiswa);
//        }
//
//        $tambahSiswa = Permission::where('name', 'tambah_siswa')->first();
//        if ($tambahSiswa == null) {
//            $tambahSiswa = new Permission;
//            $tambahSiswa->name = 'tambah_siswa';
//            $tambahSiswa->display_name = 'Tambah Siswa';
//            $tambahSiswa->description = 'Menambah Siswa Baru';
//            $tambahSiswa->save();
//
//            $ownerRole->attachPermission($tambahSiswa);
//            $adminRole->attachPermission($tambahSiswa);
//        }
//
//        $ubahsiswa = Permission::where('name', 'tambah_siswa')->first();
//        if ($ubahsiswa == null) {
//            $ubahsiswa = new Permission;
//            $ubahsiswa->name = 'tambah_siswa';
//            $ubahsiswa->display_name = 'Tambah Siswa';
//            $ubahsiswa->description = 'Menambah Siswa Baru';
//            $ubahsiswa->save();
//
//            $ownerRole->attachPermission($ubahsiswa);
//            $adminRole->attachPermission($ubahsiswa);
//        }
//
//        $ubahsiswa = Permission::create([
//            'name' => 'ubah_siswa',
//            'display_name' => 'Ubah Siswa',
//            'description' => 'Mengubah Data Siswa', // optional
//        ]);
//
//        $hapusSiswa = Permission::create([
//            'name' => 'hapus_siswa',
//            'display_name' => 'Hapus Siswa',
//            'description' => 'Menghapus Data Siswa', // optional
//        ]);
//
//        $lihatiuran = Permission::create([
//            'name' => 'lihat_iuran',
//            'display_name' => 'Lihat Pembayaran Iuran',
//            'description' => 'Melihat Daftar Pembayaran Iuran', // optional
//        ]);
//
//        $bayarIuran = Permission::create([
//            'name' => 'bayar_iuran',
//            'display_name' => 'Bayar Iuran',
//            'description' => 'Membayar Iuran Siswa', // optional
//        ]);
//
////        $ubahIuran = Permission::create([
////            'name' => 'ubah_iuran',
////            'display_name' => 'Ubah Iuran',
////            'description' => 'Koreksi Ubah Iuran Siswa', // optional
////        ]);
//
//        $hapusIuran = Permission::create([
//            'name' => 'hapus_iuran',
//            'display_name' => 'Hapus Iuran',
//            'description' => 'Koreksi Hapus Iuran Siswa', // optional
//        ]);
//
//        $daftarPenunggak = Permission::create([
//            'name' => 'daftar_penunggak',
//            'display_name' => 'Lihat Daftar Penunggak',
//            'description' => 'Melihat Daftar Penunggak', // optional
//        ]);
//
//        $bukuiuran = Permission::create([
//            'name' => 'buku_iuran',
//            'display_name' => 'Cetak Buku Iuran Komite',
//            'description' => 'Melihat dan Mencetak Buku Iuran Komite', // optional
//        ]);
//        $laporanDaftarSiswa = Permission::create([
//            'name' => 'laporan_daftarsiswa',
//            'display_name' => 'Lihat Laporan Siswa',
//            'description' => 'Melihat Laporan Daftar Siswa', // optional
//        ]);
//
//        $laporanPembayaranIuran = Permission::create([
//            'name' => 'laporan_pembayaraniuran',
//            'display_name' => 'Lihat Laporan Iuran',
//            'description' => 'Melihat Laporan Pembayaran Iuran', // optional
//        ]);
//
//        $lihatTarif = Permission::create([
//            'name' => 'lihat_tarif',
//            'display_name' => 'Lihat Tarif',
//            'description' => 'Melihat Tarif Iuran', // optional
//        ]);
//
//        $tambahTarif = Permission::create([
//            'name' => 'tambah_tarif',
//            'display_name' => 'Tambah Tarif',
//            'description' => 'Menambah Tarif Iuran Baru', // optional
//        ]);
//
//        $ubahTarif = Permission::create([
//            'name' => 'ubah_tarif',
//            'display_name' => 'Mengubah Tarif',
//            'description' => 'Mengubah Tarif Iuran', // optional
//        ]);
//
//        $hapusTarif = Permission::create([
//            'name' => 'hapus_tarif',
//            'display_name' => 'Menghapus Tarif',
//            'description' => 'Menghapus Tarif Iuran', // optional
//        ]);
//
//        $lihatMulaiIuran = Permission::create([
//            'name' => 'lihat_mulaiiuran',
//            'display_name' => 'Lihat Mulai Iuran Siswa',
//            'description' => 'Melihat Daftar Mulai Iuran Siswa', // optional
//        ]);
//
//        $tambahMulaiIuran = Permission::create([
//            'name' => 'tambah_mulaiiuran',
//            'display_name' => 'Tambah Mulai Iuran Siswa',
//            'description' => 'Menambah Mulai Iuran Siswa', // optional
//        ]);
//
//        $ubahMulaiIuran = Permission::create([
//            'name' => 'ubah_mulaiiuran',
//            'display_name' => 'Ubah Mulai Iuran Siswa',
//            'description' => 'Mengubah Mulai Iuran Siswa', // optional
//        ]);
//
//        $hapusMulaiIuran = Permission::create([
//            'name' => 'hapus_mulaiiuran',
//            'display_name' => 'Hapus Mulai Iuran Siswa',
//            'description' => 'Menghapus Mulai Iuran Siswa', // optional
//        ]);
//
//        $lihatpengguna = Permission::create([
//            'name' => 'lihat_pengguna',
//            'display_name' => 'Lihat Pengguna',
//            'description' => 'Melihat Daftar Pengguna Aplikasi', // optional
//        ]);
//
//        $tambahpengguna = Permission::create([
//            'name' => 'tambah_pengguna',
//            'display_name' => 'Tambah Pengguna',
//            'description' => 'Tambah Pengguna Baru', // optional
//        ]);
//
//        $ubahpengguna = Permission::create([
//            'name' => 'ubah_pengguna',
//            'display_name' => 'Ubah Pengguna',
//            'description' => 'Mengubah Data Pengguna', // optional
//        ]);
//
//        $hapuspengguna = Permission::create([
//            'name' => 'hapus_pengguna',
//            'display_name' => 'Hapus Pengguna',
//            'description' => 'Menghapus Data Pengguna', // optional
//        ]);
//
//        $lihatGrup = Permission::create([
//            'name' => 'lihat_grup',
//            'display_name' => 'Lihat Grup',
//            'description' => 'Melihat Daftar Grup Pengguna', // optional
//        ]);
//
//        $tambahGrup = Permission::create([
//            'name' => 'tambah_grup',
//            'display_name' => 'Tambah Grup',
//            'description' => 'Menambah Grup Pengguna Baru', // optional
//        ]);
//
//        $ubahGrup = Permission::create([
//            'name' => 'ubah_grup',
//            'display_name' => 'Ubah Grup',
//            'description' => 'Mengubah Data Grup Pengguna', // optional
//        ]);
//
//        $hapusGrup = Permission::create([
//            'name' => 'hapus_grup',
//            'display_name' => 'Hapus Grup',
//            'description' => 'Menghapus Data Grup Pengguna', // optional
//        ]);
//
//        $lihatOwner = Permission::create([
//            'name' => 'lihat_owner',
//            'display_name' => 'Lihat Owner Project',
//            'description' => 'Melihat data Grup atau Pengguna Owner Project', // optional
//        ]);
//
//
//        $lihatpengesah = Permission::create([
//            'name' => 'lihat_pengesah',
//            'display_name' => 'Lihat Daftar Pengesah',
//            'description' => 'Melihat Daftar Pengesah', // optional
//        ]);
//
//        $aturpengesah = Permission::create([
//            'name' => 'atur_pengesah',
//            'display_name' => 'Atur Data Pengesah',
//            'description' => 'Mengatur Data Pengesah', // optional
//        ]);
//
//        $lihat_tahunajaran = Permission::create([
//            'name' => 'lihat_tahun',
//            'display_name' => 'Lihat Daftar Tahun Ajaran',
//            'description' => 'Melihat Daftar Tahun Ajaran', // optional
//        ]);
//
//        $tambah_tahunajaran = Permission::create([
//            'name' => 'tambah_tahun',
//            'display_name' => 'Tambah Tahun Ajaran Baru',
//            'description' => 'Menambah Tahun Ajaran Baru', // optional
//        ]);
//
//        $ubah_tahunajaran = Permission::create([
//            'name' => 'ubah_tahun',
//            'display_name' => 'Ubah Data Tahun Ajaran',
//            'description' => 'Mengubah Data Tahun Ajaran', // optional
//        ]);
//
//        $hapus_tahunajaran = Permission::create([
//            'name' => 'hapus_tahun',
//            'display_name' => 'Hapus Data Tahun Ajaran',
//            'description' => 'Menghapus Data Tahun Ajaran', // optional
//        ]);
//
//        $lihat_program = Permission::create([
//            'name' => 'lihat_program',
//            'display_name' => 'Lihat Daftar Program Studi',
//            'description' => 'Melihat Daftar Program Studi', // optional
//        ]);
//
//        $tambah_program = Permission::create([
//            'name' => 'tambah_program',
//            'display_name' => 'Tambah Program Studi Baru',
//            'description' => 'Menambahkan Program Studi Baru', // optional
//        ]);
//
//        $ubah_program = Permission::create([
//            'name' => 'ubah_program',
//            'display_name' => 'Ubah Data Program Studi',
//            'description' => 'Mengubah Data Program Studi', // optional
//        ]);
//
//        $hapus_program = Permission::create([
//            'name' => 'hapus_program',
//            'display_name' => 'Hapus Data Program Studi',
//            'description' => 'Menghapus Data Program Studi', // optional
//        ]);
//
//        $lihat_kelas = Permission::create([
//            'name' => 'lihat_kelas',
//            'display_name' => 'Lihat Daftar Kelas',
//            'description' => 'Melihat Daftar Kelas', // optional
//        ]);
//
//        $tambah_kelas = Permission::create([
//            'name' => 'tambah_kelas',
//            'display_name' => 'Tambah Kelas Baru',
//            'description' => 'Menambahkan Kelas Baru', // optional
//        ]);
//
//        $ubah_kelas = Permission::create([
//            'name' => 'ubah_kelas',
//            'display_name' => 'Ubah Data Kelas',
//            'description' => 'Mengubah Data Kelas', // optional
//        ]);
//
//        $hapus_kelas = Permission::create([
//            'name' => 'hapus_kelas',
//            'display_name' => 'Hapus Data Kelas',
//            'description' => 'Menghapus Data Kelas', // optional
//        ]);
//
//
//        // **** maintenance
//        $lihat_sistem_log = Permission::where('name', 'lihat_sistem_log')->first();
//        if ($lihat_sistem_log == null) {
//            $lihat_sistem_log = new Permission;
//            $lihat_sistem_log->name = 'lihat_sistem_log';
//            $lihat_sistem_log->display_name = 'Lihat Sistem Log';
//            $lihat_sistem_log->description = 'Melihat Sistem Log';
//            $lihat_sistem_log->save();
//
//            $ownerRole->attachPermission($lihat_sistem_log);
//        }
//
//        $bersihkan_sistem_log = Permission::where('name', 'bersihkan_sistem_log')->first();
//        if ($bersihkan_sistem_log == null) {
//            $bersihkan_sistem_log = new Permission;
//            $bersihkan_sistem_log->name = 'bersihkan_sistem_log';
//            $bersihkan_sistem_log->display_name = 'Bersihkan Sistem Log';
//            $bersihkan_sistem_log->description = 'Membersihkan semua data Sistem Log';
//            $bersihkan_sistem_log->save();
//
//            $ownerRole->attachPermission($bersihkan_sistem_log);
//        }
    }
}
