<?php

use App\Models\User;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $userAdmin = User::where('username', 'admin')->first();
        if($userAdmin == null) {
            $userAdmin = new User();
            $userAdmin->fullname = 'Administrator';
            $userAdmin->email = 'royroyesta54@gmail.com';
            $userAdmin->username = 'admin';
            $userAdmin->password = bcrypt('123456');
            $userAdmin->address = 'Jl. Elang Lrng. 3';
            $userAdmin->gsm = '0853-9403-7406';
            $userAdmin->phone = '0822-9936-6940';
            $userAdmin->gender = 'L';
            $userAdmin->description = 'User Administrator';
            $userAdmin->active = true;

            $userAdmin->save();
        }

        $userAdmin = User::where('username', 'ocol')->first();
        if($userAdmin == null) {
            $userAdmin = new User();
            $userAdmin->fullname = 'Bang Ocol';
            $userAdmin->email = 'royestalab@gmail.com';
            $userAdmin->username = 'ocol';
            $userAdmin->password = bcrypt('qwe123RTY!@#');
            $userAdmin->address = 'Jl. Elang Lrng. 3';
            $userAdmin->gsm = '0853-9403-7406';
            $userAdmin->phone = '0822-9936-6940';
            $userAdmin->gender = 'L';
            $userAdmin->description = 'User Bang Ocol';
            $userAdmin->active = true;

            $userAdmin->save();
        }
    }
}
