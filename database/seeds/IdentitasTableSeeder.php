<?php

use App\Models\Identitas;
use Illuminate\Database\Seeder;

class IdentitasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $identitas = Identitas::all();

//        $table->string('nama', 100);
//        $table->string('alamat', 200);
//        $table->string('telp', 20);
//        $table->string('fax', 20);
//        $table->string('logo', 100);
//        $table->string('dinas', 100);
//        $table->string('kota', 100);
//        $table->string('kelurahan', 100);
//        $table->string('kecamatan', 100);
//        $table->string('kabupaten', 100);

        if ($identitas->isEmpty()) {
            $identitas = new Identitas;
            $identitas->nama = 'Royesta Lab';
            $identitas->alamat = 'Jl. Elang Lrg. III';
            $identitas->telp = '0822-9936-940';
            $identitas->fax = '0822-9936-940';

            $alamat = public_path().'/'.env('IMG_DIR').'/'.env('IMG_IDENTITAS_DIR').'/'.'default'.'.png';

            $tujuan = public_path().'/'.env('IMG_DIR').'/'.env('IMG_IDENTITAS_DIR').'/'.'identitas'.'.png';

            $img = Image::make($alamat);

            // upload ke direktori yang sudah ditentukan
            $img->save($tujuan);

            $identitas->logo = 'identitas.png';
            $identitas->dinas = 'Dinas Pendidikan Dan Olah Raga';
            $identitas->kota = 'Palu';
            $identitas->kelurahan = 'Tatura Utara';
            $identitas->kecamatan = 'Tatura';
            $identitas->kabupaten = 'Palu';
            $identitas->save();
        }
    }

}
