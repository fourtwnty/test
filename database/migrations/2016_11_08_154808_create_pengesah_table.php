<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePengesahTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pengesah', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 100)->unique;
            $table->string('display_name', 200);
            $table->string('nilai', 200);
            $table->string('nip', 18)->unique;
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pengesah');
    }
}
