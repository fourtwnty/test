<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email')->unique();
            $table->string('fullname', 200);
            $table->string('username', 50)->unique();
            $table->string('password', 100);
            $table->string('address', 200)->default('');
            $table->string('gsm', 20)->default('');
            $table->string('phone', 20)->default('');
            $table->enum('gender', ['L', 'P'])->default('L');
            $table->string('description', 50)->default('');
            $table->boolean('active')->default(0);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
