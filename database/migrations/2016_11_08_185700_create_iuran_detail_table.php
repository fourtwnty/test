<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIuranDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('iuran_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->string('iuran_id', 13);
            $table->foreign('iuran_id')->references('id')->on('iuran')->onUpdate('cascade')->onDelete('cascade');


            $table->integer('tahun_id')->unsigned();
            $table->foreign('tahun_id')->references('id')->on('tahun_ajaran');

            $table->integer('bulan_id')->unsigned();
            $table->foreign('bulan_id')->references('id')->on('bulan');

            $table->integer('harga')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('iuran_detail');
    }
}
