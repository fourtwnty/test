<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMulaiIuranTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mulai_iuran', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('siswa_id')->unsigned()->unique;
            $table->foreign('siswa_id')->references('id')->on('siswa');

            $table->integer('tahun_id')->unsigned();
            $table->foreign('tahun_id')->references('id')->on('tahun_ajaran');

            $table->integer('bulan_id')->unsigned();
            $table->foreign('bulan_id')->references('id')->on('bulan');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mulai_iuran');
    }
}
