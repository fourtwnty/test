/**
 * Created by ocol on 10/11/16.
 */
$(document).ajaxStart(function() {
    Pace.restart();
});

$(document).ready(function () {

    document.getElementById("name").maxLength = 100;
    document.getElementById("display_name").maxLength = 200;
    document.getElementById("description").maxLength = 200;

    $('#name').focus();

    var inputHakAkses = $('select[name="permissions_grup[]"]').bootstrapDualListbox();

    $('#simpan').click(function() {
        var route = "/grup";
        var token = $('#token').val();

        var name = $('#name').val();
        if (name == '' || name == undefined) {
            alert('Nama Group User tidak boleh dikosongkan');
            $('#name').focus();
            return;
        }


        var display_name = $('#display_name').val();
        if (display_name == '' || display_name == undefined) {
            alert('Nama yang akan terlihat tidak boleh dikosongkan');
            $('#display_name').focus();
            return;
        }

        var description = $('#description').val();
        if (description == undefined) {
            description = '';
        }

        var permissions_grup = $('select[name="permissions_grup[]"]').val();
        if (permissions_grup == null || jQuery.isEmptyObject(permissions_grup)) {
            alert('Group User harus memiliki hak akses');
            $('select[name="permissions_grup[]"]').focus();
            return;
        }

        //console.log(permissions_grup);
        //
        //return;


        $.ajax({
            url: route,
            type: 'POST',
            headers: {'X-CSRF-TOKEN': token},
            dataType: 'json',
            data: {
                name: name,
                display_name: display_name,
                description: description,
                permissions_grup: permissions_grup,
                _token: token
            },
            error: function (res) {
                var errors = res.responseJSON;
                var pesan = '';
                $.each(errors, function (index, value) {
                    pesan += value + "\n";
                });

                alert(pesan);
            },
            success: function () {
                alert('Sukses Menyimpan Data');

                $('#name').val(null);
                $('#display_name').val(null);
                $('#description').val(null);
                $('select[name="permissions_grup[]"]').val(null);

                $('select[name="permissions_grup[]"]').bootstrapDualListbox('refresh');

                //console.log($('select[name="permissions_grup[]"]').val());

                $('#name').focus();
            }
        });
    });
});