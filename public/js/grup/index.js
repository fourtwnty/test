/**
 * Created by ocol on 10/11/16.
 */
$(document).ajaxStart(function() {
    Pace.restart();
});

$(document).ready(function () {
    var route = "/cekhakakses/ubah_grup";
    var bolehUbah;
    $.get(route, function (res) {
        bolehUbah = res;
    });

    var route = "/cekhakakses/hapus_grup";
    var bolehHapus;
    $.get(route, function (res) {
        bolehHapus = res;
    });


    $('#dataTableBuilder').DataTable({
        //scrollX: true,
        //scrollColapse:true,
        responsive: true,
        'ajax': {
            'url': '/grups',
        },
        'columnDefs': [{
            'targets': 3,
            'searchable': false,
            "orderable": false,
            "orderData": false,
            "orderDataType": false,
            "orderSequence": false,
            "sClass": "text-center col-lg-1 td-aksi",
            'render': function (data, type, full, meta) {
                var kembali = '';
                if (bolehUbah == true) {
                    kembali += '<button title="Ubah Data" class="btn btn-warning btn-flat" data-toggle="modal" data-target="#modalUbah" onclick="UbahClick(this);"><i class="fa fa-pencil-square-o fa-fw"></i> </button>';
                }
                if (bolehHapus == true) {
                    kembali += '<button title="Hapus Data" class="btn btn-danger btn-flat" data-toggle="modal" data-target="#modalHapus" onclick="HapusClick(this);"><i class="fa fa-trash fa-fw"></i> </button>';
                }

                return kembali;

            }
        },{
            'targets':0,
            'sClass': "col-lg-2"
        }, {
            'targets':1,
            'sClass': "col-lg-3"
        },{
            'targets':2,
            'sClass': "col-lg-6"
        }],
        'rowCallback': function (row, data, dataIndex) {
            if (bolehUbah == true) {
                $(row).find('button[class="btn btn-warning btn-flat"]').prop('value', data[3]);
            }
            if (bolehHapus == true) {
                $(row).find('button[class="btn btn-danger btn-flat"]').prop('value', data[3]);
            }

        }
    });

    $('select[name="permissions_grup[]"]').bootstrapDualListbox();

    document.getElementById("name").maxLength = 100;
    document.getElementById("display_name").maxLength = 200;
    document.getElementById("description").maxLength = 200;


});

function reloadTable() {
    var table = $('#dataTableBuilder').dataTable();
    table.cleanData;
    table.api().ajax.reload();
}

function HapusClick(btn) {
    $('#idHapus').val(btn.value);
}

$('#yakinhapus').click(function () {
    var token = $('#token').val();
    var id = $('#idHapus').val();
    var route = "/grup/" + id;

    $.ajax({
        url: route,
        headers: {'X-CSRF-TOKEN': token},
        type: 'DELETE',
        dataType: 'json',
        error: function (res) {
            var errors = res.responseJSON;
            var pesan = '';
            $.each(errors, function (index, value) {
                pesan += value + "\n";
            });

            alert(pesan);
        },
        success: function () {
            reloadTable();
            alert('Sukses Menghapus Data');
            $('#modalHapus').modal('toggle');
        }
    });
});

function UbahClick(btn) {
    route = "/grup/" + btn.value + "/edit";

    $.get(route, function (res) {
        $('#id').val(res.id);
        $('#name').val(res.name);
        $('#display_name').val(res.display_name);
        $('#description').val(res.description);

        $('select[name="permissions_grup[]"]').val(res.permissions_grup);

        $('select[name="permissions_grup[]"]').bootstrapDualListbox('refresh');

        $('#display_name').focus();

    });

}

$('#simpan').click(function () {
    var id = $('#id').val();
    var token = $('#token').val();
    var route = "/grup/" + id;

    var name = $('#name').val();
    if (name == '' || name == undefined) {
        alert('Nama Group User tidak boleh dikosongkan');
        $('#name').focus();
        return;
    }

    var display_name = $('#display_name').val();
    if (display_name == '' || display_name == undefined) {
        alert('Nama yang akan terlihat tidak boleh dikosongkan');
        $('#display_name').focus();
        return;
    }

    var description = $('#description').val();
    if (description == undefined) {
        description = '';
    }

    var permissions_grup = $('select[name="permissions_grup[]"]').val();
    if (permissions_grup == null || jQuery.isEmptyObject(permissions_grup)) {
        alert('Group User harus memiliki hak akses');
        $('select[name="permissions_grup[]"]').focus();
        return;
    }

    $.ajax({
        url: route,
        headers: {'X-CSRF-TOKEN': token},
        type: 'PUT',
        dataType: 'json',
        data: {
            name: name,
            display_name: display_name,
            description: description,
            permissions_grup: permissions_grup,
            _token: token
        },
        error: function (res) {
            var errors = res.responseJSON;
            var pesan = '';
            $.each(errors, function (index, value) {
                pesan += value + "\n";
            });

            alert(pesan);
        },
        success: function () {
            reloadTable();
            alert('Sukses Mengubah Data');
            $('#modalUbah').modal('toggle');
        }
    });
});