/**
 * Created by ocol on 08/11/16.
 */

$(document).ajaxStart(function() {
    Pace.restart();
    getPenggunaTerdaftar();
    getKelas();
    getNilaiProgramStudi();
    getPenunggak();
    getTotalSiswa();
});

function getPenggunaTerdaftar() {
    var route = "/getpenggunaterdaftar";;
    $.get(route, function (res) {
        var jumlah = res;
        $('#j-pengguna').html(jumlah);

        //console.log('p: ' + jumlah);
    });
}

function getPenunggak() {
    var route = "/gettotalpenunggak";;
    $.get(route, function (res) {
        var menunggak = res.menunggak;
        var lunas = res.lunas;

        var total = intVal(menunggak + lunas);
        var persenlunas = lunas * 100 / total;
        persenlunas = number_format(persenlunas, 2, ',', '');

        console.log(res);

        $('#j-penunggak').html(menunggak);

        $('#j-lunas').html(persenlunas + '<sup style="font-size: 20px">%</sup>');
    });
}

function getTotalSiswa() {
    var route = "/gettotalsiswa";;
    $.get(route, function (res) {
        var jumlah = res;
        $('#j-siswa').html(jumlah);
    });
}

function getNilaiProgramStudi() {
    var route = "/gettotalprodi";;
    $.get(route, function (res) {
        var jumlah = res;
        $('#j-program').html(jumlah);
    });
}
function getKelas() {
    var route = "/gettotalkelas";;
    $.get(route, function (res) {
        var jumlah = res;
        $('#j-kelas').html(jumlah);
    });
}