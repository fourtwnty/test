/**
 * Created by ocol on 10/11/16.
 */
$(document).ajaxStart(function() {
    Pace.restart();
});

$(document).ready(function () {
    var route = "/kelases";
    var inputTipe = $('#kelas_id');

    var list = document.getElementById("kelas_id");
    while (list.hasChildNodes()) {
        list.removeChild(list.firstChild);
    }
    inputTipe.append('<option value=" ">Pilih Kelas Siswa</option>');

    $.get(route, function (res) {
        $.each(res.data, function (index, value) {
            inputTipe.append('<option value="' + value[0] + '">' + value[1] + '</option>');
        });
    });

    $("#kelas_id").select2();

    $('#kelas_id').focus();

    var table = $('#dataTableBuilder').DataTable({
        'ajax': {
            'url': '/laporan/getsiswaperkelas',
            'data': function (d) {
                d.keywords = $('#kelas_id').val();
            }
        },
        "language": {
            "sProcessing": "Sedang memproses...",
            "sLengthMenu": "Tampilkan _MENU_ entri",
            "sZeroRecords": "Tidak ditemukan data yang sesuai",
            "sInfo": "Menampilkan _START_ sampai _END_ dari _TOTAL_ entri",
            "sInfoEmpty": "Menampilkan 0 sampai 0 dari 0 entri",
            "sInfoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
            "sInfoPostFix": "",
            "sSearch": "Cari:",
            "sUrl": "",
            "oPaginate": {
                "sFirst": "Pertama",
                "sPrevious": "Sebelumnya",
                "sNext": "Selanjutnya",
                "sLast": "Terakhir"
            }
        },
        'columnDefs': [{
            'targets': 0,
            "sClass": "col-md-2 text-center"
        },{
            'targets': 1,
            "sClass": "col-md-5"
        },{
            'targets': 2,
            "sClass": "text-center col-md-2"
        },{
            'targets': 3,
            "sClass": "col-md-3"
        }]
    });

    $('#cetak').on('click', function(e) {
        reloadTable();

        if ( ! table.data().count() ) {
            alert( 'Tidak ada data siswa dengan kelas yang dipilih untuk dicetak' );
            return;
        }

        window.location.href = '/laporan/laporansiswaperkelas/' + $('#kelas_id').val();
    });

    $('#cetakbawah').on('click', function(e) {
        reloadTable();

        if ( ! table.data().count() ) {
            alert( 'Tidak ada data siswa dengan kelas yang dipilih untuk dicetak' );
            return;
        }

        window.location.href = '/laporan/laporansiswaperkelas/' + $('#kelas_id').val();
    });

    //reloadTable();
});

function reloadTable() {
    var table = $('#dataTableBuilder').dataTable();
    table.cleanData;
    table.api().ajax.reload();
}