/**
 * Created by ocol on 09/11/16.
 */

$(document).ajaxStart(function() {
    Pace.restart();
});

function updateDataTableSelectAllCtrl(table) {
    var $table = table.table().node();
    var $chkbox_all = $('tbody input[type="checkbox"]', $table);
    var $chkbox_checked = $('tbody input[type="checkbox"]:checked', $table);
    var chkbox_select_all = $('thead input[name="select_all"]', $table).get(0);

    // If none of the checkboxes are checked
    if ($chkbox_checked.length === 0) {
        chkbox_select_all.checked = false;
        if ('indeterminate' in chkbox_select_all) {
            chkbox_select_all.indeterminate = false;
        }

        // If all of the checkboxes are checked
    } else if ($chkbox_checked.length === $chkbox_all.length) {
        chkbox_select_all.checked = true;
        if ('indeterminate' in chkbox_select_all) {
            chkbox_select_all.indeterminate = false;
        }

        // If some of the checkboxes are checked
    } else {
        chkbox_select_all.checked = true;
        if ('indeterminate' in chkbox_select_all) {
            chkbox_select_all.indeterminate = true;
        }
    }
}

$(document).ready(function () {
    var rows_selected = [];

    var table = $('#dataTableBuilder').DataTable({
        'ajax': {
            'url': '/laporan/getdaftarpenunggak',
        },
        "language": {
            "sProcessing": "Sedang memproses...",
            "sLengthMenu": "Tampilkan _MENU_ entri",
            "sZeroRecords": "Tidak ditemukan data yang sesuai",
            "sInfo": "Menampilkan _START_ sampai _END_ dari _TOTAL_ entri",
            "sInfoEmpty": "Menampilkan 0 sampai 0 dari 0 entri",
            "sInfoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
            "sInfoPostFix": "",
            "sSearch": "Cari:",
            "sUrl": "",
            "oPaginate": {
                "sFirst": "Pertama",
                "sPrevious": "Sebelumnya",
                "sNext": "Selanjutnya",
                "sLast": "Terakhir"
            }
        },
        'columnDefs': [{
            'targets': 0,
            'searchable': false,
            //'orderable': false,
            //orderable: false,
            "orderable": false,
            "orderData": false,
            "orderDataType": false,
            "orderSequence": false,
            'className': 'col-md-1 text-center',
            'render': function (data, type, full, meta) {
                return '<input type="checkbox">';
            }
        }, {
            'targets': 1,
            "sClass": "col-md-2"
        },{
            'targets': 2,
            "sClass": "col-md-3"
        },{
            'targets': 3,
            "sClass": "text-right col-md-3"
        },{
            'targets': 4,
            "sClass": "col-md-3"
        }],

        'rowCallback': function (row, data, dataIndex) {
            // Get row ID
            var rowId = data[0];

            // If row ID is in the list of selected row IDs
            if ($.inArray(rowId, rows_selected) !== -1) {
                $(row).find('input[type="checkbox"]').prop('checked', true);
                $(row).addClass('selected');
            }
        },
        "footerCallback": function (row, data, start, end, display) {
            //updateTotalBayar(table);
            //var api = this.api();

            if (data.length > 0) {
                var api = this.api(), data;

                // Remove the formatting to get integer data for summation
                var intVal = function ( i ) {
                    return typeof i === 'string' ?
                    i.replace(/[\$Rp,.]/g, '')*1 :
                        typeof i === 'number' ?
                            i : 0;
                };

                // Total over all pages

                total = api
                    .column( 3 )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    } );

                // Total over this page
                pageTotal = api
                    .column( 3, { page: 'current'} )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );

                // Update footer
                $( api.column( 2 ).footer() ).html(
                    //'Rp '+ numberfo pageTotal +' dari total Rp '+ total +''
                    'Rp '+ number_format(pageTotal, 0, ',', '.')  +' dari total Rp '+ number_format(total, 0, ',', '.')  +''
                );
            } else {
                //$( api.column( 3 ).footer() ).html(
                //    //'Rp '+ numberfo pageTotal +' dari total Rp '+ total +''
                //    'Rp 0'
                //);
            }
        },
    });

    // Handle click on checkbox
    $('#dataTableBuilder tbody').on('click', 'input[type="checkbox"]', function (e) {

        var $row = $(this).closest('tr');

        // Get row data
        var data = table.row($row).data();
        //alert('data: ' + data[2]);

        // Get row ID
        var rowId = data[0];

        // Determine whether row ID is in the list of selected row IDs
        var index = $.inArray(rowId, rows_selected);

        // If checkbox is checked and row ID is not in list of selected row IDs
        if (this.checked && index === -1) {
            rows_selected.push(rowId);

            // Otherwise, if checkbox is not checked and row ID is in list of selected row IDs
        } else if (!this.checked && index !== -1) {
            rows_selected.splice(index, 1);
        }

        if (this.checked) {
            $row.addClass('selected');
        } else {
            $row.removeClass('selected');
        }

        // Update state of "Select all" control
        updateDataTableSelectAllCtrl(table);

        //if (this.checked) {
        //    updateTotalBayar(table, data);
        //} else {
        //    kurangTotalBayar(table, data);
        //}

        // Prevent click event from propagating to parent
        e.stopPropagation();
    });

    // Handle click on table cells with checkboxes
    $('#dataTableBuilder').on('click', 'tbody td, thead th:first-child', function (e) {
        $(this).parent().find('input[type="checkbox"]').trigger('click');
    });

    // Handle click on "Select all" control
    $('#dataTableBuilder thead input[name="select_all"]').on('click', function (e) {
        if (this.checked) {
            $('#dataTableBuilder tbody input[type="checkbox"]:not(:checked)').trigger('click');
        } else {
            $('#dataTableBuilder tbody input[type="checkbox"]:checked').trigger('click');
        }

        // Prevent click event from propagating to parent
        e.stopPropagation();
    });

    // Handle table draw event
    table.on('draw', function () {
        // Update state of "Select all" control
        updateDataTableSelectAllCtrl(table);
    });

    // Handle form submission event
    //$('#formCetakSuratTagihan').on('submit', function (e) {
    //    var form = this;
    //
    //    // Iterate over all selected checkboxes
    //    $.each(rows_selected, function (index, rowId) {
    //        // Create a hidden element
    //        $(form).append(
    //            $('<input>')
    //                .attr('type', 'hidden')
    //                .attr('name', 'id[]')
    //                .val(rowId)
    //        );
    //    });
    //});

    $('#surattagihan').on('click', function(e) {
        var route = "/laporan/surattagihan/";

        if (jQuery.isEmptyObject(rows_selected)) {
            alert('Anda belum memilih siswa untuk mencetak surat tagihan !');
            return;
        }

        window.location.href = route + rows_selected;
    });

    $('#surattagihanbawah').on('click', function(e) {
        var route = "/laporan/surattagihan/";

        if (jQuery.isEmptyObject(rows_selected)) {
            alert('Anda belum memilih siswa untuk mencetak surat tagihan !');
            return;
        }

        window.location.href = route + rows_selected;
    });

    $('#laporanpenunggakbawah').on('click', function(e) {
        reloadTableTunggakan();

        if ( ! table.data().count() ) {
            alert( 'Tidak ada data penunggak yang akan dicetak' );
            return;
        }

        window.location.href = '/laporan/lapdaftarpenunggak';
    });

    $('#laporanpenunggak').on('click', function(e) {
        reloadTableTunggakan();

        if ( ! table.data().count() ) {
            alert( 'Tidak ada data penunggak yang akan dicetak' );
            return;
        }

        window.location.href = '/laporan/lapdaftarpenunggak';
    });
});

function reloadTableTunggakan() {
    var table = $('#dataTableBuilder').dataTable();
    table.cleanData;
    table.api().ajax.reload();
}