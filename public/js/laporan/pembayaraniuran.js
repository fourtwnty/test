/**
 * Created by ocol on 10/11/16.
 */
$(document).ajaxStart(function() {
    Pace.restart();
});

function updateDataTableSelectAllCtrl(table) {
    var $table = table.table().node();
    var $chkbox_all = $('tbody input[type="checkbox"]', $table);
    var $chkbox_checked = $('tbody input[type="checkbox"]:checked', $table);
    var chkbox_select_all = $('thead input[name="select_all"]', $table).get(0);

    // If none of the checkboxes are checked
    if ($chkbox_checked.length === 0) {
        chkbox_select_all.checked = false;
        if ('indeterminate' in chkbox_select_all) {
            chkbox_select_all.indeterminate = false;
        }

        // If all of the checkboxes are checked
    } else if ($chkbox_checked.length === $chkbox_all.length) {
        chkbox_select_all.checked = true;
        if ('indeterminate' in chkbox_select_all) {
            chkbox_select_all.indeterminate = false;
        }

        // If some of the checkboxes are checked
    } else {
        chkbox_select_all.checked = true;
        if ('indeterminate' in chkbox_select_all) {
            chkbox_select_all.indeterminate = true;
        }
    }
}

$(document).ready(function () {
    var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";

    //$('.input-daterange').datepicker({
    //    format: "dd/mm/yyyy",
    //    language: "id",
    //    orientation: "bottom auto",
    //    autoclose: true,
    //    todayHighlight: true
    //});

    $('.input-daterange').datepicker({
        format: "dd/mm/yyyy",
        container: container,
        todayHighlight: true,
        autoclose: true,
    });

    var route2 = "/pengaturan/tahuns";
    var inputTipe2 = $('#tahun_id');

    var list2 = document.getElementById("tahun_id");
    while (list2.hasChildNodes()) {
        list2.removeChild(list2.firstChild);
    }
    inputTipe2.append('<option value=" ">Pilih Tahun Ajaran</option>');

    $.get(route2, function (res) {
        $.each(res.data, function (index, value) {
            inputTipe2.append('<option value="' + value[1] + '">' + value[0] + '</option>');
        });
    });

    $("#tahun_id").select2();

    var rows_selected = [];

    var table = $('#dataTableBuilder').DataTable({
        'ajax': {
            'url': '/laporan/getdaftarpembayaran',
            'data': function (d) {
                d.tahunajaran = $('#tahun_id').val();
                d.mulaitanggal = $('#start').val();
                d.sampaitanggal = $('#end').val();
            }
        },
        "language": {
            "sProcessing": "Sedang memproses...",
            "sLengthMenu": "Tampilkan _MENU_ entri",
            "sZeroRecords": "Tidak ditemukan data yang sesuai",
            "sInfo": "Menampilkan _START_ sampai _END_ dari _TOTAL_ entri",
            "sInfoEmpty": "Menampilkan 0 sampai 0 dari 0 entri",
            "sInfoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
            "sInfoPostFix": "",
            "sSearch": "Cari:",
            "sUrl": "",
            "oPaginate": {
                "sFirst": "Pertama",
                "sPrevious": "Sebelumnya",
                "sNext": "Selanjutnya",
                "sLast": "Terakhir"
            }
        },
        'columnDefs': [{
            'targets': 0,
            'searchable': false,
            //'orderable': false,
            //orderable: false,
            "orderable": false,
            "orderData": false,
            "orderDataType": false,
            "orderSequence": false,
            'className': 'col-md-1 text-center',
            'render': function (data, type, full, meta) {
                return '<input type="checkbox">';
            }
        }, {
            'targets': 1,
            "sClass": "text-center col-md-1"
        },{
            'targets': 2,
            "sClass": "text-center col-md-2"
        }, {
            'targets': 3,
            "sClass": "text-center col-md-1"
        }, {
            'targets': 4,
            "sClass": "text-kecil col-lg-3"
        },{
            'targets': 5,
            "sClass": "text-right col-md-1"
        },{
            'targets': 6,
            "sClass": "text-kecil col-lg-3"
        }],
        'rowCallback': function (row, data, dataIndex) {
            // Get row ID
            var rowId = data[0];

            // If row ID is in the list of selected row IDs
            if ($.inArray(rowId, rows_selected) !== -1) {
                $(row).find('input[type="checkbox"]').prop('checked', true);
                $(row).addClass('selected');
            }
        },
        "footerCallback": function (row, data, start, end, display) {
            //updateTotalBayar(table);
            //var api = this.api();

            var api = this.api(), data;
            if (data.length > 0) {
                // Remove the formatting to get integer data for summation
                var intVal = function ( i ) {
                    return typeof i === 'string' ?
                    i.replace(/[\$,.]/g, '')*1 :
                        typeof i === 'number' ?
                            i : 0;
                };

                // Total over all pages

                if (data.length > 1){
                    total = api
                        .column( 5 )
                        .data()
                        .reduce( function (a, b) {
                            //alert(a + " " + b);
                            return intVal(a) + intVal(b);
                        } );
                    // Total over this page
                    pageTotal = api
                        .column( 5, { page: 'current'} )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );
                } else {
                    total = api
                        .column( 5 )
                        .data().reduce(function(a) {
                            return intVal(a);
                        });
                    total = pageTotal = intVal(total);

                    //// Total over this page
                    //pageTotal = api
                    //    .column( 5, { page: 'current'} )
                    //    .data()
                    //    .reduce( function (a, b) {
                    //        return intVal(a) + intVal(b);
                    //    }, 0 );
                }

                // Update footer
                $( api.column( 0 ).footer() ).html(
                    //'Rp '+ numberfo pageTotal +' dari total Rp '+ total +''
                    'Rp '+ number_format(pageTotal, 0, ',', '.')  +' dari total Rp '+ number_format(total, 0, ',', '.')  +''
                );
            } else {
                $( api.column( 0 ).footer() ).html(
                    //'Rp '+ numberfo pageTotal +' dari total Rp '+ total +''
                    'Rp 0'
                );
            }
        },
    });

    // Handle click on checkbox
    $('#dataTableBuilder tbody').on('click', 'input[type="checkbox"]', function (e) {

        var $row = $(this).closest('tr');

        // Get row data
        var data = table.row($row).data();
        //alert('data: ' + data[2]);

        // Get row ID
        var rowId = data[0];

        // Determine whether row ID is in the list of selected row IDs
        var index = $.inArray(rowId, rows_selected);

        // If checkbox is checked and row ID is not in list of selected row IDs
        if (this.checked && index === -1) {
            rows_selected.push(rowId);

            // Otherwise, if checkbox is not checked and row ID is in list of selected row IDs
        } else if (!this.checked && index !== -1) {
            rows_selected.splice(index, 1);
        }

        if (this.checked) {
            $row.addClass('selected');
        } else {
            $row.removeClass('selected');
        }

        // Update state of "Select all" control
        updateDataTableSelectAllCtrl(table);

        //if (this.checked) {
        //    updateTotalBayar(table, data);
        //} else {
        //    kurangTotalBayar(table, data);
        //}

        // Prevent click event from propagating to parent
        e.stopPropagation();
    });

    // Handle click on table cells with checkboxes
    $('#dataTableBuilder').on('click', 'tbody td, thead th:first-child', function (e) {
        $(this).parent().find('input[type="checkbox"]').trigger('click');
    });

    // Handle click on "Select all" control
    $('#dataTableBuilder thead input[name="select_all"]').on('click', function (e) {
        if (this.checked) {
            $('#dataTableBuilder tbody input[type="checkbox"]:not(:checked)').trigger('click');
        } else {
            $('#dataTableBuilder tbody input[type="checkbox"]:checked').trigger('click');
        }

        // Prevent click event from propagating to parent
        e.stopPropagation();
    });

    // Handle table draw event
    table.on('draw', function () {
        // Update state of "Select all" control
        updateDataTableSelectAllCtrl(table);
    });

    // Handle form submission event
    $('#formCetakLaporanDetailTerseleksi').on('submit', function (e) {
        if ( ! table.data().count() ) {
            alert( 'Tidak ada data pembayaran iuran komite yang akan dicetak' );
            return false;
        }

        if (jQuery.isEmptyObject(rows_selected)) {
            alert('Anda belum memilih pembayaran iuran yang akan dicetak !');
            return false;
        }

        var tahunajaran = $('#tahun_id').val();
        var dari = $('#start').val();
        var sampai= $('#end').val();

        var form = this;

        // Iterate over all selected checkboxes
        $.each(rows_selected, function (index, rowId) {
            // Create a hidden element
            $(form).append(
                $('<input>')
                    .attr('type', 'hidden')
                    .attr('name', 'id[]')
                    .val(rowId)
            );
        });

        $(form).append(
            $('<input>')
                .attr('type', 'hidden')
                .attr('name', 'tahunajaran')
                .val(tahunajaran)
        );
        $(form).append(
            $('<input>')
                .attr('type', 'hidden')
                .attr('name', 'daritanggal')
                .val(dari)
        );
        $(form).append(
            $('<input>')
                .attr('type', 'hidden')
                .attr('name', 'sampaitanggal')
                .val(sampai)
        );
    });

    // Handle form submission event
    $('#formCetakLaporanDetail').on('submit', function (e) {
        reloadTable();

        if ( ! table.data().count() ) {
            alert( 'Tidak ada data pembayaran iuran komite yang akan dicetak' );
            return false;
        }

        var form = this;

        var tahunajaran = $('#tahun_id').val();
        var dari = $('#start').val();
        var sampai= $('#end').val();

        $(form).append(
            $('<input>')
                .attr('type', 'hidden')
                .attr('name', 'tahunajaran')
                .val(tahunajaran)
        );
        $(form).append(
            $('<input>')
                .attr('type', 'hidden')
                .attr('name', 'daritanggal')
                .val(dari)
        );
        $(form).append(
            $('<input>')
                .attr('type', 'hidden')
                .attr('name', 'sampaitanggal')
                .val(sampai)
        );
    });
    //
    // Handle form submission event
    $('#formCetakLaporan').on('submit', function (e) {
        reloadTable();

        if ( ! table.data().count() ) {
            alert( 'Tidak ada data pembayaran iuran komite yang akan dicetak' );
            return false;
        }

        var form = this;

        var tahunajaran = $('#tahun_id').val();
        var dari = $('#start').val();
        var sampai= $('#end').val();
        $(form).append(
            $('<input>')
                .attr('type', 'hidden')
                .attr('name', 'tahunajaran')
                .val(tahunajaran)
        );
        $(form).append(
            $('<input>')
                .attr('type', 'hidden')
                .attr('name', 'daritanggal')
                .val(dari)
        );
        $(form).append(
            $('<input>')
                .attr('type', 'hidden')
                .attr('name', 'sampaitanggal')
                .val(sampai)
        );
    });

    $('#tahun_id').on('change', function (e) {
        reloadTable();
    });
    $('#start').on('change', function (e) {
        reloadTable();
    });
    $('#end').on('change', function (e) {
        reloadTable();
    });

    //$('#cetak').on('click', function(e) {
    //    reloadTable();
    //
    //    if ( ! table.data().count() ) {
    //        alert( 'Tidak ada data pembayaran iuran komite yang akan dicetak' );
    //        return;
    //    }
    //
    //    var tahunajaran = $('#tahun_id').val();
    //    var dari = $('#start').val();
    //    var sampai= $('#end').val();
    //
    //    var data = [];
    //    data[0] = tahunajaran;
    //    data[1] = dari;
    //    data[2] = sampai;
    //
    //
    //    window.location.href = '/laporan/laporaniuran/' + data;
    //    //window.location.href = '/laporan/laporaniuran/t=' + tahunajaran + '/f=' + dari + '/t=' + sampai;
    //});
} );

function reloadTable() {
    var table = $('#dataTableBuilder').dataTable();
    table.cleanData;
    table.api().ajax.reload();
}