/**
 * Created by ocol on 10/11/16.
 */
$(document).ajaxStart(function() {
    Pace.restart();
});

$(document).ready(function () {
    var route = "/pengaturan/get_program_studi";
    var inputTipe = $('#program_studi');

    var list = document.getElementById("program_studi");
    while (list.hasChildNodes()) {
        list.removeChild(list.firstChild);
    }
    inputTipe.append('<option value=" ">Pilih Program Studi / Jurusan</option>');

    $.get(route, function (res) {
        if (!jQuery.isEmptyObject(res)) {
            $.each(res.data, function (index, value) {
                inputTipe.append('<option value="' + value[0] + '">' + value[1] + '</option>');
            });
        }
    });

    $("#program_studi").select2();

    $('#program_studi').focus();

    var table = $('#dataTableBuilder').DataTable({
        'ajax': {
            'url': '/laporan/getsiswaperprogram',
            'data': function (d) {
                d.keywords = $('#program_studi').val();
            }
        },
        "language": {
            "sProcessing": "Sedang memproses...",
            "sLengthMenu": "Tampilkan _MENU_ entri",
            "sZeroRecords": "Tidak ditemukan data yang sesuai",
            "sInfo": "Menampilkan _START_ sampai _END_ dari _TOTAL_ entri",
            "sInfoEmpty": "Menampilkan 0 sampai 0 dari 0 entri",
            "sInfoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
            "sInfoPostFix": "",
            "sSearch": "Cari:",
            "sUrl": "",
            "oPaginate": {
                "sFirst": "Pertama",
                "sPrevious": "Sebelumnya",
                "sNext": "Selanjutnya",
                "sLast": "Terakhir"
            }
        },
        'columnDefs': [{
            'targets': 0,
            "sClass": "col-md-2 text-center"
        },{
            'targets': 1,
            "sClass": "col-md-5"
        },{
            'targets': 2,
            "sClass": "text-center col-md-2"
        },{
            'targets': 3,
            "sClass": "col-md-3"
        }]
    });

    $('#cetakprogram').on('click', function(e) {
        reloadTable();

        if ( ! table.data().count() ) {
            alert( 'Tidak ada data siswa dengan program studi/jurusan yang dipilih, yang akan dicetak' );
            return;
        }

        window.location.href = '/laporan/laporansiswaperprogram/' + $('#program_studi').val();
    });

    $('#cetakprogrambawah').on('click', function(e) {
        reloadTable();

        if ( ! table.data().count() ) {
            alert( 'Tidak ada data siswa dengan program studi/jurusan yang dipilih, yang akan dicetak' );
            return;
        }

        window.location.href = '/laporan/laporansiswaperprogram/' + $('#program_studi').val();
    });

    //reloadTable();
});

function reloadTable() {
    var table = $('#dataTableBuilder').dataTable();
    table.cleanData;
    table.api().ajax.reload();
}