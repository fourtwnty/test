/**
 * Created by ocol on 09/11/16.
 */
$(document).ready(function () {
    //$('.inputanangka').maskMoney({thousands:'.', decimal:',', allowZero:true});
    // tambah event keypress untuk input bayar
    $('.inputanangka').on('keypress', function(e) {
        var c = e.keyCode || e.charCode;
        switch (c) {
            case 8: case 9: case 27: case 13: return;
            case 65:
                if (e.ctrlKey === true) return;
        }
        if (c < 48 || c > 57) e.preventDefault();
    }).on('keyup', function() {
        //alert('disini');
        var inp = $(this).val().replace(/\./g, '');
        $(this).val(formatRibuan(inp));
    });


    var route = "/pengaturan/get_program_studi";
    var inputTipe = $('#program_studi');

    var list = document.getElementById("program_studi");
    while (list.hasChildNodes()) {
        list.removeChild(list.firstChild);
    }
    inputTipe.append('<option value=" ">Pilih Program Studi / Jurusan</option>');

    $.get(route, function (res) {
        $.each(res.data, function (index, value) {
            inputTipe.append('<option value="' + value[0] + '">' + value[1] + '</option>');
        });
    });

    //Initialize Select2 Elements
    $("#program_studi").select2();

    $('#program_studi').focus();

    $('#simpan').click(function() {
        var route = "/pengaturan/tarif";
        var token = $('#token').val();

        var program_studi = $('#program_studi').val();
        if (program_studi == '' || program_studi == ' ' || program_studi == undefined) {
            alert('Pilih Program Studi/Jurusan dengan benar!');
            $('#program_studi').focus();
            return;
        }

        var nilai = $('#nilai').val();
        if (nilai == '' || nilai == ' ' || intVal(nilai) <= 0) {
            alert('Nilai tarif iuran tidak valid');
            $('#nilai').focus();
            return;
        }

        nilai = intVal(nilai);

        $.ajax({
            url: route,
            type: 'POST',
            headers: {'X-CSRF-TOKEN': token},
            dataType: 'json',
            data: {
                program_studi : program_studi,
                nilai : nilai,
                _token: token
            },
            error: function (res) {
                var errors = res.responseJSON;
                var pesan = '';
                $.each(errors, function (index, value) {
                    pesan += value + "\n";
                });

                alert(pesan);
            },
            success: function () {
                alert('Sukses Menyimpan Data');

                $('#program_studi').select2('val', ' ');
                $('#nilai').val('0');

                $('#program_studi').focus();
            }
        });
    });
});