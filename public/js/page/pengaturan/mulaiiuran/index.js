/**
 * Created by ocol on 09/11/16.
 */
$(document).ajaxStart(function() {
    Pace.restart();
});

$(document).ready(function () {
    var route = "/cekhakakses/ubah_mulaiiuran";
    var bolehUbah;
    $.get(route, function (res) {
        bolehUbah = res;
    });


    $('#dataTableBuilder').DataTable({
        //scrollX: true,
        //scrollColapse:true,
        responsive: true,
        'ajax': {
            'url': '/pengaturan/mulaiiurans',
        },
        'columnDefs': [{
            'targets': 3,
            'searchable': false,
            "orderable": false,
            "orderData": false,
            "orderDataType": false,
            "orderSequence": false,
            "sClass": "text-center col-lg-1 td-aksi",
            'render': function (data, type, full, meta) {
                var kembali = '';
                if (bolehUbah == true) {
                    kembali += '<button title="Ubah Data" class="btn btn-warning btn-flat" data-toggle="modal" data-target="#modalUbah" onclick="UbahClick(this);"><i class="fa fa-pencil-square-o fa-fw"></i> </button>';
                }

                return kembali;

            }
        },{
            'targets':0,
            'sClass': "col-lg-2"
        },{
            'targets':1,
            'sClass': "col-lg-7"
        },{
            'targets':2,
            'sClass': "col-lg-2 text-center"
        }],
        'rowCallback': function (row, data, dataIndex) {
            if (bolehUbah == true) {
                $(row).find('button[class="btn btn-warning btn-flat"]').prop('value', data[3]);
            }

        }
    });

    var route1 = "/get_bulan";
    var inputTipe1 = $('#bulan_id');

    var list1 = document.getElementById("bulan_id");
    while (list1.hasChildNodes()) {
        list1.removeChild(list1.firstChild);
    }
    inputTipe1.append('<option value=" ">Pilih Bulan Mulai</option>');

    $.get(route1, function (res) {
        $.each(res.data, function (index, value) {
            inputTipe1.append('<option value="' + value[0] + '">' + value[1] + '</option>');
        });
    });

    var route2 = "/pengaturan/tahuns";
    var inputTipe2 = $('#tahun_id');

    var list2 = document.getElementById("tahun_id");
    while (list2.hasChildNodes()) {
        list2.removeChild(list2.firstChild);
    }
    inputTipe2.append('<option value=" ">Pilih Tahun Ajaran</option>');

    $.get(route2, function (res) {
        $.each(res.data, function (index, value) {
            inputTipe2.append('<option value="' + value[1] + '">' + value[0] + '</option>');
        });
    });

    $('#tahun_id').select2();
    $('#bulan_id').select2();

});

function reloadTable() {
    var table = $('#dataTableBuilder').dataTable();
    table.cleanData;
    table.api().ajax.reload();
}


function UbahClick(btn) {
    route = "/pengaturan/mulaiiuran/" + btn.value + "/edit";

    $.get(route, function (res) {
        $('#id').val(res.id);
        $('#nisn').val(res.nisn);
        $('#nama').val(res.nama);

        $('#bulan_id').select2('val', ''+res.bulan_id);
        $('#tahun_id').select2('val', ''+res.tahun_id);

        $('#bulan_id').focus();

    });
}

$('#simpan').click(function () {
    var id = $('#id').val();
    var token = $('#token').val();
    var route = "/pengaturan/mulaiiuran/" + id;

    var bulan_id = $('#bulan_id').val();
    if (bulan_id == '' || bulan_id == ' ' || bulan_id == undefined) {
        alert('Pilih bulan mulai iuran dengan benar !');
        $('#bulan_id').focus();
        return;
    }

    var tahun_id = $('#tahun_id').val();
    if (tahun_id == '' || tahun_id == ' ' || tahun_id == undefined) {
        alert('Pilih tahun ajaran dengan benar !');
        $('#tahun_id').focus();
        return;
    }

    $.ajax({
        url: route,
        headers: {'X-CSRF-TOKEN': token},
        type: 'PUT',
        dataType: 'json',
        data: {
            bulan_id: bulan_id,
            tahun_id: tahun_id,
            _token: token
        },
        error: function (res) {
            var errors = res.responseJSON;
            var pesan = '';
            $.each(errors, function (index, value) {
                pesan += value + "\n";
            });

            alert(pesan);
        },
        success: function () {
            reloadTable();
            alert('Sukses Mengubah Data');
            $('#modalUbah').modal('toggle');
        }
    });
});