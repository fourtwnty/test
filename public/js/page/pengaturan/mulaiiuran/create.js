/**
 * Created by ocol on 09/11/16.
 */
$(document).ajaxStart(function() {
    Pace.restart();
});

$(document).ready(function () {
    document.getElementById("nisn-cari").maxLength = 10;

    $('.nisn-asli').val(null);

    var timer;

    $('#nisn-cari').on('keydown', function(evt) {
        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode == 13) {
            clearTimeout(timer);
        }
    }).on('keyup', function(e) {
        timer = setTimeout(function () {
            //alert('disini');
            var keywords = $('#nisn-cari').val();

            if (keywords.length > 0) {

                $.get('/siswamulaisearch/' + keywords, function(res) {
                    if (res.nama == null) {
                        $('.nisn-asli').val('');
                        $('.hasil-cari').val('Tidak ditemukan');
                    } else {
                        $('.nisn-asli').val(res.nisn);
                        $('#nama').val(res.nama);
                    }
                });
            }
        }, 500);
    });

    var route1 = "/get_bulan";
    var inputTipe1 = $('#bulan_id');

    var list1 = document.getElementById("bulan_id");
    while (list1.hasChildNodes()) {
        list1.removeChild(list1.firstChild);
    }
    inputTipe1.append('<option value=" ">Pilih Bulan Mulai</option>');

    $.get(route1, function (res) {
        $.each(res.data, function (index, value) {
            inputTipe1.append('<option value="' + value[0] + '">' + value[1] + '</option>');
        });
    });

    var route2 = "/pengaturan/tahuns";
    var inputTipe2 = $('#tahun_id');

    var list2 = document.getElementById("tahun_id");
    while (list2.hasChildNodes()) {
        list2.removeChild(list2.firstChild);
    }
    inputTipe2.append('<option value=" ">Pilih Tahun Ajaran</option>');

    $.get(route2, function (res) {
        $.each(res.data, function (index, value) {
            inputTipe2.append('<option value="' + value[1] + '">' + value[0] + '</option>');
        });
    });

    $("#bulan_id").select2();
    $("#tahun_id").select2();

    $('#dataTableBuilder').DataTable({
        //scrollX: true,
        //scrollColapse:true,
        responsive: true,
        'ajax': {
            'url': '/get_daftar_siswa',
        },
        'columnDefs': [{
            'targets': 4,
            'searchable': false,
            "orderable": false,
            "orderData": false,
            "orderDataType": false,
            "orderSequence": false,
            "sClass": "text-center col-lg-1 td-aksi",
            'render': function (data, type, full, meta) {
                var kembali = '';

                kembali += '<button title="Pilih Data" class="btn btn-success btn-flat" onclick="PilihClick(this);"><i class="fa fa-hand-pointer-o fa-fw"></i> </button>';


                return kembali;

            }
        },{
            'targets':0,
            'sClass': "col-lg-2"
        }, {
            'targets':1,
            'sClass': "col-lg-4"
        },{
            'targets':2,
            'sClass': "col-lg-2"
        },{
            'targets':3,
            'sClass': "col-lg-3"
        }],
        'rowCallback': function (row, data, dataIndex) {
            $(row).find('button[class="btn btn-success btn-flat"]').prop('value', data[4]);
        }
    });


    $('#carisiswa').click(function() {
        reloadTable();
    });

    $('#nisn-cari').focus();

    $('#simpan').click(function() {
        var route = "/pengaturan/mulaiiuran";
        var token = $('#token').val();

        var nisncari = $('#nisn-cari').val();
        var nisn = $('.nisn-asli').val();

        //console.log(nisn + ', ' + nisncari);

        if (nisncari != nisn) {
            alert('NISN tidak valid! Silahkan ulangi lagi pencarian NISN Siswa !!!');
            $('#nisn-cari').focus();
            return;
        }

        if (nisn == '' || nisn == ' ' || nisn == undefined) {
            alert('NISN Siswa harus ada dan tidak boleh kosong !');
            $('#nisn').focus();
            return;
        }

        var bulan_id = $('#bulan_id').val();
        if (bulan_id == '' || bulan_id == ' ' || bulan_id == undefined) {
            alert('Pilih bulan mulai iuran dengan benar !');
            $('#bulan_id').focus();
            return;
        }

        var tahun_id = $('#tahun_id').val();
        if (tahun_id == '' || tahun_id == ' ' || tahun_id == undefined) {
            alert('Pilih tahun ajaran dengan benar !');
            $('#tahun_id').focus();
            return;
        }

        $.ajax({
            url: route,
            type: 'POST',
            headers: {'X-CSRF-TOKEN': token},
            dataType: 'json',
            data: {
                nisn: nisn,
                bulan_id: bulan_id,
                tahun_id: tahun_id,
                _token: token
            },
            error: function (res) {
                var errors = res.responseJSON;
                var pesan = '';
                $.each(errors, function (index, value) {
                    pesan += value + "\n";
                });

                alert(pesan);
            },
            success: function () {
                alert('Sukses Menyimpan Data');

                $('.nisn-asli').val(null);
                $('#nisn-cari').val(null);
                $('#nama').val(null);
                $('#bulan_id').select2('val', ' ');
                $('#tahun_id').select2('val', ' ');

                $('#nisn-cari').focus();
            }
        });
    });
});

function reloadTable() {
    var table = $('#dataTableBuilder').dataTable();
    table.cleanData;
    table.api().ajax.reload();
}


function PilihClick(btn) {
    route = "/siswa/" + btn.value + "/edit";

    $.get(route, function (res) {
        $('.nisn-asli').val(res.nisn);
        $('#nisn-cari').val(res.nisn);
        $('#nama').val(res.nama);

        $('#nisn-cari').focus();

        $('#modalCari').modal('toggle');

    });
}