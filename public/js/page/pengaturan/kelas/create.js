/**
 * Created by ocol on 09/11/16.
 */
$(document).ajaxStart(function() {
    Pace.restart();
});

$(document).ready(function () {
    var route = "/pengaturan/get_program_studi";
    var inputTipe = $('#program_studi');

    var list = document.getElementById("program_studi");
    while (list.hasChildNodes()) {
        list.removeChild(list.firstChild);
    }
    inputTipe.append('<option value=" ">Pilih Program Studi / Jurusan</option>');

    $.get(route, function (res) {
        $.each(res.data, function (index, value) {
            inputTipe.append('<option value="' + value[0] + '">' + value[1] + '</option>');
        });
    });

    //Initialize Select2 Elements
    $("#program_studi").select2();

    $('#program_studi').focus();

    $('#simpan').click(function() {
        var route = "/pengaturan/kelas";
        var token = $('#token').val();

        var program_studi = $('#program_studi').val();
        if (program_studi == '' || program_studi == ' ' || program_studi == undefined) {
            alert('Pilih Program Studi/Jurusan dengan benar!');
            $('#program_studi').focus();
            return;
        }

        //var tingkatan = $('#tingkatan').val();
        //if (tingkatan == '' || tingkatan == ' ' || tingkatan == undefined || (tingkatan != 'I' && tingkatan != 'II' && tingkatan != 'III')) {
        //    alert('Tingkatan kelas tidak valid');
        //    $('#tingkatan').focus();
        //    return;
        //}

        var tingkatan = $('#tingkatan').val();
        if (tingkatan == '' || tingkatan == ' ' || tingkatan == undefined) {
            alert('Inputkan tingkatan kelas dengan benar');
            $('#tingkatan').focus();
            return;
        }

        $.ajax({
            url: route,
            type: 'POST',
            headers: {'X-CSRF-TOKEN': token},
            dataType: 'json',
            data: {
                program_studi : program_studi,
                tingkatan : tingkatan,
                _token: token
            },
            error: function (res) {
                var errors = res.responseJSON;
                var pesan = '';
                $.each(errors, function (index, value) {
                    pesan += value + "\n";
                });

                alert(pesan);
            },
            success: function () {
                alert('Sukses Menyimpan Data');

                $('#program_studi').select2('val', ' ');
                $('#tingkatan').val('');

                $('#program_studi').focus();
            }
        });
    });
});