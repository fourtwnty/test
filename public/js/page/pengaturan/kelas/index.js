/**
 * Created by ocol on 09/11/16.
 */
$(document).ajaxStart(function() {
    Pace.restart();
});

$(document).ready(function () {
    var route = "/cekhakakses/ubah_kelas";
    var bolehUbah;
    $.get(route, function (res) {
        bolehUbah = res;
    });

    var route = "/cekhakakses/hapus_kelas";
    var bolehHapus;
    $.get(route, function (res) {
        bolehHapus = res;
    });


    $('#dataTableBuilder').DataTable({
        //scrollX: true,
        //scrollColapse:true,
        responsive: true,
        'ajax': {
            'url': '/pengaturan/kelases',
        },
        'columnDefs': [{
            'targets': 2,
            'searchable': false,
            "orderable": false,
            "orderData": false,
            "orderDataType": false,
            "orderSequence": false,
            "sClass": "text-center col-lg-2 td-aksi",
            'render': function (data, type, full, meta) {
                var kembali = '';
                if (bolehUbah == true) {
                    kembali += '<button title="Ubah Data" class="btn btn-warning btn-flat" data-toggle="modal" data-target="#modalUbah" onclick="UbahClick(this);"><i class="fa fa-pencil-square-o fa-fw"></i> </button>';
                }
                if (bolehHapus == true) {
                    kembali += '<button title="Hapus Data" class="btn btn-danger btn-flat" data-toggle="modal" data-target="#modalHapus" onclick="HapusClick(this);"><i class="fa fa-trash fa-fw"></i> </button>';
                }

                return kembali;

            }
        },{
            'targets':0,
            'sClass': "col-lg-8"
        },{
            'targets':1,
            'sClass': "col-lg-2 text-center"
        }],
        'rowCallback': function (row, data, dataIndex) {
            if (bolehUbah == true) {
                $(row).find('button[class="btn btn-warning btn-flat"]').prop('value', data[2]);
            }
            if (bolehHapus == true) {
                $(row).find('button[class="btn btn-danger btn-flat"]').prop('value', data[2]);
            }

        }
    });

    var route = "/pengaturan/get_program_studi";
    var inputTipe = $('#program_studi');

    var list = document.getElementById("program_studi");
    while (list.hasChildNodes()) {
        list.removeChild(list.firstChild);
    }
    inputTipe.append('<option value=" ">Pilih Program Studi / Jurusan</option>');

    $.get(route, function (res) {
        $.each(res.data, function (index, value) {
            inputTipe.append('<option value="' + value[0] + '">' + value[1] + '</option>');
        });
    });

    //Initialize Select2 Elements
    $("#program_studi").select2();

});

function reloadTable() {
    var table = $('#dataTableBuilder').dataTable();
    table.cleanData;
    table.api().ajax.reload();
}


function UbahClick(btn) {
    route = "/pengaturan/kelas/" + btn.value + "/edit";

    $.get(route, function (res) {
        $('#id').val(res.id);


        var nilai = res.program_id;

        //console.log('nilai = ' + nilai);

        $("#program_studi").select2('val',''+nilai);
        //$('#program_studi').val(res.program_id);
        $('#tingkatan').val(res.tingkatan);
        $('#program_studi').focus();

    });
}

$('#simpan').click(function () {
    var id = $('#id').val();
    var token = $('#token').val();
    var route = "/pengaturan/kelas/" + id;

    var program_studi = $('#program_studi').val();
    if (program_studi == '' || program_studi == ' ' || program_studi == undefined) {
        alert('Pilih Program Studi/Jurusan dengan benar!');
        $('#program_studi').focus();
        return;
    }

    //var tingkatan = $('#tingkatan').val();
    //if (tingkatan == '' || tingkatan == ' ' || tingkatan == undefined || (tingkatan != 'I' && tingkatan != 'II' && tingkatan != 'III')) {
    //    alert('Tingkatan kelas tidak valid');
    //    $('#tingkatan').focus();
    //    return;
    //}

    var tingkatan = $('#tingkatan').val();
    if (tingkatan == '' || tingkatan == ' ' || tingkatan == undefined) {
        alert('Tingkatan kelas tidak valid');
        $('#tingkatan').focus();
        return;
    }

    $.ajax({
        url: route,
        headers: {'X-CSRF-TOKEN': token},
        type: 'PUT',
        dataType: 'json',
        data: {
            program_studi : program_studi,
            tingkatan : tingkatan,
            _token: token
        },
        error: function (res) {
            var errors = res.responseJSON;
            var pesan = '';
            $.each(errors, function (index, value) {
                pesan += value + "\n";
            });

            alert(pesan);
        },
        success: function () {
            reloadTable();
            alert('Sukses Mengubah Data');
            $('#modalUbah').modal('toggle');
        }
    });
});

function HapusClick(btn) {
    $('#idHapus').val(btn.value);
}

$('#yakinhapus').click(function () {
    var token = $('#token').val();
    var id = $('#idHapus').val();
    var route = "/pengaturan/kelas/" + id;

    $.ajax({
        url: route,
        headers: {'X-CSRF-TOKEN': token},
        type: 'DELETE',
        dataType: 'json',
        error: function (res) {
            var errors = res.responseJSON;
            var pesan = '';
            $.each(errors, function (index, value) {
                pesan += value + "\n";
            });

            alert(pesan);
        },
        success: function () {
            reloadTable();
            alert('Sukses Menghapus Data');
            $('#modalHapus').modal('toggle');
        }
    });
});