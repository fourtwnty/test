/**
 * Created by ocol on 09/11/16.
 */
$(document).ajaxStart(function() {
    Pace.restart();
});

$(document).ready(function () {
    var route = "/cekhakakses/ubah_tahun";
    var bolehUbah;
    $.get(route, function (res) {
        bolehUbah = res;
    });

    var route = "/cekhakakses/hapus_tahun";
    var bolehHapus;
    $.get(route, function (res) {
        bolehHapus = res;
    });


    $('#dataTableBuilder').DataTable({
        //scrollX: true,
        //scrollColapse:true,
        responsive: true,
        'ajax': {
            'url': '/pengaturan/tahuns',
        },
        'columnDefs': [{
            'targets': 1,
            'searchable': false,
            "orderable": false,
            "orderData": false,
            "orderDataType": false,
            "orderSequence": false,
            "sClass": "text-center col-lg-2 td-aksi",
            'render': function (data, type, full, meta) {
                var kembali = '';
                if (bolehUbah == true) {
                    kembali += '<button title="Ubah Data" class="btn btn-warning btn-flat" data-toggle="modal" data-target="#modalUbah" onclick="UbahClick(this);"><i class="fa fa-pencil-square-o fa-fw"></i> </button>';
                }
                if (bolehHapus == true) {
                    kembali += '<button title="Hapus Data" class="btn btn-danger btn-flat" data-toggle="modal" data-target="#modalHapus" onclick="HapusClick(this);"><i class="fa fa-trash fa-fw"></i> </button>';
                }

                return kembali;

            }
        },{
            'targets':0,
            'sClass': "col-lg-10"
        }],
        'rowCallback': function (row, data, dataIndex) {
            if (bolehUbah == true) {
                $(row).find('button[class="btn btn-warning btn-flat"]').prop('value', data[1]);
            }
            if (bolehHapus == true) {
                $(row).find('button[class="btn btn-danger btn-flat"]').prop('value', data[1]);
            }

        }
    });

    var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";

    $('.input-daterange').datepicker({
        format: "yyyy",
        minViewMode: 2,
        container: container,
        todayHighlight: true,
        autoclose: true,
    });


});

function reloadTable() {
    var table = $('#dataTableBuilder').dataTable();
    table.cleanData;
    table.api().ajax.reload();
}


function UbahClick(btn) {
    route = "/pengaturan/tahun/" + btn.value + "/edit";

    $.get(route, function (res) {
        $('#id').val(res.id);
        $('#start').val(res.awal);
        $('#end').val(res.akhir);
        $('#start').focus();

    });
}

$('#simpan').click(function () {
    var id = $('#id').val();
    var token = $('#token').val();
    var route = "/pengaturan/tahun/" + id;

    var awal = $("#start").val();
    if (awal == '' || awal==' ' || awal == undefined) {
        alert('Tahun Awal tidak boleh dikosongkan');
        $('#start').focus();
        return;
    }

    //console.log(awal);

    var akhir = $('#end').val();
    if (akhir == '' || akhir==' ' || akhir == undefined) {
        alert('Tahun Akhir tidak boleh dikosongkan');
        //console.log(akhir);
        $('#end').focus();
        return;
    }

    var intawal = intVal(awal);
    var intakhir = intVal(akhir);

    if (intakhir <= intawal) {
        alert('Tahun awal dan akhir untuk tahun ajaran tidak benar! Contoh valid : 2016/2017');
        return;
    }

    if (intakhir - intawal > 1) {
        alert('Tahun awal dan akhir untuk tahun ajaran tidak benar! Contoh valid : 2016/2017');
        return;
    }

    $.ajax({
        url: route,
        headers: {'X-CSRF-TOKEN': token},
        type: 'PUT',
        dataType: 'json',
        data: {
            name: awal + "/" + akhir,
            _token: token
        },
        error: function (res) {
            var errors = res.responseJSON;
            var pesan = '';
            $.each(errors, function (index, value) {
                pesan += value + "\n";
            });

            alert(pesan);
        },
        success: function () {
            reloadTable();
            alert('Sukses Mengubah Data');
            $('#modalUbah').modal('toggle');
        }
    });
});

function HapusClick(btn) {
    $('#idHapus').val(btn.value);
}

$('#yakinhapus').click(function () {
    var token = $('#token').val();
    var id = $('#idHapus').val();
    var route = "/pengaturan/tahun/" + id;

    $.ajax({
        url: route,
        headers: {'X-CSRF-TOKEN': token},
        type: 'DELETE',
        dataType: 'json',
        error: function (res) {
            var errors = res.responseJSON;
            var pesan = '';
            $.each(errors, function (index, value) {
                pesan += value + "\n";
            });

            alert(pesan);
        },
        success: function () {
            reloadTable();
            alert('Sukses Menghapus Data');
            $('#modalHapus').modal('toggle');
        }
    });
});