/**
 * Created by ocol on 09/11/16.
 */
$(document).ready(function () {
    var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";

    $('.input-daterange').datepicker({
        format: "yyyy",
        minViewMode: 2,
        container: container,
        todayHighlight: true,
        autoclose: true,
    });

    $('#simpan').click(function() {
        var route = "/pengaturan/tahun";
        var token = $('#token').val();

        //var awal = $("#start").data('datepicker').getFormattedDate('yyyy-mm-dd');
        var awal = $("#start").val();
        if (awal == '' || awal==' ' || awal == undefined) {
            alert('Tahun Awal tidak boleh dikosongkan');
            $('#start').focus();
            return;
        }

        //console.log(awal);

        var akhir = $('#end').val();
        if (akhir == '' || akhir==' ' || akhir == undefined) {
            alert('Tahun Akhir tidak boleh dikosongkan');
            //console.log(akhir);
            $('#end').focus();
            return;
        }

        var intawal = intVal(awal);
        var intakhir = intVal(akhir);

        if (intakhir <= intawal) {
            alert('Tahun awal dan akhir untuk tahun ajaran tidak benar! Contoh valid : 2016/2017');
            return;
        }

        if (intakhir - intawal > 1) {
            alert('Tahun awal dan akhir untuk tahun ajaran tidak benar! Contoh valid : 2016/2017');
            return;
        }

        $.ajax({
            url: route,
            type: 'POST',
            headers: {'X-CSRF-TOKEN': token},
            dataType: 'json',
            data: {
                name: awal + "/" + akhir,
                _token: token
            },
            error: function (res) {
                var errors = res.responseJSON;
                var pesan = '';
                $.each(errors, function (index, value) {
                    pesan += value + "\n";
                });

                alert(pesan);
            },
            success: function () {
                alert('Sukses Menyimpan Data');

                $('#start').val(null);
                $('#end').val(null);

                $('#start').focus();
            }
        });
    });
});