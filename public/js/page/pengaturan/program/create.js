/**
 * Created by ocol on 09/11/16.
 */
$(document).ajaxStart(function() {
    Pace.restart();
});

$(document).ready(function () {

    document.getElementById("name").maxLength = 100;

    $('#name').focus();

    $('#simpan').click(function() {
        var route = "/pengaturan/program";
        var token = $('#token').val();

        var name = $('#name').val();
        if (name == '' || name == undefined) {
            alert('Nama Program Studi / Jurusan tidak boleh dikosongkan');
            $('#name').focus();
            return;
        }

        $.ajax({
            url: route,
            type: 'POST',
            headers: {'X-CSRF-TOKEN': token},
            dataType: 'json',
            data: {
                name: name,
                _token: token
            },
            error: function (res) {
                var errors = res.responseJSON;
                var pesan = '';
                $.each(errors, function (index, value) {
                    pesan += value + "\n";
                });

                alert(pesan);
            },
            success: function () {
                alert('Sukses Menyimpan Data');

                $('#name').val(null);

                $('#name').focus();
            }
        });
    });
});