/**
 * Created by ocol on 09/11/16.
 */
$(document).ajaxStart(function() {
    Pace.restart();
});

function updateDataTableSelectAllCtrl(table) {
    var $table = table.table().node();
    var $chkbox_all = $('tbody input[type="checkbox"]', $table);
    var $chkbox_checked = $('tbody input[type="checkbox"]:checked', $table);
    var chkbox_select_all = $('thead input[name="select_all"]', $table).get(0);

    // If none of the checkboxes are checked
    if ($chkbox_checked.length === 0) {
        chkbox_select_all.checked = false;
        if ('indeterminate' in chkbox_select_all) {
            chkbox_select_all.indeterminate = false;
        }

        // If all of the checkboxes are checked
    } else if ($chkbox_checked.length === $chkbox_all.length) {
        chkbox_select_all.checked = true;
        if ('indeterminate' in chkbox_select_all) {
            chkbox_select_all.indeterminate = false;
        }

        // If some of the checkboxes are checked
    } else {
        chkbox_select_all.checked = true;
        if ('indeterminate' in chkbox_select_all) {
            chkbox_select_all.indeterminate = true;
        }
    }
}

function updateTotalBayar(table, data) {
    //var data = table.row( this ).data();

    var $table = table.table().node();
    var $chkbox_all = $('tbody input[type="checkbox"]', $table);
    var $chkbox_checked = $('tbody input[type="checkbox"]:checked', $table);
    var chkbox_select_all = $('thead input[name="select_all"]', $table).get(0);

    var api = globalapi;

    // If none of the checkboxes are checked
    if ($chkbox_checked.length === 0) {
        $(globalapi.column(2).footer()).html(
            //'Rp '+ numberfo pageTotal +' dari total Rp '+ total +''
            'Rp 0'
        );

        // If all of the checkboxes are checked
    } else if ($chkbox_checked.length === $chkbox_all.length) {
        var datasemua = table.table().data();
        var api = globalapi, datasemua;

        // Remove the formatting to get integer data for summation
        var intVal = function (i) {
            return typeof i === 'string' ?
            i.replace(/[\$,.]/g, '') * 1 :
                typeof i === 'number' ?
                    i : 0;
        };

        // Total over all pages

        total = api
            .column(2)
            .data()
            .reduce(function (a, b) {
                return intVal(a) + intVal(b);
            });

        //// Total over this page
        //pageTotal = api
        //    .column(2, {page: 'current'})
        //    .data()
        //    .reduce(function (a, b) {
        //        //if (api.column(2, {page: 'current'}).data() === '')
        //        return intVal(a) + intVal(b);
        //    }, 0);

        // Update footer
        $(api.column(2).footer()).html(
            //'Rp '+ numberfo pageTotal +' dari total Rp '+ total +''
            'Rp ' + number_format(total, 0, ',', '.')
        );
    } else {
        totalsekarang = $(api.column(2).footer()).html();
        var intTotalSekarang = totalsekarang.replace(/[\Rp,.]/g, '') * 1;

        var stringpesan = "total sekarang: " + intTotalSekarang;

        var hargaseleksi = data[2];
        var inthargaseleksi = hargaseleksi.replace(/[\Rp,.]/g, '') * 1;

        stringpesan += ", harga diseleksi" + inthargaseleksi;

        intTotalSekarang += inthargaseleksi;

        stringpesan += ", total menjadi: " + intTotalSekarang;
        //alert(stringpesan);

        $(api.column(2).footer()).html(
            //'Rp '+ numberfo pageTotal +' dari total Rp '+ total +''
            'Rp ' + number_format(intTotalSekarang, 0, ',', '.')
        );
    }

    //totalnya = 0;
    //alert("data check : " + $chkbox_checked);
    //
    //$chkbox_checked.each(function () {
    //    // Get row data
    //    var data = table.row(this).data();
    //
    //    // Get row ID
    //    var harga = data[2];
    //
    //    totalnya += harga;
    //});
}

function kurangTotalBayar(table, data) {
    //var data = table.row( this ).data();

    var $table = table.table().node();
    var $chkbox_all = $('tbody input[type="checkbox"]', $table);
    var $chkbox_checked = $('tbody input[type="checkbox"]:checked', $table);
    var chkbox_select_all = $('thead input[name="select_all"]', $table).get(0);

    var api = globalapi;

    // If none of the checkboxes are checked
    if ($chkbox_checked.length === 0) {
        $(globalapi.column(2).footer()).html(
            //'Rp '+ numberfo pageTotal +' dari total Rp '+ total +''
            'Rp 0'
        );

        // If all of the checkboxes are checked
    } else if ($chkbox_checked.length === $chkbox_all.length) {
        var datasemua = table.table().data();
        var api = globalapi, datasemua;

        // Remove the formatting to get integer data for summation
        var intVal = function (i) {
            return typeof i === 'string' ?
            i.replace(/[\$,.]/g, '') * 1 :
                typeof i === 'number' ?
                    i : 0;
        };

        // Total over all pages

        total = api
            .column(2)
            .data()
            .reduce(function (a, b) {
                return intVal(a) + intVal(b);
            });

        //// Total over this page
        //pageTotal = api
        //    .column(2, {page: 'current'})
        //    .data()
        //    .reduce(function (a, b) {
        //        //if (api.column(2, {page: 'current'}).data() === '')
        //        return intVal(a) + intVal(b);
        //    }, 0);

        // Update footer
        $(api.column(2).footer()).html(
            //'Rp '+ numberfo pageTotal +' dari total Rp '+ total +''
            'Rp ' + number_format(total, 0, ',', '.')
        );
    } else {
        totalsekarang = $(api.column(2).footer()).html();
        var intTotalSekarang = totalsekarang.replace(/[\Rp,.]/g, '') * 1;

        var stringpesan = "total sekarang: " + intTotalSekarang;

        var hargaseleksi = data[2];
        var inthargaseleksi = hargaseleksi.replace(/[\Rp,.]/g, '') * 1;

        stringpesan += ", harga diseleksi" + inthargaseleksi;

        intTotalSekarang -= inthargaseleksi;

        stringpesan += ", total menjadi: " + intTotalSekarang;
        //alert(stringpesan);

        $(api.column(2).footer()).html(
            //'Rp '+ numberfo pageTotal +' dari total Rp '+ total +''
            'Rp ' + number_format(intTotalSekarang, 0, ',', '.')
        );
    }

    //totalnya = 0;
    //alert("data check : " + $chkbox_checked);
    //
    //$chkbox_checked.each(function () {
    //    // Get row data
    //    var data = table.row(this).data();
    //
    //    // Get row ID
    //    var harga = data[2];
    //
    //    totalnya += harga;
    //});
}

function loadDaftarTunggakan() {
    var tableTunggakan = $('#dataTableBuilderTunggakan').dataTable();
    tableTunggakan.cleanData;
    tableTunggakan.api().ajax.reload();
};

var globalapi;

$(document).ready(function () {
    var rows_selected = [];

    var table = $('#dataTableBuilderTunggakan').DataTable({
        "footerCallback": function (row, data, start, end, display) {
            var api = this.api();
            globalapi = this.api();

            $( api.column( 2 ).footer() ).html(
                //'Rp '+ numberfo pageTotal +' dari total Rp '+ total +''
                'Rp 0'
            );
        },
        'ajax': {
            //'url': (($('#nisn-cari').val() == undefined || $('#nisn-cari').val() == '') ? '/daftartunggakan/XXXX' : '/daftartunggakan/' + $('#nisn-cari').val()),
            'url': '/daftartunggakan',
            'data': function (d) {
                d.keywords = $('#nisn-cari').val();
            }
        },
        "language": {
            "sProcessing": "Sedang memproses...",
            "sLengthMenu": "Tampilkan _MENU_ entri",
            "sZeroRecords": "Tidak ditemukan data yang sesuai",
            "sInfo": "Menampilkan _START_ sampai _END_ dari _TOTAL_ entri",
            "sInfoEmpty": "Menampilkan 0 sampai 0 dari 0 entri",
            "sInfoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
            "sInfoPostFix": "",
            "sSearch": "Cari:",
            "sUrl": "",
            "oPaginate": {
                "sFirst": "Pertama",
                "sPrevious": "Sebelumnya",
                "sNext": "Selanjutnya",
                "sLast": "Terakhir"
            }
        },
        'columnDefs': [{
            'targets': 0,
            'searchable': false,
            'orderable': false,
            'className': 'col-md-1 dt-body-center text-center',
            'render': function (data, type, full, meta) {
                return '<input type="checkbox">';
            }
        }, {
            'targets': 1,
            "sClass": "col-md-8"
        }, {
            'targets': 2,
            "sClass": "col-md-3 text-right font-18"
        }
        ],
        "ordering": false,
        //scrollY: 300,
        paging: false,
        searching: false,
        info: false,

        'rowCallback': function (row, data, dataIndex) {
            // Get row ID
            var rowId = data[0];

            // If row ID is in the list of selected row IDs
            if ($.inArray(rowId, rows_selected) !== -1) {
                $(row).find('input[type="checkbox"]').prop('checked', true);
                $(row).addClass('selected');
            }
        }
    });

    // Handle click on checkbox
    $('#dataTableBuilderTunggakan tbody').on('click', 'input[type="checkbox"]', function (e) {

        var $row = $(this).closest('tr');

        // Get row data
        var data = table.row($row).data();
        //alert('data: ' + data[2]);

        // Get row ID
        var rowId = data[0];

        // Determine whether row ID is in the list of selected row IDs
        var index = $.inArray(rowId, rows_selected);

        // If checkbox is checked and row ID is not in list of selected row IDs
        if (this.checked && index === -1) {
            rows_selected.push(rowId);

            // Otherwise, if checkbox is not checked and row ID is in list of selected row IDs
        } else if (!this.checked && index !== -1) {
            rows_selected.splice(index, 1);
        }

        if (this.checked) {
            $row.addClass('selected');
        } else {
            $row.removeClass('selected');
        }

        // Update state of "Select all" control
        updateDataTableSelectAllCtrl(table);

        if(this.checked) {
            updateTotalBayar(table, data);
        } else {
            kurangTotalBayar(table, data);
        }

        // Prevent click event from propagating to parent
        e.stopPropagation();
    });

    // Handle click on table cells with checkboxes
    $('#dataTableBuilderTunggakan').on('click', 'tbody td, thead th:first-child', function (e) {
        $(this).parent().find('input[type="checkbox"]').trigger('click');
    });

    // Handle click on "Select all" control
    $('#dataTableBuilderTunggakan thead input[name="select_all"]').on('click', function (e) {
        if (this.checked) {
            $('#dataTableBuilderTunggakan tbody input[type="checkbox"]:not(:checked)').trigger('click');
        } else {
            $('#dataTableBuilderTunggakantbody input[type="checkbox"]:checked').trigger('click');
        }

        // Prevent click event from propagating to parent
        e.stopPropagation();
    });

    // Handle table draw event
    table.on('draw', function () {
        // Update state of "Select all" control
        updateDataTableSelectAllCtrl(table);
    });

    // Handle form submission event
    $('#simpan').on('click', function (e) {

        var route = "/iuran";
        var token = $('#token').val();

        var kodetransaksi = $('.kodetransaksi').val();
        if (kodetransaksi == '' || kodetransaksi == ' ' || kodetransaksi == undefined) {
            alert('Terjadi kesalahan pada halaman anda ! Silahkan refresh page ini !!');
            return;
        }


        var nisncari = $('#nisn-cari').val();
        var nisn = $('.nisn-asli').val();

        //console.log(nisn + ', ' + nisncari);

        if (nisncari != nisn) {
            alert('NISN tidak valid! Silahkan ulangi lagi pencarian NISN Siswa !!!');
            $('#nisn-cari').focus();
            return;
        }

        if (nisn == '' || nisn == ' ' || nisn == undefined) {
            alert('NISN Siswa harus ada dan tidak boleh kosong !');
            $('#nisn').focus();
            return;
        }

        if (jQuery.isEmptyObject(rows_selected)) {
            alert('Anda belum memilih bulan tunggakan yang akan di bayarkan !');
            return;
        }

        var data_seleksi = rows_selected;
        //data_seleksi.sort();
        data_seleksi.sort(function(a,b) {return a-b;});

        //console.log(data_seleksi);


        var dataLama;
        var dataBaru;

        var statusSalah = false;
        $.each(data_seleksi, function (index, rowId) {
            if (dataBaru == undefined) {
                if (rowId != 1) {
                    statusSalah = true;
                    return false;
                }

                dataLama = rowId;
                dataBaru = rowId;
            } else {
                dataLama = dataBaru;
                dataBaru = rowId;
            }

            //console.log('l:' + dataLama + ', b: ' + dataBaru);

            if (intVal(dataLama) != intVal(dataBaru)) {
                if ((intVal(dataBaru) - intVal(dataLama)) > 1) {
                    //console.log(dataLama + ', ' + dataBaru);
                    statusSalah = true;

                    return false;
                }
            }
        });

        if (statusSalah) {
            alert('Bulan tunggakan yang akan di bayarkan harus berurut mulai dari 1 - n !');
            return;
        }


        $.ajax({
            url: route,
            type: 'POST',
            headers: {'X-CSRF-TOKEN': token},
            dataType: 'json',
            data: {
                kodetransaksi: kodetransaksi,
                nisn: nisn,
                data_seleksi: data_seleksi,
                _token: token
            },
            error: function (res) {
                var errors = res.responseJSON;
                var pesan = '';
                $.each(errors, function (index, value) {
                    pesan += value + "\n";
                });

                alert(pesan);
            },
            success: function () {
                alert('Sukses Menyimpan Data');
                window.location.href = "/iuran/nota/" + kodetransaksi;
            }
        });

    });


    document.getElementById("nisn-cari").maxLength = 10;

    $('.nisn-asli').val(null);

    var timer;

    $('#nisn-cari').on('keydown', function(evt) {
        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode == 13) {
            clearTimeout(timer);
        }
    }).on('keyup', function(e) {
        timer = setTimeout(function () {
            //alert('disini');
            var keywords = $('#nisn-cari').val();

            if (keywords.length > 0) {
                route = "/siswabynisn/" + keywords;

                $.get(route, function (res) {
                    if (jQuery.isEmptyObject(res)) {
                        $('.nisn-asli').val('');
                        $('#nama').val('NISN Siswa tidak ditemukan');

                        rows_selected = [];
                    } else {
                        $('.nisn-asli').val(res.nisn);
                        $('#nisn-cari').val(res.nisn);
                        $('#nama').val(res.nama);
                    }

                    loadDaftarTunggakan();

                });
            }
        }, 500);
    });

    $('#dataTableBuilder').DataTable({
        //scrollX: true,
        //scrollColapse:true,
        responsive: true,
        'ajax': {
            'url': '/get_daftar_siswa',
        },
        'columnDefs': [{
            'targets': 4,
            'searchable': false,
            "orderable": false,
            "orderData": false,
            "orderDataType": false,
            "orderSequence": false,
            "sClass": "text-center col-lg-1 td-aksi",
            'render': function (data, type, full, meta) {
                var kembali = '';

                kembali += '<button title="Pilih Data" class="btn btn-success btn-flat" onclick="PilihClick(this);"><i class="fa fa-hand-pointer-o fa-fw"></i> </button>';


                return kembali;

            }
        },{
            'targets':0,
            'sClass': "col-lg-2"
        }, {
            'targets':1,
            'sClass': "col-lg-4"
        },{
            'targets':2,
            'sClass': "col-lg-2"
        },{
            'targets':3,
            'sClass': "col-lg-3"
        }],
        'rowCallback': function (row, data, dataIndex) {
            $(row).find('button[class="btn btn-success btn-flat"]').prop('value', data[4]);
        }
    });


    $('#carisiswa').click(function() {
        reloadTableSiswa();
    });

    getKodeOtomatis();

});

function getKodeOtomatis () {
    $.get('/getkodetransaksi', function (res) {
        if (jQuery.isEmptyObject(res)) {
            $('.kodetransaksi').val('');
        } else {
            $('.kodetransaksi').val(res.kode);
        }
    });
}

function reloadTableSiswa() {
    var table = $('#dataTableBuilder').dataTable();
    table.cleanData;
    table.api().ajax.reload();
}


function PilihClick(btn) {
    route = "/siswa/" + btn.value + "/edit";

    $.get(route, function (res) {
        $('.nisn-asli').val(res.nisn);
        $('#nisn-cari').val(res.nisn);
        $('#nama').val(res.nama);

        $('#nisn-cari').focus();

        $('#modalCari').modal('toggle');

        loadDaftarTunggakan();

    });
}