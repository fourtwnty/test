/**
 * Created by ocol on 09/11/16.
 */
$(document).ajaxStart(function() {
    Pace.restart();
});

$(document).ready(function () {
    var route = "/cekhakakses/cetak_iuran";
    var bolehCetak;
    $.get(route, function (res) {
        bolehCetak = res;
    });

    var route = "/cekhakakses/hapus_iuran";
    var bolehHapus;
    $.get(route, function (res) {
        bolehHapus = res;
    });


    $('#dataTableBuilder').DataTable({
        //scrollX: true,
        //scrollColapse:true,
        responsive: true,
        'ajax': {
            'url': '/iurans',
        },
        'columnDefs': [{
            'targets': 6,
            'searchable': false,
            "orderable": false,
            "orderData": false,
            "orderDataType": false,
            "orderSequence": false,
            "sClass": "text-center col-md-1 td-aksi",
            'render': function (data, type, full, meta) {
                var kembali = '';
                if (bolehCetak == true) {
                    kembali += '<button title="Cetak Nota" class="btn btn-info btn-flat" onclick="CetakClick(this);"><i class="fa fa-print fa-fw"></i> </button>';
                }
                if (bolehHapus == true) {
                    kembali += '<button title="Hapus Data" class="btn btn-danger btn-flat" data-toggle="modal" data-target="#modalHapus" onclick="HapusClick(this);"><i class="fa fa-trash fa-fw"></i> </button>';
                }

                return kembali;

            }
        },{
            'targets':0,
            'sClass': "col-md-1"
        },{
            'targets':1,
            'sClass': "col-md-2"
        },{
            'targets':2,
            'sClass': "col-md-1"
        },{
            'targets':3,
            'sClass': "col-md-3"
        },{
            'targets':4,
            'sClass': "col-md-2 text-right",
            'render': function (data, type, full, meta) {
                return formatRibuan(data);

            }
        },{
            'targets':5,
            'sClass': "col-md-2"
        }],
        'rowCallback': function (row, data, dataIndex) {
            if (bolehCetak == true) {
                $(row).find('button[class="btn btn-info btn-flat"]').prop('value', data[6]);
            }
            if (bolehHapus == true) {
                $(row).find('button[class="btn btn-danger btn-flat"]').prop('value', data[6]);
            }

        }
    });

    //document.getElementById("name").maxLength = 100;


});

function reloadTable() {
    var table = $('#dataTableBuilder').dataTable();
    table.cleanData;
    table.api().ajax.reload();
}


function CetakClick(btn) {
    window.location.href = "/iuran/nota/" + btn.value;
}
function HapusClick(btn) {
    $('#idHapus').val(btn.value);
}

$('#yakinhapus').click(function () {
    var token = $('#token').val();
    var id = $('#idHapus').val();
    var route = "/iuran/" + id;

    $.ajax({
        url: route,
        headers: {'X-CSRF-TOKEN': token},
        type: 'DELETE',
        dataType: 'json',
        error: function (res) {
            var errors = res.responseJSON;
            var pesan = '';
            $.each(errors, function (index, value) {
                pesan += value + "\n";
            });

            alert(pesan);
        },
        success: function () {
            reloadTable();
            alert('Sukses Menghapus Data');
            $('#modalHapus').modal('toggle');
        }
    });
});