/**
 * Created by ocol on 09/11/16.
 */
$(document).ajaxStart(function() {
    Pace.restart();


});

$(document).ready(function () {
    var route = "/cekhakakses/atur_pengesah";
    var bolehUbah;
    $.get(route, function (res) {
        bolehUbah = res;
    });

    $('#dataTableBuilder').DataTable({
        //scrollX: true,
        //scrollColapse:true,
        responsive: true,
        'ajax': {
            'url': '/pengesahs',
        },
        'columnDefs': [{
            'targets': 4,
            'searchable': false,
            "orderable": false,
            "orderData": false,
            "orderDataType": false,
            "orderSequence": false,
            "sClass": "text-center col-lg-1 td-aksi",
            'render': function (data, type, full, meta) {
                var kembali = '';
                if (bolehUbah == true) {
                    kembali += '<button title="Ubah Data" class="btn btn-warning btn-flat" data-toggle="modal" data-target="#modalUbah" onclick="UbahClick(this);"><i class="fa fa-pencil-square-o fa-fw"></i> </button>';
                }

                return kembali;

            }
        },{
            'targets':0,
            'sClass': "col-lg-2"
        }, {
            'targets':1,
            'sClass': "col-lg-2"
        },{
            'targets':2,
            'sClass': "col-lg-5"
        },{
            'targets':3,
            'sClass': "col-lg-2"
        }],
        'rowCallback': function (row, data, dataIndex) {
            if (bolehUbah == true) {
                $(row).find('button[class="btn btn-warning btn-flat"]').prop('value', data[4]);
            }

        }
    });

    document.getElementById("name").maxLength = 100;
    document.getElementById("display_name").maxLength = 200;
    document.getElementById("nilai").maxLength = 200;
    document.getElementById("nip").maxLength = 18;
});

function reloadTable() {
    var table = $('#dataTableBuilder').dataTable();
    table.cleanData;
    table.api().ajax.reload();
}

function UbahClick(btn) {
    route = "/pengesah/" + btn.value + "/edit";

    $.get(route, function (res) {
        $('#id').val(res.id);
        $('#name').val(res.name);
        $('#display_name').val(res.display_name);
        $('#nilai').val(res.nilai);
        $('#nip').val(res.nip);

        $('#display_name').focus();

    });

}

$('#simpan').click(function () {
    var id = $('#id').val();
    var token = $('#token').val();
    var route = "/pengesah/" + id;

    var name = $('#name').val();
    if (name == '' || name == undefined) {
        alert('Nama Pengesah tidak boleh dikosongkan');
        $('#name').focus();
        return;
    }

    var display_name = $('#display_name').val();
    if (display_name == '' || display_name == undefined) {
        alert('Nama yang akan terlihat tidak boleh dikosongkan');
        $('#display_name').focus();
        return;
    }

    var nilai = $('#nilai').val();
    if (nilai == '' || nilai == undefined) {
        alert('Nilai Pengesah tidak boleh dikosongkan');
        $('#nilai').focus();
        return;
    }

    var nip = $('#nip').val();
    if (nip == '' || nip == undefined) {
        alert('NIP Pengesah tidak boleh dikosongkan');
        $('#nip').focus();
        return;
    }



    $.ajax({
        url: route,
        headers: {'X-CSRF-TOKEN': token},
        type: 'PUT',
        dataType: 'json',
        data: {
            name: name,
            display_name: display_name,
            nilai: nilai,
            nip: nip,
            _token: token
        },
        error: function (res) {
            var errors = res.responseJSON;
            var pesan = '';
            $.each(errors, function (index, value) {
                pesan += value + "\n";
            });

            alert(pesan);
        },
        success: function () {
            reloadTable();
            alert('Sukses Mengubah Data');
            $('#modalUbah').modal('toggle');
        }
    });
});