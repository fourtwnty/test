/**
 * Created by ocol on 08/11/16.
 */

$(document).ajaxStart(function() {
    Pace.restart();

    document.getElementById("nama").maxLength = 100;
    document.getElementById("alamat").maxLength = 200;
    document.getElementById("telp").maxLength = 20;
    document.getElementById("fax").maxLength = 20;
    document.getElementById("dinas").maxLength = 100;
    document.getElementById("kota").maxLength = 100;
    document.getElementById("kelurahan").maxLength = 100;
    document.getElementById("kecamatan").maxLength = 100;
    document.getElementById("kabupaten").maxLength = 100;

    var route = "/getidentitas";

    $.get(route, function (res) {
        $('#id').val(res.id);
        $('#nama').val(res.nama);
        $('#alamat').val(res.alamat);
        $('#telp').val(res.telp);
        $('#fax').val(res.fax);
        $('#logo').val(res.logo);

        if (res.srclogogambar != undefined && res.srclogogambar != null) {
            //$('#img-profile-logo').sr
            $('#img-profile-logo').attr('src', res.srclogogambar);
        }

        $('#dinas').val(res.dinas);
        $('#kota').val(res.kota);
        $('#kelurahan').val(res.kelurahan);
        $('#kecamatan').val(res.kecamatan);
        $('#kabupaten').val(res.kabupaten);

        $('#form-identitas').attr('action', '/identitas/' + res.id);

        $('#nama').focus();


    });
});

function reloadPage () {
    $.ajax({
        url: '#',
        success: function(result) {

        }
    });
}

$('#batal').click(function(){
    $.ajax({
        url: '#',
        success: function(result) {

        }
    });
})


$('form').submit(function () {
    //$('#id').val(res.id);
    //$('#nama').val(res.nama);
    var nama = $('#nama').val();
    if (nama == '' || nama == undefined) {
        alert('Nama tidak boleh dikosongkan');
        $('#nama').focus();
        return false;
    }
    //$('#alamat').val(res.alamat);
    var alamat = $('#alamat').val();
    if (alamat == '' || alamat == undefined) {
        alert('Alamat tidak boleh dikosongkan');
        $('#alamat').focus();
        return false;
    }
    //$('#telp').val(res.telp);
    var telp = $('#telp').val();
    if (telp == undefined) {
        telp = '';
    }

    telp = formatNomorTelepon(telp);
    $('#telp').val(telp);

    //$('#fax').val(res.fax);
    var fax = $('#fax').val();
    if (fax == undefined) {
        fax = '';
    }

    fax = formatNomorTelepon(fax);
    $('#fax').val(fax);
    //$('#logo').val(res.logo);
    //
    //if (res.srclogogambar != undefined && res.srclogogambar != null) {
    //    //$('#img-profile-logo').sr
    //    $('#img-profile-logo').attr('src', res.srclogogambar);
    //}
    var logobaru = $('#logo-baru').val();
    if (logobaru == undefined) {
        logobaru = '';
    }

    //alert('logo baru = [' + logobaru + ']');

    //$('#dinas').val(res.dinas);
    var dinas = $('#dinas').val();
    if (dinas == '' || dinas == undefined) {
        alert('Nama dinas tidak boleh kosong');
        $('#dinas').focus();
        return false;
    }

    //$('#kota').val(res.kota);
    var kota = $('#kota').val();
    if (kota == '' || kota == undefined) {
        alert('Nama kota tidak boleh kosong');
        $('#kota').focus();
        return false;
    }

    //$('#kelurahan').val(res.kelurahan);
    var kelurahan = $('#kelurahan').val();
    if (kelurahan == '' || kelurahan == undefined) {
        alert('Nama kelurahan tidak boleh kosong');
        $('#kelurahan').focus();
        return false;
    }

    //$('#kecamatan').val(res.kecamatan);
    var kecamatan = $('#kecamatan').val();
    if (kecamatan == '' || kecamatan == undefined) {
        alert('Nama kecamatan tidak boleh kosong');
        $('#kecamatan').focus();
        return false;
    }

    //$('#kabupaten').val(res.kabupaten);
    var kabupaten = $('#kabupaten').val();
    if (kabupaten == '' || kabupaten == undefined) {
        alert('Nama kabupaten tidak boleh kosong');
        $('#kabupaten').focus();
        return false;
    }
});

//$('#simpan').click(function () {
//    var id = $('#id').val();
//    var token = $('#token').val();
//    var route = "/identitas/" + id;
//
//    //$('#id').val(res.id);
//    //$('#nama').val(res.nama);
//    var nama = $('#nama').val();
//    if (nama == '' || nama == undefined) {
//        alert('Nama tidak boleh dikosongkan');
//        $('#nama').focus();
//        return;
//    }
//    //$('#alamat').val(res.alamat);
//    var alamat = $('#alamat').val();
//    if (alamat == '' || alamat == undefined) {
//        alert('Alamat tidak boleh dikosongkan');
//        $('#alamat').focus();
//        return;
//    }
//    //$('#telp').val(res.telp);
//    var telp = $('#telp').val();
//    if (telp == undefined) {
//        telp = '';
//    }
//
//    telp = formatNomorTelepon(telp);
//    //$('#fax').val(res.fax);
//    var fax = $('#fax').val();
//    if (fax == undefined) {
//        fax = '';
//    }
//
//    fax = formatNomorTelepon(fax);
//    //$('#logo').val(res.logo);
//    //
//    //if (res.srclogogambar != undefined && res.srclogogambar != null) {
//    //    //$('#img-profile-logo').sr
//    //    $('#img-profile-logo').attr('src', res.srclogogambar);
//    //}
//    var logobaru = $('#logo-baru').val();
//    if (logobaru == undefined) {
//        logobaru = '';
//    }
//
//    //alert('logo baru = [' + logobaru + ']');
//
//    //$('#dinas').val(res.dinas);
//    var dinas = $('#dinas').val();
//    if (dinas == '' || dinas == undefined) {
//        alert('Nama dinas tidak boleh kosong');
//        $('#dinas').focus();
//        return;
//    }
//
//    //$('#kota').val(res.kota);
//    var kota = $('#kota').val();
//    if (kota == '' || kota == undefined) {
//        alert('Nama kota tidak boleh kosong');
//        $('#kota').focus();
//        return;
//    }
//
//    //$('#kelurahan').val(res.kelurahan);
//    var kelurahan = $('#kelurahan').val();
//    if (kelurahan == '' || kelurahan == undefined) {
//        alert('Nama kelurahan tidak boleh kosong');
//        $('#kelurahan').focus();
//        return;
//    }
//
//    //$('#kecamatan').val(res.kecamatan);
//    var kecamatan = $('#kecamatan').val();
//    if (kecamatan == '' || kecamatan == undefined) {
//        alert('Nama kecamatan tidak boleh kosong');
//        $('#kecamatan').focus();
//        return;
//    }
//
//    //$('#kabupaten').val(res.kabupaten);
//    var kabupaten = $('#kabupaten').val();
//    if (kabupaten == '' || kabupaten == undefined) {
//        alert('Nama kabupaten tidak boleh kosong');
//        $('#kabupaten').focus();
//        return;
//    }
//
//    $.ajax({
//        url: route,
//        headers: {'X-CSRF-TOKEN': token},
//        type: 'PUT',
//        dataType: 'json',
//        data: {
//            nama :nama,
//            alamat :alamat,
//            telp :telp,
//            fax :fax,
//            logobaru :logobaru,
//            dinas :dinas,
//            kota :kota,
//            kelurahan :kelurahan,
//            kabupaten :kabupaten,
//            kecamatan :kecamatan,
//            _token: token
//        },
//        error: function (res) {
//            var errors = res.responseJSON;
//            var pesan = '';
//            $.each(errors, function (index, value) {
//                pesan += value + "\n";
//            });
//
//            alert(pesan);
//        },
//        success: function () {
//            alert('Sukses Mengubah Identitas Sekolah');
//            reloadPage();
//        }
//    });
//});