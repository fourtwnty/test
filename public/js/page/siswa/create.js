/**
 * Created by ocol on 09/11/16.
 */
$(document).ajaxStart(function() {
    Pace.restart();
});

$(document).ready(function () {
    var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";

    $('.tgllahir').datepicker({
        format: "mm/dd/yyyy",
        container: container,
        todayHighlight: true,
        autoclose: true,
    });

    var route = "/kelases";
    var inputTipe = $('#kelas_id');

    var list = document.getElementById("kelas_id");
    while (list.hasChildNodes()) {
        list.removeChild(list.firstChild);
    }
    inputTipe.append('<option value=" ">Pilih Kelas Siswa</option>');

    $.get(route, function (res) {
        $.each(res.data, function (index, value) {
            inputTipe.append('<option value="' + value[0] + '">' + value[1] + '</option>');
        });
    });

    $('.text-alamat').keydown(function () {
        var len = this.value.length;
        if (len >= 200) {
            this.value = this.value.substring(0,200);
        }
    });

    document.getElementById("nisn").maxLength = 10;
    document.getElementById("nama").maxLength = 100;
    document.getElementById("tempat_lahir").maxLength = 50;

    $("#kelas_id").select2();
    $("#jk").select2();
    $("#aktif").select2();

    $('#nisn').focus();

    $('#simpan').click(function() {
        var route = "/siswa";
        var token = $('#token').val();

        var nisn = $('#nisn').val();
        if (nisn == '' || nisn == ' ' || nisn == undefined) {
            alert('NISN Siswa harus ada dan tidak boleh kosong !');
            $('#nisn').focus();
            return;
        }

        var nama = $('#nama').val();
        if (nama == '' || nama == ' ' || nama == undefined) {
            alert('Nama Siswa harus ada dan tidak boleh kosong !');
            $('#nama').focus();
            return;
        }

        var jk = $('#jk').val();
        if (jk == '' || jk == ' ' || jk == undefined || (jk != 'L' && jk != 'P')) {
            alert('Pilih jenis kelamin dengan benar !');
            $('#jk').focus();
            return;
        }

        var tempat_lahir = $('#tempat_lahir').val();
        if (tempat_lahir == '' || tempat_lahir == ' ' || tempat_lahir == undefined) {
            alert('Tempat Kelahiran Siswa harus ada dan tidak boleh kosong !');
            $('#tempat_lahir').focus();
            return;
        }

        var tgl_lahir = $("#tgl_lahir").val();
        if (tgl_lahir == '' || tgl_lahir==' ' || tgl_lahir == undefined) {
            alert('Inputkan tanggal lahir dengan benar !');
            $('#tgl_lahir').focus();
            return;
        }

        var kelas_id = $('#kelas_id').val();
        if (kelas_id == '' || kelas_id == ' ' || kelas_id == undefined) {
            alert('Pilih kelas siswa dengan benar !');
            $('#kelas_id').focus();
            return;
        }

        var aktif = $('#aktif').val();
        if (aktif == '' || aktif == ' ' || aktif == undefined || (aktif != '0' && aktif != '1')) {
            alert('Pilih status aktif siswa dengan benar !');
            $('#aktif').focus();
            return;
        }

        var alamat = $('#alamat').val();
        if (alamat == '' || alamat == ' ' || alamat == undefined) {
            alert('Alamat siswa harus ada dan tidak boleh kosong !');
            $('#alamat').focus();
            return;
        }

        $.ajax({
            url: route,
            type: 'POST',
            headers: {'X-CSRF-TOKEN': token},
            dataType: 'json',
            data: {
                nisn: nisn,
                nama: nama,
                jk: jk,
                tempat_lahir: tempat_lahir,
                tgl_lahir: tgl_lahir,
                kelas_id: kelas_id,
                aktif: aktif,
                alamat: alamat,
                _token: token
            },
            error: function (res) {
                var errors = res.responseJSON;
                var pesan = '';
                $.each(errors, function (index, value) {
                    pesan += value + "\n";
                });

                alert(pesan);
            },
            success: function () {
                alert('Sukses Menyimpan Data');

                $('#nisn').val(null);
                $('#nama').val(null);
                $('#jk').select2('val', ' ');
                $('#tempat_lahir').val(null);
                $('#tgl_lahir').val(null);
                $('#kelas_id').select2('val', ' ');
                $('#aktif').select2('val', ' ');
                $('#alamat').val(null);

                $('#nisn').focus();
            }
        });
    });

});