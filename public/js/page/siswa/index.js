/**
 * Created by ocol on 09/11/16.
 */
$(document).ajaxStart(function() {
    Pace.restart();
});

$(document).ready(function () {
    var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";

    $('.tgllahir').datepicker({
        format: "mm/dd/yyyy",
        container: container,
        todayHighlight: true,
        autoclose: true,
    });

    var route = "/kelases";
    var inputTipe = $('#kelas_id');

    var list = document.getElementById("kelas_id");
    while (list.hasChildNodes()) {
        list.removeChild(list.firstChild);
    }
    inputTipe.append('<option value=" ">Pilih Kelas Siswa</option>');

    $.get(route, function (res) {
        $.each(res.data, function (index, value) {
            inputTipe.append('<option value="' + value[0] + '">' + value[1] + '</option>');
        });
    });

    $("#kelas_id").select2();
    $("#jk").select2();
    $("#aktif").select2();

    $('.text-alamat').keydown(function () {
        var len = this.value.length;
        if (len >= 200) {
            this.value = this.value.substring(0,200);
        }
    });

    document.getElementById("nisn").maxLength = 10;
    document.getElementById("nama").maxLength = 100;
    document.getElementById("tempat_lahir").maxLength = 50;


    var route = "/cekhakakses/ubah_siswa";
    var bolehUbah;
    $.get(route, function (res) {
        bolehUbah = res;
    });

    var route = "/cekhakakses/hapus_siswa";
    var bolehHapus;
    $.get(route, function (res) {
        bolehHapus = res;
    });


    $('#dataTableBuilder').DataTable({
        //scrollX: true,
        //scrollColapse:true,
        responsive: true,
        'ajax': {
            'url': '/siswas',
        },
        'columnDefs': [{
            'targets': 5,
            'searchable': false,
            "orderable": false,
            "orderData": false,
            "orderDataType": false,
            "orderSequence": false,
            "sClass": "text-center col-lg-1 td-aksi",
            'render': function (data, type, full, meta) {
                var kembali = '';
                if (bolehUbah == true) {
                    kembali += '<button title="Ubah Data" class="btn btn-warning btn-flat" data-toggle="modal" data-target="#modalUbah" onclick="UbahClick(this);"><i class="fa fa-pencil-square-o fa-fw"></i> </button>';
                }
                if (bolehHapus == true) {
                    kembali += '<button title="Hapus Data" class="btn btn-danger btn-flat" data-toggle="modal" data-target="#modalHapus" onclick="HapusClick(this);"><i class="fa fa-trash fa-fw"></i> </button>';
                }

                return kembali;

            }
        },{
            'targets':0,
            'sClass': "col-lg-2"
        }, {
            'targets':1,
            'sClass': "col-lg-4"
        },{
            'targets':2,
            'sClass': "col-lg-1"
        },{
            'targets':3,
            'sClass': "col-lg-3"
        }, {
            'targets':4,
            'sClass': "col-lg-1"
        }],
        'rowCallback': function (row, data, dataIndex) {
            if (bolehUbah == true) {
                $(row).find('button[class="btn btn-warning btn-flat"]').prop('value', data[5]);
            }
            if (bolehHapus == true) {
                $(row).find('button[class="btn btn-danger btn-flat"]').prop('value', data[5]);
            }

        }
    });
});

function reloadTable() {
    var table = $('#dataTableBuilder').dataTable();
    table.cleanData;
    table.api().ajax.reload();
}

function UbahClick(btn) {
    route = "/siswa/" + btn.value + "/edit";

    $.get(route, function (res) {
        $('#nisn').val(res.nisn);
        $('#nama').val(res.nama);
        //$('#jk').val(''+ res.jk);
        $('#tempat_lahir').val(res.tempat_lahir);
        $('#tgl_lahir').val(res.tgl_lahir);

        $("#jk").select2('val','' + res.jk);
        $("#kelas_id").select2('val','' + res.kelas_id);
        $("#aktif").select2('val','' + res.aktif);

        //$('#kelas_id').val('' + res.kelas_id);
        //$('#aktif').val('' + res.aktif);
        $('#alamat').val(res.alamat);
        $('#id').val(res.id);

        $('#nisn').focus();

    });

}

$('#simpan').click(function () {
    var id = $('#id').val();
    var token = $('#token').val();
    var route = "/siswa/" + id;

    var nisn = $('#nisn').val();
    if (nisn == '' || nisn == ' ' || nisn == undefined) {
        alert('NISN Siswa harus ada dan tidak boleh kosong !');
        $('#nisn').focus();
        return;
    }

    var nama = $('#nama').val();
    if (nama == '' || nama == ' ' || nama == undefined) {
        alert('Nama Siswa harus ada dan tidak boleh kosong !');
        $('#nama').focus();
        return;
    }

    var jk = $('#jk').val();
    if (jk == '' || jk == ' ' || jk == undefined || (jk != 'L' && jk != 'P')) {
        alert('Pilih jenis kelamin dengan benar !');
        $('#jk').focus();
        return;
    }

    var tempat_lahir = $('#tempat_lahir').val();
    if (tempat_lahir == '' || tempat_lahir == ' ' || tempat_lahir == undefined) {
        alert('Tempat Kelahiran Siswa harus ada dan tidak boleh kosong !');
        $('#tempat_lahir').focus();
        return;
    }

    var tgl_lahir = $("#tgl_lahir").val();
    if (tgl_lahir == '' || tgl_lahir==' ' || tgl_lahir == undefined) {
        alert('Inputkan tanggal lahir dengan benar !');
        $('#tgl_lahir').focus();
        return;
    }

    var kelas_id = $('#kelas_id').val();
    if (kelas_id == '' || kelas_id == ' ' || kelas_id == undefined) {
        alert('Pilih kelas siswa dengan benar !');
        $('#kelas_id').focus();
        return;
    }

    var aktif = $('#aktif').val();
    if (aktif == '' || aktif == ' ' || aktif == undefined || (aktif != '0' && aktif != '1')) {
        alert('Pilih status aktif siswa dengan benar !');
        $('#aktif').focus();
        return;
    }

    var alamat = $('#alamat').val();
    if (alamat == '' || alamat == ' ' || alamat == undefined) {
        alert('Alamat siswa harus ada dan tidak boleh kosong !');
        $('#alamat').focus();
        return;
    }

    $.ajax({
        url: route,
        headers: {'X-CSRF-TOKEN': token},
        type: 'PUT',
        dataType: 'json',
        data: {
            nisn: nisn,
            nama: nama,
            jk: jk,
            tempat_lahir: tempat_lahir,
            tgl_lahir: tgl_lahir,
            kelas_id: kelas_id,
            aktif: aktif,
            alamat: alamat,
            _token: token
        },
        error: function (res) {
            var errors = res.responseJSON;
            var pesan = '';
            $.each(errors, function (index, value) {
                pesan += value + "\n";
            });

            alert(pesan);
        },
        success: function () {
            reloadTable();
            alert('Sukses Mengubah Data');
            $('#modalUbah').modal('toggle');
        }
    });
});

function HapusClick(btn) {
    $('#idHapus').val(btn.value);
}

$('#yakinhapus').click(function () {
    var token = $('#token').val();
    var id = $('#idHapus').val();
    var route = "/siswa/" + id;

    $.ajax({
        url: route,
        headers: {'X-CSRF-TOKEN': token},
        type: 'DELETE',
        dataType: 'json',
        error: function (res) {
            var errors = res.responseJSON;
            var pesan = '';
            $.each(errors, function (index, value) {
                pesan += value + "\n";
            });

            alert(pesan);
        },
        success: function () {
            reloadTable();
            alert('Sukses Menghapus Data');
            $('#modalHapus').modal('toggle');
        }
    });
});