/**
 * Created by ocol on 10/11/16.
 */
$(document).ajaxStart(function() {
    Pace.restart();
});

$(document).ready(function () {

    document.getElementById("password-lama").maxLength = 50;
    document.getElementById("validate_password").maxLength = 50;
    document.getElementById("password-baru").maxLength = 50;

    $('#password-lama').focus();

    $('#simpan').click(function() {
        var route = "/change_password";
        var token = $('#token').val();

        var passwordlama = $('#password-lama').val();
        if (passwordlama == '' || passwordlama == undefined) {
            alert('Kata sandi lama tidak boleh kosong');
            $('#password-lama').focus();
            return;
        }

        var passwordbaru = $('#password-baru').val();
        if (passwordbaru == '' || passwordbaru == undefined) {
            alert('Kata sandi baru tidak boleh kosong');
            $('#password-baru').focus();
            return;
        }

        var validate_password = $('#validate_password').val();
        if (validate_password == '' || validate_password == undefined) {
            alert('Silahkan melakukan validasi kata sandi baru !!!');
            $('#validate_password').focus();
            return;
        }

        if (validate_password != passwordbaru) {
            alert('Validasi kata sandi baru tidak cocok !!!');
            $('#validate_password').val('');
            $('#validate_password').focus();
            return;
        }

        $.ajax({
            url: route,
            type: 'POST',
            headers: {'X-CSRF-TOKEN': token},
            dataType: 'json',
            data: {
                passwordbaru : passwordbaru,
                passwordlama : passwordlama,

                _token: token
            },
            error: function (res) {
                var errors = res.responseJSON;
                var pesan = '';
                $.each(errors, function (index, value) {
                    pesan += value + "\n";
                });

                alert(pesan);
            },
            success: function () {
                alert('Sukses mengubah kata sandi');

                window.location.href = "/";
            }
        });
    });
});