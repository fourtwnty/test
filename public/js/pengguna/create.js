/**
 * Created by ocol on 10/11/16.
 */
$(document).ajaxStart(function() {
    Pace.restart();
});

$(document).ready(function () {
    document.getElementById("email").maxLength = 100;
    document.getElementById("fullname").maxLength = 200;
    document.getElementById("username").maxLength = 50;
    document.getElementById("password").maxLength = 50;
    document.getElementById("validate_password").maxLength = 50;
    document.getElementById("address").maxLength = 200;
    document.getElementById("gsm").maxLength = 20;
    document.getElementById("phone").maxLength = 20;
    document.getElementById("description").maxLength = 200;

    $("#active").attr('checked', true);

    var route = "/get_select_group";
    var inputTipe = $('#role');

    var list = document.getElementById("role");
    while (list.hasChildNodes()) {
        list.removeChild(list.firstChild);
    }
    inputTipe.append('<option value=" ">Pilih Grup Pengguna</option>');

    $.get(route, function (res) {
        $.each(res.data, function (index, value) {
            inputTipe.append('<option value="' + value[0] + '">' + value[1] + '</option>');
        });
    });

    $("#role").select2();
    $("#gender").select2();

    $('#username').focus();



    $('#simpan').click(function() {
        var route = "/pengguna";
        var token = $('#token').val();

        var username = $('#username').val();
        if (username == '' || username == undefined) {
            alert('Nama Pengguna tidak boleh dikosongkan');
            $('#username').focus();
            return;
        }

        var email = $('#email').val();
        if (email == '' || email == undefined) {
            alert('Alamat Email tidak boleh dikosongkan');
            $('#email').focus();
            return;
        }

        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

        if(re.test(email) == false) {
            alert('Format penulisan E-mail tidak benar!');
            $('#email').focus();
            return;
        }

        var role = $('#role').val();
        if (role == '' || role == ' ' || role == undefined) {
            alert('Pilih Grup Pengguna dengan benar!');
            $('#role').focus();
            return;
        }

        var fullname = $('#fullname').val();
        if (fullname == '' || fullname == undefined) {
            alert('Nama Lengkap tidak boleh dikosongkan');
            $('#fullname').focus();
            return;
        }

        var password = $('#password').val();
        if (password == '' || password == undefined) {
            alert('Kata Sandi tidak boleh kosong');
            $('#password').focus();
            return;
        }

        var validate_password = $('#validate_password').val();
        if (validate_password == '' || validate_password == undefined) {
            alert('Silahkan melakukan validasi kata sandi!!!');
            $('#validate_password').focus();
            return;
        }

        if (validate_password != password) {
            alert('Validasi Kata Sandi tidak cocok !!!');
            $('#validate_password').val('');
            $('#validate_password').focus();
            return;
        }

        var address = $('#address').val();
        if (address == '' || address == undefined) {
            alert('Alamat belum diinputkan');
            $('#address').focus();
            return;
        }

        var gsm = $('#gsm').val();
        if (gsm == undefined) {
            gsm = '';
        }

        gsm = formatNomorTelepon(gsm);

        var phone = $('#phone').val();
        if (phone == undefined) {
            phone = '';
        }

        phone = formatNomorTelepon(phone);

        var gender = $('#gender').val();
        if (gender == '' || gender == ' ' || gender == undefined || (gender != 'L' && gender != 'P')) {
            alert('Jenis Kelamin tidak valid');
            $('#gender').focus();
            return;
        }

        var description = $('#description').val();
        if (description == undefined) {
            description = '';
        }

        var active = $("#active").is(':checked') ? 1 : 0;
        if (active == undefined) {
            active = 1;
        }

        $.ajax({
            url: route,
            type: 'POST',
            headers: {'X-CSRF-TOKEN': token},
            dataType: 'json',
            data: {
                fullname : fullname,
                username : username,
                email: email,
                role : role,
                password : password,
                address : address,
                gsm : gsm,
                phone : phone,
                gender : gender,
                description : description,
                active : active,
                _token: token
            },
            error: function (res) {
                var errors = res.responseJSON;
                var pesan = '';
                $.each(errors, function (index, value) {
                    pesan += value + "\n";
                });

                alert(pesan);
            },
            success: function () {
                alert('Sukses Menyimpan Data');

                $('#username').val(null);
                $('#email').val(null);
                $('#role').select2('val', ' ');
                $('#fullname').val(null);
                $('#password').val(null);
                $('#validate_password').val(null);
                $('#address').val(null);
                $('#gsm').val(null);
                $('#phone').val(null);
                $('#gender').select2('val', ' ');
                $('#description').val(null);
                $("#active").attr('checked', true);


                $('#username').focus();
            }
        });
    });
});