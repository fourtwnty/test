/**
 * Created by ocol on 10/11/16.
 */
$(document).ajaxStart(function() {
    Pace.restart();
});

$(document).ready(function () {
    document.getElementById("email").maxLength = 100;
    document.getElementById("fullname").maxLength = 200;
    document.getElementById("username").maxLength = 50;
    document.getElementById("password").maxLength = 50;
    document.getElementById("validate_password").maxLength = 50;
    document.getElementById("address").maxLength = 200;
    document.getElementById("gsm").maxLength = 20;
    document.getElementById("phone").maxLength = 20;
    document.getElementById("description").maxLength = 200;

    var route = "/get_select_group";
    var inputTipe = $('#role');

    var list = document.getElementById("role");
    while (list.hasChildNodes()) {
        list.removeChild(list.firstChild);
    }
    inputTipe.append('<option value=" ">Pilih Grup Pengguna</option>');

    $.get(route, function (res) {
        $.each(res.data, function (index, value) {
            inputTipe.append('<option value="' + value[0] + '">' + value[1] + '</option>');
        });
    });


    var route = "/cekhakakses/ubah_user";
    var bolehUbah;
    $.get(route, function (res) {
        bolehUbah = res;
    });

    var route = "/cekhakakses/hapus_user";
    var bolehHapus;
    $.get(route, function (res) {
        bolehHapus = res;
    });

    $("#role").select2();
    $("#gender").select2();


    $('#dataTableBuilder').DataTable({
        //scrollX: true,
        //scrollColapse:true,
        responsive: true,
        'ajax': {
            'url': '/penggunas',
        },
        'columnDefs': [{
            'targets': 6,
            'searchable': false,
            "orderable": false,
            "orderData": false,
            "orderDataType": false,
            "orderSequence": false,
            "sClass": "text-center col-lg-1 td-aksi",
            'render': function (data, type, full, meta) {
                var kembali = '';
                if (bolehUbah == true) {
                    kembali += '<button title="Ubah Data" class="btn btn-warning btn-flat" data-toggle="modal" data-target="#modalUbah" onclick="UbahClick(this);"><i class="fa fa-pencil-square-o fa-fw"></i> </button>';
                }
                if (bolehHapus == true) {
                    kembali += '<button title="Hapus Data" class="btn btn-danger btn-flat" data-toggle="modal" data-target="#modalHapus" onclick="HapusClick(this);"><i class="fa fa-trash fa-fw"></i> </button>';
                }

                kembali += '<button title="Lihat Data" class="btn btn-info btn-flat" data-toggle="modal" data-target="#modalLihat" onclick="LihatClick(this);"><i class="fa fa-search fa-fw"></i> </button>';


                return kembali;

            }
        },{
            'targets':0,
            'sClass': "col-lg-1"
        }, {
            'targets':1,
            'sClass': "col-lg-3"
        },{
            'targets':2,
            'sClass': "col-lg-2"
        },{
            'targets':3,
            'sClass': "col-lg-2"
        }, {
            'targets':4,
            'sClass': "col-lg-2"
        }, {
            'targets':5,
            'sClass': "col-lg-1"
        }],
        'rowCallback': function (row, data, dataIndex) {
            if (bolehUbah == true) {
                $(row).find('button[class="btn btn-warning btn-flat"]').prop('value', data[6]);
            }
            if (bolehHapus == true) {
                $(row).find('button[class="btn btn-danger btn-flat"]').prop('value', data[6]);
            }

            $(row).find('button[class="btn btn-info btn-flat"]').prop('value', data[6]);

        }
    });

    $('#ubahkatasandi').click(function () {
        var ubahkatasandi = $('#ubahkatasandi').is(':checked') ? 1 : 0;
        if (ubahkatasandi == 1) {
            $('#divSandi').removeClass('hidden');
            $('#divValidasiSandi').removeClass('hidden');
        } else {
            $('#divSandi').addClass('hidden');
            $('#divValidasiSandi').addClass('hidden');
        }
    });
});

function reloadTable() {
    var table = $('#dataTableBuilder').dataTable();
    table.cleanData;
    table.api().ajax.reload();
}

function LihatClick(btn) {
    route = "/pengguna/" + btn.value;

    $.get(route, function (res) {
        $('#username-lihat').val(res.username);
        $('#email-lihat').val(res.email);
        $('#role-lihat').val(res.role);
        $('#fullname-lihat').val(res.fullname);
        $('#address-lihat').val(res.address);
        $('#gsm-lihat').val(res.gsm);
        $('#phone-lihat').val(res.phone);
        $('#gender-lihat').val(res.gender);
        $('#description-lihat').val(res.description);
        $("#active-lihat").val(res.active);

    });

}

function UbahClick(btn) {
    route = "/pengguna/" + btn.value + "/edit";

    $.get(route, function (res) {
        $('#id').val(res.id);
        $('#username').val(res.username);
        $('#email').val(res.email);

        $('#role').select2('val', '' + res.role);

        //$('#role').val(res.role);
        $('#fullname').val(res.fullname);
        $('#address').val(res.address);
        $('#gsm').val(res.gsm);
        $('#phone').val(res.phone);
        //$('#gender').val(res.gender);

        $('#gender').select2('val', '' + res.gender);
        $('#description').val(res.description);

        $("#active").prop('checked', (res.active == '0' ? false : true));

        var ubahkatasandi = $('#ubahkatasandi').is(':checked') ? 1 : 0;
        if (ubahkatasandi == 1) {
            $('#divSandi').addClass('hidden');
            $('#divValidasiSandi').addClass('hidden');

            $("#ubahkatasandi").prop('checked', false);
        }

        $('#username').focus();

    });

}

$('#simpan').click(function () {
    var id = $('#id').val();
    var token = $('#token').val();
    var route = "/pengguna/" + id;

    var username = $('#username').val();
    if (username == '' || username == undefined) {
        alert('Nama Pengguna tidak boleh dikosongkan');
        $('#username').focus();
        return;
    }

    var email = $('#email').val();
    if (email == '' || email == undefined) {
        alert('Alamat Email tidak boleh dikosongkan');
        $('#email').focus();
        return;
    }

    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    if(re.test(email) == false) {
        alert('Format penulisan E-mail tidak benar!');
        $('#email').focus();
        return;
    }

    var role = $('#role').val();
    if (role == '' || role == ' ' || role == undefined) {
        alert('Pilih Grup Pengguna dengan benar!');
        $('#role').focus();
        return;
    }

    var fullname = $('#fullname').val();
    if (fullname == '' || fullname == undefined) {
        alert('Nama Lengkap tidak boleh dikosongkan');
        $('#fullname').focus();
        return;
    }

    var password = '';
    var validate_password = '';

    var ubahkatasandi = $('#ubahkatasandi').is(':checked') ? 1 : 0;
    if (ubahkatasandi == 1) {
        password = $('#password').val();
        if (password == '' || password == undefined) {
            alert('Kata Sandi tidak boleh kosong');
            $('#password').focus();
            return;
        }

        validate_password = $('#validate_password').val();
        if (validate_password == '' || validate_password == undefined) {
            alert('Silahkan melakukan validasi kata sandi!!!');
            $('#validate_password').focus();
            return;
        }

        if (validate_password != password) {
            alert('Validasi Kata Sandi tidak cocok !!!');
            $('#validate_password').val('');
            $('#validate_password').focus();
            return;
        }
    }

    var address = $('#address').val();
    if (address == '' || address == undefined) {
        alert('Alamat belum diinputkan');
        $('#address').focus();
        return;
    }

    var gsm = $('#gsm').val();
    if (gsm == undefined) {
        gsm = '';
    }

    gsm = formatNomorTelepon(gsm);

    var phone = $('#phone').val();
    if (phone == undefined) {
        phone = '';
    }

    phone = formatNomorTelepon(phone);

    var gender = $('#gender').val();
    if (gender == '' || gender == ' ' || gender == undefined || (gender != 'L' && gender != 'P')) {
        alert('Jenis Kelamin tidak valid');
        $('#gender').focus();
        return;
    }

    var description = $('#description').val();
    if (description == undefined) {
        description = '';
    }

    var active = $("#active").is(':checked') ? 1 : 0;
    if (active == undefined) {
        active = 1;
    }

    $.ajax({
        url: route,
        headers: {'X-CSRF-TOKEN': token},
        type: 'PUT',
        dataType: 'json',
        data: {
            fullname : fullname,
            username : username,
            email : email,
            role : role,
            ubahkatasandi : ubahkatasandi,
            password : password,
            address : address,
            gsm : gsm,
            phone : phone,
            gender : gender,
            description : description,
            active : active,
            _token: token
        },
        error: function (res) {
            var errors = res.responseJSON;
            var pesan = '';
            $.each(errors, function (index, value) {
                pesan += value + "\n";
            });

            alert(pesan);
        },
        success: function () {
            reloadTable();
            alert('Sukses Mengubah Data');
            $('#modalUbah').modal('toggle');
        }
    });
});

function HapusClick(btn) {
    $('#idHapus').val(btn.value);
}

$('#yakinhapus').click(function () {
    var token = $('#token').val();
    var id = $('#idHapus').val();
    var route = "/pengguna/" + id;

    $.ajax({
        url: route,
        headers: {'X-CSRF-TOKEN': token},
        type: 'DELETE',
        dataType: 'json',
        error: function (res) {
            var errors = res.responseJSON;
            var pesan = '';
            $.each(errors, function (index, value) {
                pesan += value + "\n";
            });

            alert(pesan);
        },
        success: function () {
            reloadTable();
            alert('Sukses Menghapus Data');
            $('#modalHapus').modal('toggle');
        }
    });
});