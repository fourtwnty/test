<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class IuranDetail extends Model
{
    protected $table = 'iuran_detail';
    public $namabulantahun = null;

    protected $fillable = ['iuran_id','tahun_id', 'bulan_id', 'harga'];

    public function iuran() {
        return Iuran::where('id', $this->iuran_id)->first();
    }

    public function loadTambahan() {
        if ($this->namabulantahun === null) {
            $bulan = $this->bulan();
            $tahun = $this->tahun();
            $this->namabulantahun = $this->getNamaBulanDanTahun($bulan->id, $tahun);
        }
    }

    public function bulan () {
        return Bulan::where('id', $this->bulan_id)->first();
    }

    public function tahun() {
        return TahunAjaran::where('id', $this->tahun_id)->first();
    }

    protected function getNamaBulanDanTahun ($bulan_id, $tahun) {
        $bulan = Bulan::where('id', $bulan_id)->first();

        $arr_tahun = explode ("/", $tahun->name, 2);

        if ($bulan->id > 6){
            $namaTahun = $arr_tahun[1];
        } else {
            $namaTahun = $arr_tahun[0];
        }

        return  $bulan->name.' '.$namaTahun;
    }
}
