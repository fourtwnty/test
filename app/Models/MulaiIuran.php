<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MulaiIuran extends Model
{
    public $nisn_siswa = null;
    public $nama_siswa = null;
    public $nama_mulaiiuran = null;

    protected $table = 'mulai_iuran';

    protected $fillable = ['siswa_id', 'tahun_id', 'bulan_id'];

    public function siswa()
    {
        return $this->belongsTo('App\Models\Siswa');
    }

    public function loadTambahan() {
        if (($this->nisn_siswa === null) || ($this->nama_siswa === null)) {
            $siswa = $this->siswa()->first();

            $this->nisn_siswa = $siswa->nisn;
            $this->nama_siswa = $siswa->nama;
        }

        if ($this->nama_mulaiiuran === null) {
            $bulan = $this->bulan();
            $tahun = $this->tahunajaran();

            $arr_tahun = explode ("/", $tahun->name, 2);

            $namaTahun = '';

            if ($bulan->id > 6){
                $namaTahun = $arr_tahun[1];
            } else {
                $namaTahun = $arr_tahun[0];
            }

            $this->nama_mulaiiuran = $bulan->name.' '.$namaTahun;
        }
    }

    public function bulan() {
        return Bulan::where('id', $this->bulan_id)->first();
    }

    public function tahunajaran() {
        return TahunAjaran::where('id', $this->tahun_id)->first();
    }
}
