<?php

namespace App\Models;

use Exception;

trait SiswaAccessData
{
    public $banyakmulai = null;
    public $banyakiuran = null;
    public $kelas_name = null;
    public $harga_iuran = null;

    protected function hargaiuran() {
        $kelas = Kelas::where('id', $this->kelas_id)->first();
        $program = Program::where('id', $kelas->program_id)->first();
        $tarif = Tarif::where('program_id', $program->id)->first();

        $this->harga_iuran = $tarif->nilai;
    }

    protected function getNamaBulanDanTahun ($bulan_id, $tahun) {
        try {
            $bulan = Bulan::where('id', $bulan_id)->first();

            $arr_tahun = explode ("/", $tahun->name, 2);

            if ($bulan->id > 6){
                $namaTahun = $arr_tahun[1];
            } else {
                $namaTahun = $arr_tahun[0];
            }

        } catch (Exception $ex) {
            dd($ex->getMessage() . ' '. $bulan_id.', t: '.$tahun->name);
        }

        return  $bulan->name.' '.$namaTahun;
    }

    protected function getTahunSelanjutnya($tahun) {
        $hasil = TahunAjaran::where('id', '>', $tahun->id)->first();

        if ($hasil == null) {
            dd('null data   <br/>'.$tahun);
        }

        return $hasil;
    }

    protected $arraybulan = [
        '01' => 'Januari',
        '02' => 'Februari',
        '03' => 'Maret',
        '04' => 'April',
        '05' => 'Mei',
        '06' => 'Juni',
        '07' => 'Juli',
        '08' => 'Agustus',
        '09' => 'September',
        '10' => 'Oktober',
        '11' => 'November',
        '12' => 'Desember',
    ];

    protected function getNamaBulanSekarang() {
//        dd(date('m').', '.date('Y'));

        return $this->arraybulan[date('m')].' '.date('Y');
    }

    protected function tunggakanLengkaphitung($bulan_id, $tahun_id) {
        if ($bulan_id != null && $tahun_id != null) {
            $tahun = TahunAjaran::where('id', $tahun_id)->first();

            if ($this->harga_iuran === null) {
                $this->hargaiuran();
            }

            $selesai = false;
            $datakembali = [];

            while($selesai === false) {
                if ($bulan_id > 12) {
                    $tahun = $this->getTahunSelanjutnya($tahun);
                    $bulan_id = 1;
                }
                $namaBulan = $this->getNamaBulanDanTahun($bulan_id, $tahun);

                if ($namaBulan == $this->getNamaBulanSekarang()) {
                    $selesai = true;
                }

                if ($selesai === true) {
                    break;
                }

//                $datakembali[] = ['bulan'=>$namaBulan, 'harga'=>$this->harga_iuran];
                $datakembali[] = ['bulan'=>$namaBulan, 'harga'=>$this->harga_iuran, 'bulan_id' => $bulan_id, 'tahun' => $tahun];

                $bulan_id++;
            }

            return $datakembali;
        }
        return null;
    }

    protected function tunggakanhitung($bulan_id = null, $tahun_id = null) {
        if ($bulan_id != null && $tahun_id != null) {

            $tahun = TahunAjaran::where('id', $tahun_id)->first();

            if ($this->harga_iuran === null) {
                $this->hargaiuran();
            }

            $selesai = false;
            $datakembali = [];

            while($selesai === false) {
                if ($bulan_id > 12) {
                    $tahun = $this->getTahunSelanjutnya($tahun);
                    $bulan_id = 1;
                }

                $namaBulan = $this->getNamaBulanDanTahun($bulan_id, $tahun);

                if ($namaBulan == $this->getNamaBulanSekarang()) {
                    $datakembali[] = ['bulan'=>$namaBulan, 'harga'=>$this->harga_iuran];
                    $selesai = true;
                }

                if ($selesai === true) {
                    break;
                }

                $datakembali[] = ['bulan'=>$namaBulan, 'harga'=>$this->harga_iuran];

                $bulan_id++;
            }

            return $datakembali;
        }
        return null;
    }

    protected function tunggakanhitungdarimulai($mulaiIuran) {

        if (isset($mulaiIuran) && $mulaiIuran !== null) {
            $mulaibulan = $mulaiIuran->bulan_id;

            $mulaiTahun = TahunAjaran::where('id', $mulaiIuran->tahun_id)->first();

            if ($mulaiTahun == null || $mulaibulan == null) {
                return null;
            }

            if ($this->harga_iuran === null) {
                $this->hargaiuran();
            }

            $selesai = false;
            $datakembali = [];

            while($selesai === false) {
                if ($mulaibulan > 12) {
                    $mulaiTahun = $this->getTahunSelanjutnya($mulaiTahun);
                    $mulaibulan = 1;
                }

                $namaBulan = $this->getNamaBulanDanTahun($mulaibulan, $mulaiTahun);


                if ($namaBulan == $this->getNamaBulanSekarang()) {
                    $datakembali[] = ['bulan'=>$namaBulan, 'harga'=>$this->harga_iuran];
                    $selesai = true;
                }

//                dd('disini coba');

                if ($selesai === true) {
                    break;
                }

                $datakembali[] = ['bulan'=>$namaBulan, 'harga'=>$this->harga_iuran];

                $mulaibulan++;
            }



            return $datakembali;
        }
        return null;
    }


    protected function tunggakanLengkaphitungdarimulai($mulaiIuran) {

        if (isset($mulaiIuran) && $mulaiIuran !== null) {
            $mulaibulan = $mulaiIuran->bulan_id;
            $mulaiTahun = TahunAjaran::where('id', $mulaiIuran->tahun_id)->first();

            if ($mulaiTahun == null || $mulaibulan == null) {
                return null;
            }

            if ($this->harga_iuran === null) {
                $this->hargaiuran();
            }

            $selesai = false;
            $datakembali = [];

            while($selesai === false) {
                if ($mulaibulan > 12) {
                    $mulaiTahun = $this->getTahunSelanjutnya($mulaiTahun);
                    $mulaibulan = 1;
                }
                $namaBulan = $this->getNamaBulanDanTahun($mulaibulan, $mulaiTahun);

                if ($namaBulan == $this->getNamaBulanSekarang()) {
                    $datakembali[] = ['bulan'=>$namaBulan, 'harga'=>$this->harga_iuran, 'bulan_id' => $mulaibulan, 'tahun' => $mulaiTahun];
                    $selesai = true;
                }

                if ($selesai === true) {
                    break;
                }

                $datakembali[] = ['bulan'=>$namaBulan, 'harga'=>$this->harga_iuran, 'bulan_id' => $mulaibulan, 'tahun' => $mulaiTahun];

                $mulaibulan++;
            }

            return $datakembali;
        }
        return null;
    }

    public function mulaiiuran()
    {
        return $this->hasOne('App\Models\MulaiIuran');
    }

    /**
     * Return array of iuran
     * @return mixed
     */
    public function iurans() {
        return Iuran::where('siswa_id', $this->id)->get();
    }

    public function lastiuran() {
        return Iuran::latest('id')->where('siswa_id', $this->id)->first();
        //return Iuran::where('siswa_id', $this->id)->last();
    }

    public function lastdetailiuran($iuran) {
        if (isset($iuran->id)) {
            return IuranDetail::latest('id')->where('iuran_id', $iuran->id)->first();
        }
        return null;
    }

    public function kelas() {
        $kelas = Kelas::where('id', $this->kelas_id)->first();
        $kelas->loadTambahan();

        return $kelas;
    }
}
