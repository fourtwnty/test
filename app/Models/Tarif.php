<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tarif extends Model
{
    public $program_name = null;

    protected $table = 'tarif';

    protected $fillable = [ 'program_id','nilai'];

    public function program() {
        return Program::where('id', $this->program_id)->first();
    }

    public function loadTambahan() {
        if ($this->program_name === null) {
            $program = $this->program();
            $this->program_name = $program->name;
        }
    }
}
