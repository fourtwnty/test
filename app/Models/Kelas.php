<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Kelas extends Model
{
    public $program_name = null;
    public $banyaksiswa = 0;

    protected $table = 'kelas';

    protected $fillable = ['tingkatan', 'program_id'];

    public function siswa() {
        return DB::table('siswa')->where('kelas_id', $this->id)->get();
    }

    public function program() {
        return Program::where('id', $this->program_id)->first();
    }

    public function loadTambahan() {
        $this->banyaksiswa = count($this->siswa());

        $program = $this->program();
        $this->program_name = $program->name;
    }
}
