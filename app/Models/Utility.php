<?php

/**
 * Created by PhpStorm.
 * User: ocol
 * Date: 02/09/16
 * Time: 17:29
 */

namespace App\Models;

class Utility
{
    public static function getBulanSaatIni()
    {
        $arrbulan = [
            1 => 'Januari',
            2 => 'Februari',
            3 => 'Maret',
            4 => 'April',
            5 => 'Mei',
            6 => 'Juni',
            7 => 'Juli',
            8 => 'Agustus',
            9 => 'September',
            10 => 'Oktober',
            11 => 'November',
            12 => 'Desember',
        ];

        return $arrbulan[date('m')] . ' ' . date('Y');
    }

    public static function sisipkanNolKodeOtomatis7($nilai)
    {
        $panjang = strlen($nilai);

        if ($panjang == 1) {
            return '00000' . $nilai;
        } else if ($panjang == 2) {
            return '0000' . $nilai;
        } else if ($panjang == 3) {
            return '000' . $nilai;
        } else if ($panjang == 4) {
            return '00' . $nilai;
        } else if ($panjang == 5) {
            return '0' . $nilai;
        } else if ($panjang == 6) {
            return $nilai;
        } else {
            return "";
        }
    }

    public static function getRealIP()
    {
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) //CHEK IP YANG DISHARE DARI INTERNET
        {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) //UNTUK CEK IP DARI PROXY
        {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }

    public static function getTahunAjaranSekarang()
    {
        if (date('m') > 6) {
            $tahun1 = date('Y');
            $tahun2 = $tahun1 + 1;

            return $tahun1 . '/' . $tahun2;
        } else {
            $tahun2 = date('Y');
            $tahun1 = $tahun2 - 1;

            return $tahun1 . '/' . $tahun2;
        }
    }
}