<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TahunAjaran extends Model
{
    public $banyakmulaiiuran = null;
    public $banyakiurandetail = null;

    protected $table = 'tahun_ajaran';

    protected $fillable = ['name'];

    public function loadTambahan() {
        if ($this->banyakmulaiiuran === null) {
            $this->banyakmulaiiuran = count($this->mulaiiuran());
        }

        if ($this->banyakiurandetail === null) {
            $this->banyakiurandetail = count($this->iurandetail());
        }
    }

    public function mulaiiuran () {
        return MulaiIuran::where('tahun_id', $this->id)->get();
    }

    public function iurandetail () {
        return IuranDetail::where('tahun_id', $this->id)->get();
    }
}
