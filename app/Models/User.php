<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Zizaco\Entrust\Traits\EntrustUserTrait;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements
    AuthenticatableContract,
    AuthorizableContract,
    CanResetPasswordContract
{
    use Authenticatable, Notifiable, CanResetPassword,Authorizable, EntrustUserTrait {
        EntrustUserTrait::can insteadof Authorizable;
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'fullname', 'email', 'username', 'password',
        'address', 'gsm', 'phone', 'gender', 'description', 'active'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public $role_name = null;


    public function permissions() {
        $role_user = AssignedRole::where('user_id', $this->id)->first();
        $role = Role::where('id', $role_user->role_id)->first();

        return $role->perms;
    }

    public function role() {
        $role_user = AssignedRole::where('user_id', $this->id)->first();
        $role = Role::where('id', $role_user->role_id)->first();

        return $role;
    }

    public function loadTambahan() {
        if ($this->role_name == null) {
            $this->role_name  = $this->role()->display_name;
        }
    }
}
