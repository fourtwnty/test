<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Identitas extends Model
{
    protected $table = 'identitas';

    protected $fillable = ['nama', 'alamat', 'telp', 'fax',
        'logo', 'dinas', 'kota', 'kelurahan',
        'kecamatan', 'kabupaten',
    ];
}
