<?php

namespace App\Models;

use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Siswa extends Model
{
    use SiswaAccessData;

    protected $table = 'siswa';

    protected $fillable = ['nisn', 'nama', 'jk', 'tempat_lahir', 'tgl_lahir', 'alamat', 'aktif', 'kelas_id'];

    protected $dates = array('tgl_lahir');

    public function tunggakan() {
        if ($this->aktif == 1) {
            $iuran = $this->lastiuran();


            if ($iuran == null) {
                $mulaiIuran = $this->mulaiiuran()->first();
                if ($mulaiIuran === null) {
                    return null;
                }

                return $this->tunggakanhitungdarimulai($mulaiIuran);
            }

            $lastdetailIuran = $this->lastdetailiuran($iuran);

//            if ($this->id == 2) dd($lastdetailIuran);

            if ($lastdetailIuran === null) {
                return null;
            }

            $bulanterakhirid = $lastdetailIuran->bulan_id;
            $tahunterakhirid = $lastdetailIuran->tahun_id;



                if ($this->cekBulanIdTahunId($bulanterakhirid, $tahunterakhirid) == false) {
                    return null;
                } else {
                    return $this->tunggakanhitung($bulanterakhirid + 1, $tahunterakhirid);
                }

        }

        return null;

    }

    protected function cekBulanIdTahunId ($bulan_id, $tahunid) {
        try {

            $bulan = Bulan::where('id', $bulan_id)->first();
            $tahun = TahunAjaran::where('id', $tahunid)->first();

//            dd($bulan->name.' '.$tahun->name);

            $arr_tahun = explode ("/", $tahun->name, 2);

            if ($bulan->id > 6){
                $namaTahun = $arr_tahun[1];
            } else {
                $namaTahun = $arr_tahun[0];
            }

            $namaBulan = $bulan->name;

            if ($namaBulan.' '.$namaTahun == $this->arraybulan[date('m')].' '.date('Y')) {
                $iurandetail = DB::table('iuran_detail')
                    ->join('iuran', 'iuran.id', '=', 'iuran_detail.iuran_id')
                    ->join('siswa', 'siswa.id', '=', 'iuran.siswa_id')
                    ->join('tahun_ajaran', 'tahun_ajaran.id', '=', 'iuran_detail.tahun_id')
                    ->join('bulan', 'bulan.id', '=', 'iuran_detail.bulan_id')
                    ->where('siswa.id', $this->id)
                    ->where('tahun_ajaran.id', $tahunid)
                    ->where('bulan.id', $bulan_id)
                    ->select('iuran_detail.*')
                    ->get();
//                dd($iurandetail->isEmpty());

                if (!$iurandetail->isEmpty()) {
                    return false;
                }
            }

//            dd($bulan->name.' '. $namaTahun);

        } catch (Exception $ex) {
            return false;
        }

//        return  $bulan->name.' '.$namaTahun;
        return true;
    }

    public function tunggakanlengkap() {
        if ($this->aktif == 1) {
            $iuran = $this->lastiuran();

            if ($iuran == null) {
                $mulaiIuran = $this->mulaiiuran()->first();
                if ($mulaiIuran === null) {
                    return null;
                }

                return $this->tunggakanLengkaphitungdarimulai($mulaiIuran);
            }

            $lastdetailIuran = $this->lastdetailiuran($iuran);

            if ($lastdetailIuran === null) {
                return null;
            }

            $bulanterakhirid = $lastdetailIuran->bulan_id;
            $tahunterakhirid = $lastdetailIuran->tahun_id;

            return $this->tunggakanLengkaphitung($bulanterakhirid+1, $tahunterakhirid);
        }
        return null;

    }

    public function loadTambahan() {
        if ($this->banyakmulai === null) {
            $mulaiIuran = $this->mulaiiuran()->first();
            if ($mulaiIuran === null) {
                $this->banyakmulai = 0;
            } else {
                $this->banyakmulai = 1;
            }
        }

        if ($this->kelas_name === null) {
            $kelas = $this->kelas();

            $this->kelas_name = $kelas->program_name.' '.$kelas->tingkatan;
        }

        if ($this->banyakiuran == null) {
            $this->banyakiuran = count($this->iurans());
        }
    }

    public function isMenunggakBulanIni() {
        if ($this->aktif == 1) {
            $iuran = $this->lastiuran();


            if ($iuran == null) {
                $mulaiIuran = $this->mulaiiuran()->first();
                if ($mulaiIuran === null) {
                    return false;
                }

                return $this->cektunggakandarimulai($mulaiIuran);
            }

            $lastdetailIuran = $this->lastdetailiuran($iuran);

            if ($lastdetailIuran === null) {
                return false;
            }

            $bulanterakhirid = $lastdetailIuran->bulan_id;
            $tahunterakhirid = $lastdetailIuran->tahun_id;



            if ($this->cekBulanIdTahunId($bulanterakhirid, $tahunterakhirid) == false) {
                return false;
            } else {
                return $this->cekTunggakanHitung($bulanterakhirid + 1, $tahunterakhirid);
            }

        }

        return false;
    }

    protected function cektunggakandarimulai($mulaiIuran) {
        if (isset($mulaiIuran) && $mulaiIuran !== null) {
            $mulaibulan = $mulaiIuran->bulan_id;

            $mulaiTahun = TahunAjaran::where('id', $mulaiIuran->tahun_id)->first();

            if ($mulaiTahun == null || $mulaibulan == null) {
                return null;
            }

            $selesai = false;
            $datakembali = false;

            while($selesai === false) {
                if ($mulaibulan > 12) {
                    $mulaiTahun = $this->getTahunSelanjutnya($mulaiTahun);
                    $mulaibulan = 1;
                }

                $namaBulan = $this->getNamaBulanDanTahun($mulaibulan, $mulaiTahun);


                if ($namaBulan == $this->getNamaBulanSekarang()) {
                    $datakembali = true;
                    $selesai = true;
                }

                if ($selesai === true) {
                    break;
                }

                $mulaibulan++;
            }

            return $datakembali;
        }
        return false;
    }

    protected function cekTunggakanHitung($bulan_id = null, $tahun_id = null) {
        if ($bulan_id != null && $tahun_id != null) {

            $tahun = TahunAjaran::where('id', $tahun_id)->first();

            if ($this->harga_iuran === null) {
                $this->hargaiuran();
            }

            $selesai = false;
            $datakembali = false;

            while($selesai === false) {
                if ($bulan_id > 12) {
                    $tahun = $this->getTahunSelanjutnya($tahun);
                    $bulan_id = 1;
                }

                $namaBulan = $this->getNamaBulanDanTahun($bulan_id, $tahun);

                if ($namaBulan == $this->getNamaBulanSekarang()) {
                    $datakembali = true;
                    $selesai = true;
                }

                if ($selesai === true) {
                    break;
                }

                $bulan_id++;
            }

            return $datakembali;
        }
        return false;
    }
}
