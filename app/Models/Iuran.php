<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Iuran extends Model
{
    protected $primaryKey = 'id'; // or null

    public $incrementing = false;

    public $banyak_detail = null;
    public $name_siswa = null;
    public $nisn_siswa = null;
    public $string_keterangan=null;

    protected $table = 'iuran';

    protected $fillable = ['id', 'tgl','siswa_id', 'total'];

    public function siswa() {
        return Siswa::where('id', $this->siswa_id)->first();
    }

    public function iurandetail() {
        return IuranDetail::where('iuran_id', $this->id)->get();
    }

    public function loadTambahan() {
        if ($this->banyak_detail === null) {
            $this->banyak_detail = count($this->iurandetail());
        }

        if ($this->nisn_siswa === null) {
            $siswa = $this->siswa();
            $this->name_siswa = $siswa->nama;
            $this->nisn_siswa = $siswa->nisn;
        }

        if ($this->string_keterangan === null) {
            $iurandetails = $this->iurandetail();

            foreach ($iurandetails as $iud) {
                $iud->loadTambahan();

                if ($this->string_keterangan === null) {
                    $this->string_keterangan = $iud->namabulantahun;
                } else {
                    $this->string_keterangan .= ', '.$iud->namabulantahun;
                }

            }

//            dd($this->string_keterangan);
        }
    }

    public function getStringKeteranganPerTahun ($tahunajaranid) {
        $kembali = '';
        $iurandetails = $this->iurandetail();
        foreach($iurandetails as $iud) {
            if ($iud->tahun_id == $tahunajaranid) {
                $iud->loadTambahan();

                if ($kembali === '') {
                    $kembali = $iud->namabulantahun;
                } else {
                    $kembali .= ', '.$iud->namabulantahun;
                }
            }
        }

        return $kembali;
    }

    public function getTotalPerTahun ($tahunajaranid) {
        $kembali = 0;
        $iurandetails = $this->iurandetail();
        foreach($iurandetails as $iud) {
            if ($iud->tahun_id == $tahunajaranid) {
                $kembali += $iud->harga;
            }
        }

        return $kembali;
    }
}
