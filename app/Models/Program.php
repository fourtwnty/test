<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Program extends Model
{
    public $banyakkelas = null;
    public $banyaktarif = null;

    protected $table = 'programs';

    protected $fillable = ['name'];

    public function loadTambahan() {
        if ($this->banyakkelas === null) {
            $this->banyakkelas = count($this->kelas());
        }
        if ($this->banyaktarif === null) {
            $this->banyaktarif = count($this->tarif());
        }
    }

    public function kelas() {
        return DB::table('kelas')->where('program_id', $this->id)->get();
    }
    public function tarif() {
        return DB::table('tarif')->where('program_id', $this->id)->get();
    }
}
