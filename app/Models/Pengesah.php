<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pengesah extends Model
{
    protected $table = 'pengesah';

    protected $fillable = ['name', 'display_name', 'nilai', 'nip'];
}
