<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Penunggak extends Model
{
    public $nisn_siswa;
    public $name_siswa;
    public $total;
    public $string_keterangan;
    public $string_tunggakan_bulan;

    public function __construct(Siswa $siswa) {
        $this->nisn_siswa = $siswa->nisn;
        $this->name_siswa = $siswa->nama;

        $tunggakan = $siswa->tunggakan();

//        if ($siswa->nisn == '1234567891') {
//            dd($tunggakan != null);
//        }

        $this->total = 0;
        $this->string_keterangan = '';
        $this->string_tunggakan_bulan = '';

        if ($tunggakan !== null) {
            foreach ($tunggakan as $t) {

                $this->total += $t['harga'];

                if ($this->string_keterangan === '') {
                    $this->string_keterangan = $t['bulan'];
                } else {
                    $this->string_keterangan .= ', '.$t['bulan'];
                }

                $arr = explode (" ", $t['bulan'], 2);

                if ($this->string_tunggakan_bulan === '') {
                    $this->string_tunggakan_bulan = $arr[0];
                } else {
                    $this->string_tunggakan_bulan .= ', '.$arr[0];
                }
            }
        }

//        dd($this->string_keterangan.'  |  ' . $this->name_siswa.' | '.$this->total.' | '.$this->nisn_siswa.' | '.$this->string_tunggakan_bulan);
    }
}
