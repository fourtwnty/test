<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Controllers\HomeController;
use App\Http\Requests\CreateTahunRequest;
use App\Http\Requests\UpdateTahunRequest;
use App\Models\TahunAjaran;
use Exception;

use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;

class TahunController extends HomeController
{
    public function __construct() {
        parent::__construct();
    }

    public function index() {
        return view('page/pengaturan/tahun/index');
    }

    public function tahuns() {
        $tahun = TahunAjaran::all();
        $cacah = 0;
        $data = [];

        foreach ($tahun as $d) {
            $data[$cacah] = [$d->name, $d->id];
            $cacah++;

        }

        return response()->json([
            'data' => $data
        ]);
    }

    public function create() {
        return view('page/pengaturan/tahun/create');
    }

    public function store(CreateTahunRequest $request)
    {
//        dd($request->all());

        if ($request->ajax()) {
            $input = $request->all();

            if (!isset($input['_token'])) {
                return response()->json([
                    'data' => $input->toArray()
                ]);
            } else {
                $hasil = $this->simpanTransaksiCreate($input);
                if ($hasil == '') {
                    return response()->json([
                        'data' => 'Sukses Menyimpan'
                    ]);
                } else {
                    return response()->json([
                        'data' => ['Gagal menyimpan data tahun ajaran! Periksa data anda dan pastikan server MySQL anda sedang aktif! Err: ' + $hasil]
                    ], 422);
                }

            }
        }
    }

    /**
     * @param $input
     * @return string
     */
    protected function simpanTransaksiCreate($input) {
        DB::beginTransaction();

        try {
            $tahun = new TahunAjaran();
            $tahun->name = $input['name'];
            $tahun->save();
        } catch (ValidationException $ex) {
            DB::rollback();
            return $ex->getMessage();;
        } catch (Exception $ex) {
            DB::rollback();
            return $ex->getMessage();;
        }

        DB::commit();

        return '';
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tahun = TahunAjaran::find($id);

        $name = explode('/', $tahun->name);


        return response()->json([
            'id'=>$tahun->id,
            'awal' => $name[0],
            'akhir' => $name[1],
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateTahunRequest $request, $id)
    {
//        dd($request->all());
        if ($request->ajax()) {
            $input = $request->all();

            if (!isset($input['_token'])) {
                return response()->json([
                    'data' => $input->toArray()
                ]);
            } else {
                $tahun = TahunAjaran::find($id);

                $tahunCari = TahunAjaran::where('name', $input['name'])->first();
                if ($tahunCari != null) {
                    if ($tahun->id != $tahunCari->id) {
                        return response()->json([
                            'data' => ['Nama Tahun Ajaran sudah digunakan oleh data lainnya!']
                        ], 422);
                    }
                }

                if ($tahun != null) {
                    $hasil = $this->simpanTransaksiUpdate($input, $tahun);
                    if ($hasil == '') {
                        return response()->json([
                            'data' => 'Sukses Mengubah Data'
                        ]);
                    } else {
                        return response()->json([
                            'data' => ['Gagal mengubah data tahun ajaran! Periksa data anda dan pastikan server MySQL anda sedang aktif! Err: ' + $hasil]
                        ], 422);
                    }
                } else {
                    return response()->json([
                        'data' => ['Gagal mengubah data tahun ajaran! Data tidak ditemukan']
                    ], 422);
                }
            }
        }
    }

    protected function simpanTransaksiUpdate($input, $tahun) {
        DB::beginTransaction();

        try {
            DB::table('tahun_ajaran')
                ->where('id', $tahun->id)
                ->update(
                    [
                        'name' => $input['name'],
                        'updated_at' => date('Y/m/d H:i:s')
                    ]);
        } catch (ValidationException $ex) {
            DB::rollback();
            return $ex->getMessage();
        } catch (Exception $ex) {
            DB::rollback();
            return $ex->getMessage();
        }

        DB::commit();

        return '';
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $tahun = TahunAjaran::find($id);

        $tahun->loadTambahan();

        if ($tahun->banyakmulaiiuran > 0 || $tahun->banyakiurandetail > 0) {
            return response()->json([
                'data' => ['Gagal Menghapus data! Tahun ajaran ini digunakan oleh '.$tahun->banyakmulaiiuran.' data Mulai Iuran Siswa dan '.$tahun->banyakiurandetail.' data detail Iuran Siswa']
            ], 422);
        }

        $hasil = $this->simpanTransaksiDelete($tahun);
        if ($hasil == '') {
            return response()->json([
                'data' => 'Sukses Menghapus Data'
            ]);
        } else {
            return response()->json([
                'data' => ['Gagal Menghapus data! Mungkin data ini sedang digunakan oleh data di tabel lainnya! Err: ' + $hasil]
            ], 422);
        }
    }

    protected function simpanTransaksiDelete($tahun)
    {
        DB::beginTransaction();

        try {
            $tahun->delete();
        } catch (ValidationException $ex) {
            DB::rollback();
            return $ex->getMessage();
        } catch (Exception $ex) {
            DB::rollback();
            return $ex->getMessage();
        }

        DB::commit();

        return '';
    }
}