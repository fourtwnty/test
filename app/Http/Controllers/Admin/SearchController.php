<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\HomeController;
use App\Models\Bulan;
use App\Models\Kelas;
use App\Models\Program;
use App\Models\Siswa;
use App\Models\User;
use App\Models\Utility;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class SearchController extends HomeController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function cekHakAkses($hak)
    {
        return response()->json(Auth::user()->can($hak));
    }

    public function getBulan()
    {
        $bulan = Bulan::all();
        $cacah = 0;
        $data = [];

        foreach ($bulan as $d) {
            $data[$cacah] = [$d->id, $d->name];
            $cacah++;

        }

        return response()->json([
            'data' => $data
        ]);
    }

    public function siswamulaisearch($keyword)
    {
        $siswa = Siswa::where(['nisn' => $keyword, 'aktif' => true])->first();
//        $siswa = Siswa::where('nisn', $keyword)->first();

        if ($siswa !== null) {

            $mulaiIuran = $siswa->mulaiiuran()->first();

            if ($mulaiIuran !== null) {
                return response()->json([
                    'nama' => 'Sudah terdapat data Mulai Iuran untuk siswa ini.'
                ]);
            }

            return response()->json([
                'nama' => $siswa->nama,
                'nisn' => $siswa->nisn,
            ]);
        }
        return null;

    }

    public function siswabynisn($keyword)
    {
        $siswa = Siswa::where(['nisn' => $keyword, 'aktif' => true])->first();
//        $siswa = Siswa::where('nisn', $keyword)->first();

        if ($siswa !== null) {
            return response()->json([
                'nama' => $siswa->nama,
                'nisn' => $siswa->nisn,
            ]);
        }
        return null;

    }

    public function daftarTunggakan(Request $request)
    {
        if (!$request->ajax()) {
            return null;
        }

        $keyword = Input::get('keywords');

        $siswa = Siswa::where(['nisn' => $keyword, 'aktif' => true])->first();

        if ($siswa !== null) {
            $daftarTunggakan = $siswa->tunggakan();

            if ($daftarTunggakan !== null) {
                if (is_string($daftarTunggakan)) {
                    return response()->json([
                        'data' => [
                        ]
                    ]);
                }

                // jika tidak, format response
                $cacah = 0;
                $data = [];

                foreach ($daftarTunggakan as $tunggak) {
//                    $data[$cacah] = [$cacah + 1, $tunggak['bulan'], $tunggak['harga']];
                    $data[$cacah] = [$cacah + 1, $tunggak['bulan'], number_format($tunggak['harga'], 0, ',', '.')];
                    $cacah++;
                }

                return response()->json([
                    'data' => $data
                ]);
            }

            return response()->json([
                'data' => [
                ]
            ]);
        }

        return response()->json([
            'data' => [
            ]
        ]);
    }

    public function getKodeTransaksi()
    {
        $kodeTransaksi = 'T' . date('ymdHis'); //TYYmmddHHiiss

        return response()->json([
            'kode' => $kodeTransaksi,
        ]);
    }

    public function getPenggunaTerdaftar()
    {
        $users = User::where('active', true)->get();

        $jumlah = count($users);

        return response()->json(''.$jumlah);
    }

    public function getTotalSiswa() {
        $data = Siswa::where('aktif', true)->get();

        $jumlah = count($data);

        return response()->json(''.$jumlah);
    }

    protected $arraybulan = [
        '01' => 'Januari',
        '02' => 'Februari',
        '03' => 'Maret',
        '04' => 'April',
        '05' => 'Mei',
        '06' => 'Juni',
        '07' => 'Juli',
        '08' => 'Agustus',
        '09' => 'September',
        '10' => 'Oktober',
        '11' => 'November',
        '12' => 'Desember',
    ];

    public function gettotalpenunggak() {
        $data = Siswa::where('aktif', true)->get();

        $menunggak = 0;
        $lunas = 0;

        foreach ($data as $d) {
            if ($d->isMenunggakBulanIni()) {
                $menunggak++;
            } else {
                $iurandetail = DB::table('iuran_detail')
                    ->join('iuran', 'iuran.id', '=', 'iuran_detail.iuran_id')
                    ->join('siswa', 'siswa.id', '=', 'iuran.siswa_id')
                    ->join('tahun_ajaran', 'tahun_ajaran.id', '=', 'iuran_detail.tahun_id')
                    ->join('bulan', 'bulan.id', '=', 'iuran_detail.bulan_id')
                    ->where('siswa.id', $d->id)
                    ->where('tahun_ajaran.name', Utility::getTahunAjaranSekarang())
                    ->where('bulan.name', $this->arraybulan[date('m')])
                    ->select('iuran_detail.*')
                    ->get();

                if (!$iurandetail->isEmpty()) {
                    $lunas++;
                }
            }
        }

        return response()->json(['menunggak'=>$menunggak, 'lunas'=>$lunas]);
    }

    public function gettotalprodi() {
        $data = Program::all();

        $jumlah = count($data);

        return response()->json(''.$jumlah);
    }

    public function gettotalkelas() {
        $data = Kelas::all();

        $jumlah = count($data);

        return response()->json(''.$jumlah);
    }
}