<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\HomeController;
use App\Http\Requests\UpdatePengesahRequest;
use App\Models\Pengesah;
use Exception;

use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;

class PengesahController extends HomeController
{
    public function __construct() {
        parent::__construct();
    }

    public function index() {
        return view('page.pengesah.index');
    }

    public function pengesahs () {

        $pengesah = Pengesah::all();
        $cacah = 0;
        $data = [];

//        $table->string('name', 100)->unique;
//        $table->string('display_name', 200);
//        $table->string('nilai', 200);
//        $table->string('nip', 18)->unique;

        foreach ($pengesah as $d) {
            $data[$cacah] = [$d->name, $d->display_name, $d->nilai, $d->nip, $d->id];
            $cacah++;

        }

        return response()->json([
            'data' => $data
        ]);

    }

    public function edit($id)
    {
        $pengesah = Pengesah::find($id);

        return response()->json([
            'id' => $pengesah->id,
            'name' => $pengesah->name,
            'display_name'=>$pengesah->display_name,
            'nilai' => $pengesah->nilai,
            'nip'=>$pengesah->nip,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePengesahRequest $request, $id)
    {
        if ($request->ajax()) {
            $input = $request->all();

            if (!isset($input['_token'])) {
                return response()->json([
                    'data' => $input->toArray()
                ]);
            } else {
                $pengesah = Pengesah::find($id);

                if ($pengesah != null) {
                    $hasil = $this->simpanTransaksiUpdate($input, $pengesah);
                    if ($hasil == '') {
                        return response()->json([
                            'data' => 'Sukses Mengubah Data'
                        ]);
                    } else {
                        return response()->json([
                            'data' => ['Gagal mengubah data pengesah laporan! Periksa data anda dan pastikan server MySQL anda sedang aktif! Err: ' + $hasil]
                        ], 422);
                    }
                } else {
                    return response()->json([
                        'data' => ['Gagal mengubah data pengesah laporan! Pengesah tidak ditemukan']
                    ], 422);
                }
            }
        }
    }

    protected function simpanTransaksiUpdate($input, $pengesah) {
        DB::beginTransaction();

        try {
            DB::table('pengesah')
                ->where('id', $pengesah->id)
                ->update(
                    [
                        'display_name' => $input['display_name'],
                        'nilai' => $input['nilai'],
                        'nip' => $input['nip'],
                        'updated_at' => date('Y/m/d H:i:s')
                    ]);
        } catch (ValidationException $ex) {
            DB::rollback();
            return $ex->getMessage();
        } catch (Exception $ex) {
            DB::rollback();
            return $ex->getMessage();
        }

        DB::commit();

        return '';
    }

}
