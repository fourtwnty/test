<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\HomeController;
use App\Models\Identitas;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Validation\ValidationException;
use Intervention\Image\Facades\Image;
use Exception;

class IdentitasController extends HomeController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        return view('page.identitas');
    }

    public function getIdentitas()
    {
        $identitas = Identitas::first();

        if ($identitas == null) {
            return response()->json([
                'data' => ['Gagal memuat halaman! Tidak ada data identitas di database']
            ], 422);
        }

//        {{--protected $fillable = ['nama', 'alamat', 'telp', 'fax',--}}
//        {{--'logo', 'dinas', 'kota', 'kelurahan',--}}
//        {{--'kecamatan', 'kabupaten',--}}
//        {{--];--}}


        return response()->json([
            'id' => $identitas->id,
            'nama' => $identitas->nama,
            'alamat' => $identitas->alamat,
            'telp' => $identitas->telp,
            'fax' => $identitas->fax,
            'logo' => public_path() . '/' . env('IMG_DIR') . '/' . env('IMG_IDENTITAS_DIR') . '/' . $identitas->logo,
            'srclogogambar' => '/' . env('IMG_DIR') . '/' . env('IMG_IDENTITAS_DIR') . '/' . $identitas->logo,
            'dinas' => $identitas->dinas,
            'kota' => $identitas->kota,
            'kelurahan' => $identitas->kelurahan,
            'kecamatan' => $identitas->kecamatan,
            'kabupaten' => $identitas->kabupaten,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
//    public function update(Request $request, $id)
//    {
//        dd($request->all());
//
////        if ($request->ajax()) {
////            $input = $request->all();
////
////            if (!isset($input['_token'])) {
////                return response()->json([
////                    'data' => $input->toArray()
////                ]);
////            } else {
////                $pengesah = Pengesah::find($id);
////
////                if ($pengesah != null) {
////                    $hasil = $this->simpanTransaksiUpdate($input, $pengesah);
////                    if ($hasil == '') {
////                        return response()->json([
////                            'data' => 'Sukses Mengubah Data'
////                        ]);
////                    } else {
////                        return response()->json([
////                            'data' => ['Gagal mengubah data pengesah laporan! Periksa data anda dan pastikan server MySQL anda sedang aktif! Err: ' + $hasil]
////                        ], 422);
////                    }
////                } else {
////                    return response()->json([
////                        'data' => ['Gagal mengubah data pengesah laporan! Pengesah tidak ditemukan']
////                    ], 422);
////                }
////            }
////        }
//    }
//
//    protected function simpanTransaksiUpdate($input, $pengesah) {
////        DB::beginTransaction();
////
////        try {
////            DB::table('pengesah')
////                ->where('id', $pengesah->id)
////                ->update(
////                    [
////                        'display_name' => $input['display_name'],
////                        'nilai' => $input['nilai'],
////                        'nip' => $input['nip'],
////                        'updated_at' => date('Y/m/d H:i:s')
////                    ]);
////        } catch (ValidationException $ex) {
////            DB::rollback();
////            return $ex->getMessage();
////        } catch (Exception $ex) {
////            DB::rollback();
////            return $ex->getMessage();
////        }
////
////        DB::commit();
////
////        return '';
//    }

    public function update(Request $request, $id)
    {
        $input = $request->all();


        $identitas = Identitas::find($id);

        // cek upload start

        if (isset($_FILES)) {
            if (isset($_FILES['logo-baru']) && $_FILES['logo-baru']['name'] != '') {
                $img_whitelist = array('jpg', 'jpeg', 'png', 'gif');
                $img_tmp_name = $_FILES['logo-baru']['tmp_name'];
                $img_name = basename($_FILES['logo-baru']['name']);
                $img_error = $_FILES['logo-baru']['error'];

                if ($img_error === UPLOAD_ERR_OK) {
                    $img_extension = pathinfo($img_name, PATHINFO_EXTENSION);

                    if (!in_array($img_extension, $img_whitelist)) {
                        $img_error = 'Tipe File Logo tidak valid!';
                        return redirect('identitas')->with('error', $img_error);
                    } else {
                        $hasil = $this->simpanTransaksiUpdate($identitas, $input, $img_tmp_name, $img_extension);

                        if ($hasil != '') {
                            return redirect('identitas')->with('error', $hasil);
                        } else {
                            return redirect('identitas')->with('message', 'Data berhasil diubah!');
                        }
                    }
                } else {
                    return redirect('identitas')->with('error', 'Terjadi Kesalahan saat mengupload file logo');
                }
            } else {
                $hasil = $this->simpanTransaksiUpdate($identitas, $input, '', '');

                if ($hasil != '') {
                    return redirect('identitas')->with('error', $hasil);
                } else {
                    return redirect('identitas')->with('message', 'Data berhasil diubah!');
                }
            }
        } else {
            $hasil = $this->simpanTransaksiUpdate($identitas, $input, '', '');

            if ($hasil != '') {
                return redirect('master/barang/' . $id . '/edit')->with('error', $hasil);
            } else {
                return redirect('master/barang')->with('message', 'Data berhasil diubah!');
            }
        }


    }

    public function simpanTransaksiUpdate($identitas, $input, $img_tmp_name, $img_extension)
    {
        DB::beginTransaction();

        try {

            if ($img_tmp_name != '') {
                // menentukan direktori gambar yang akan disimpan
                $path_dir = public_path() . '/' . env('IMG_DIR').'/'.env('IMG_IDENTITAS_DIR').'/identitas.'. $img_extension;
            }

            $datalogo = $identitas->logo;

            if ($img_tmp_name != '') {
                $gambarlama = $identitas->logo;

                if (trim($gambarlama) != '') {
                    if (file_exists(public_path(). '/' . env('IMG_DIR').'/'.env('IMG_IDENTITAS_DIR').'/' . $gambarlama)) {
                        if ($gambarlama != 'identitas.' . $img_extension) {
                             File::delete(public_path(). '/' . env('IMG_DIR').'/'.env('IMG_IDENTITAS_DIR').'/' . $gambarlama);
                        }
                    }
                }

                $img = Image::make($img_tmp_name);
                $img->save($path_dir);

                $datalogo = 'identitas.' . $img_extension;
            }

            DB::table('identitas')
                ->where('id', $identitas->id)
                ->update(
                    [
                        'nama' => $input['nama'],
                        'alamat' => $input['alamat'],
                        'telp' => $input['telp'],
                        'fax' => $input['fax'],
                        'logo' => $datalogo,
                        'dinas' => $input['dinas'],
                        'kota' => $input['kota'],
                        'kelurahan' => $input['kelurahan'],
                        'kecamatan' => $input['kecamatan'],
                        'kabupaten' => $input['kabupaten'],
                        'updated_at' => date('Y/m/d H:i:s')
                    ]);
        } catch (ValidationException $ex) {
            DB::rollback();

            if (isset($path_dir) && file_exists($path_dir)) {
                File::delete($path_dir);
            }
            return $ex->getMessage();
        } catch (Exception $ex) {
            DB::rollback();
            if (isset($path_dir) && file_exists($path_dir)) {
                File::delete($path_dir);
            }

            return $ex->getMessage();
        }

        DB::commit();

        return '';
    }

    public function getNamaSekolah()
    {
        $identitas = Identitas::first();

//        dd($identitas);
        if ($identitas != null) {
            return response()->json($identitas->nama . ' <small>' . date('d F Y') . '</small>');
        } else {
            return response()->json('');
        }
    }
}
