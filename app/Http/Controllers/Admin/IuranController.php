<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\HomeController;
use App\Http\Requests\CreateIuranRequest;
use App\Models\Identitas;
use App\Models\Iuran;
use App\Models\IuranDetail;
use App\Models\Pengesah;
use App\Models\Siswa;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;

class IuranController extends HomeController
{
    public function __construct() {
        parent::__construct();
    }

    public function index() {
        return view('page/iuran/index');
    }

    public function iurans() {
        $iuran =  Iuran::latest('tgl')->get();
        $cacah = 0;
        $data = [];

        foreach ($iuran as $d) {
            $arr_tgl = explode ("-", $d->tgl, 3);

            $tglAsli = Carbon::createFromDate((int)$arr_tgl[0],(int)$arr_tgl[1],(int)$arr_tgl[2]);

            $d->loadTambahan();

            $data[$cacah] = [$d->id, $tglAsli->format('d-m-Y'), $d->nisn_siswa, $d->name_siswa, $d->total, $d->string_keterangan ,$d->id];
            $cacah++;

        }

        return response()->json([
            'data' => $data
        ]);
    }

    public function create() {
        return view('page/iuran/create');
    }

    public function store(CreateIuranRequest $request)
    {

        $input = $request->all();
//
        if (!isset($input['data_seleksi'])) {
            return response()->json([
                'data' => ['Silahkan menginputkan data bulan tunggakan yang akan dibayarkan !!!']
            ], 422);
        }

        $data_seleksi = $input['data_seleksi'];
        sort($data_seleksi);

        $tempidc = 0;
        foreach ($data_seleksi as $idc) {
            if ($tempidc === 0) {
                if ($idc != 1) {
                    dd($idc);
                    return response()->json([
                        'data' => ['Bulan yang dibayar harus berurut.']
                    ], 422);
                } else {
                    $tempidc = 1;
                }
            } else if ($tempidc >= 1) {
                $tempidc++;
                if ($idc != $tempidc) {
                    dd($tempidc);
                    return response()->json([
                        'data' => ['Bulan yang dibayar harus berurut.']
                    ], 422);
                }
            }

        }

        $siswa = Siswa::where(['nisn' => $input['nisn'], 'aktif' => true])->first();
        if ($siswa == null) {
            return response()->json([
                'data' => ['NISN Siswa yang diinputkan, tidak terdaftar !']
            ], 422);
        }

        $daftarTunggakan = $siswa->tunggakanlengkap();

        $daftarTunggakanSeleksi = [];
        $totalTransaksi = 0;

        foreach ($data_seleksi as $i => $v) {

            foreach ($daftarTunggakan as $key => $dt) {
                if ($v == $key + 1) {
                    $daftarTunggakanSeleksi[$key] = $dt;
                    $totalTransaksi += $dt['harga'];
                    break;
                }
            }
        }

        $hasil = $this->simpanTransaksiCreate($daftarTunggakanSeleksi, $totalTransaksi, $input['kodetransaksi'], $siswa);
        if ($hasil == '') {
            return response()->json([
                'data' => 'Data Pembayaran Iuran Komite berhasil ditambahkan!'
            ]);
        } else {
            return response()->json([
                'data' => ['Gagal menyimpan data pembayaran iuran komite! Periksa data anda dan pastikan server MySQL anda sedang aktif! Err: ' + $hasil]
            ], 422);
        }
    }

    /**
     * @param $input
     * @return string
     */
    protected function simpanTransaksiCreate(array $daftarTunggakan, $total = 0, $kodeTransaksi = '', $siswa = null) {

        DB::beginTransaction();

        try {
            $iuran = new Iuran();
            $iuran->id = $kodeTransaksi;
            $iuran->tgl = date('Y/m/d');
            $iuran->siswa_id = $siswa->id;
            $iuran->total = $total;
            $iuran->save();

            foreach ($daftarTunggakan as $dt) {
                $iurandetail = new IuranDetail();
                $iurandetail->iuran_id = $kodeTransaksi;
                $iurandetail->tahun_id = $dt['tahun']->id;
                $iurandetail->bulan_id = $dt['bulan_id'];
                $iurandetail->harga = $dt['harga'];
                $iurandetail->save();
            }

        } catch (ValidationException $ex) {
            DB::rollback();
            return $ex->getMessage();;
        } catch (Exception $ex) {
            DB::rollback();
            return $ex->getMessage();;
        }

        DB::commit();

        return '';
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $iuran = Iuran::find($id);

        $hasil = $this->simpanTransaksiDelete($iuran);
        if ($hasil == '') {
            return response()->json([
                'data' => 'Sukses Menghapus Data'
            ]);
        } else {
            return response()->json([
                'data' => ['Gagal Menghapus data! Mungkin data ini sedang digunakan oleh data di tabel lainnya! Err: ' + $hasil]
            ], 422);
        }
    }

    protected function simpanTransaksiDelete($iuran)
    {
        DB::beginTransaction();

        try {
            $iuran->delete();
        } catch (ValidationException $ex) {
            DB::rollback();
            return $ex->getMessage();
        } catch (Exception $ex) {
            DB::rollback();
            return $ex->getMessage();
        }

        DB::commit();

        return '';
    }

    public function nota($kode) {
        return view('page.iuran.report.nota', compact('kode'));
    }

    public function previewnota($kode) {
        $iuran = Iuran::find($kode);
        $iuran->loadTambahan();

        $identitas = Identitas::first();

//        return response()->json([
//            'id' => $identitas->id,
//            'nama' => $identitas->nama,
//            'alamat' => $identitas->alamat,
//            'telp' => $identitas->telp,
//            'fax' => $identitas->fax,
//            'logo' => public_path() . '/' . env('IMG_DIR') . '/' . env('IMG_IDENTITAS_DIR') . '/' . $identitas->logo,
//            'srclogogambar' => '/' . env('IMG_DIR') . '/' . env('IMG_IDENTITAS_DIR') . '/' . $identitas->logo,
//            'dinas' => $identitas->dinas,
//            'kota' => $identitas->kota,
//            'kelurahan' => $identitas->kelurahan,
//            'kecamatan' => $identitas->kecamatan,
//            'kabupaten' => $identitas->kabupaten,
//        ]);

        $arr_tgl = explode ("-", $iuran->tgl, 3);

        $tglAsli = Carbon::createFromDate((int)$arr_tgl[0],(int)$arr_tgl[1],(int)$arr_tgl[2]);

        $pengesah = Pengesah::where('name' , 'bendahara_komite')->first();

        $pdf = App::make('dompdf.wrapper');
        $pdf->loadView('page.iuran.report.preview', [
            'iuran'=>$iuran,
            'tgl_transaksi' => $tglAsli->format('d F Y'),
            'pengesah' => $pengesah,
            'kota' => $identitas->kota,
            'srcimage' => env('IMG_DIR') . '/' . env('IMG_IDENTITAS_DIR') . '/' . $identitas->logo,
        ]);
        $pdf->setPaper("nota_1")->setWarnings(false);

        return $pdf->stream();
    }
}
