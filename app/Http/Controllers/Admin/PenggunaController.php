<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\HomeController;
use App\Http\Requests\CreatePenggunaRequest;
use App\Http\Requests\GantiPasswordRequest;
use App\Http\Requests\UpdatePenggunaRequest;
use App\Models\Role;
use App\Models\User;
use Exception;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;

class PenggunaController extends HomeController
{
    public function __construct() {
        parent::__construct();
    }

    public function index() {
        return view('page/pengguna/index');
    }

    public function penggunas() {
        $user = User::all();
        $cacah = 0;
        $data = [];

        foreach ($user as $d) {
            $d->loadTambahan();

            if ($d->role()->name == 'owner') {
                if (Auth::user()->can('lihat_owner')) {
                    $data[$cacah] = [$d->username, $d->fullname, $d->email, $d->role_name, $d->description, ($d->active ? 'Active': 'Not Active'), $d->id];
                    $cacah++;
                }
            } else {
                $data[$cacah] = [$d->username, $d->fullname, $d->email, $d->role_name, $d->description, ($d->active ? 'Active': 'Not Active'), $d->id];
                $cacah++;
            }
        }

        return response()->json([
            'data' => $data
        ]);
    }

    public function create() {
        return view('page/pengguna/create');
    }

    public function getSelectGroup() {
        $role = DB::table('roles')->select('id', 'display_name', 'name')->orderBy('display_name')->get();

        $cacah = 0;
        $data = [];

        foreach ($role as $i => $d) {

            if ($d->name == 'owner') {
                if (Auth::user()->can('lihat_owner')) {
                    $data[$cacah] = [$d->id, $d->display_name];
                    $cacah++;
                }
            } else {
                $data[$cacah] = [$d->id, $d->display_name];
                $cacah++;
            }
        }

        return response()->json([
            'data' => $data
        ]);
    }

    public function store(CreatePenggunaRequest $request)
    {
        if ($request->ajax()) {
            $input = $request->all();

            if (!isset($input['_token'])) {
                return response()->json([
                    'data' => $input->toArray()
                ]);
            } else {
                $hasil = $this->simpanTransaksiCreate($input);
                if ($hasil == '') {
                    return response()->json([
                        'data' => 'Sukses Menyimpan'
                    ]);
                } else {
                    return response()->json([
                        'data' => ['Gagal menyimpan data user! Periksa data anda dan pastikan server MySQL anda sedang aktif! Err: ' + $hasil]
                    ], 422);
                }

            }
        }
    }

    protected function simpanTransaksiCreate($input) {
//        dd($input);


        DB::beginTransaction();

        try {

            $user = new User();
            $user->fullname = $input['fullname'];
            $user->email = $input['email'];
            $user->username = $input['username'];
            $user->password = bcrypt($input['password']);
            $user->address = $input['address'];
            $user->gsm = $input['gsm'];
            $user->phone = $input['phone'];
            $user->gender = $input['gender'];
            $user->description = $input['description'];
            $user->active = $input['active'];
            $user->save();

            $role = Role::where('id', $input['role'])->first();

            $user->attachRole($role);
        } catch (ValidationException $ex) {
            DB::rollback();
            return $ex->getMessage();;
        } catch (Exception $ex) {
            DB::rollback();
            return $ex->getMessage();;
        }

        DB::commit();

        return '';
    }

    public function show($id) {
        $user = User::find($id);

        $user->loadTambahan();
        return response()->json([
            'username' => $user->username,
            'role' => $user->role_name,
            'email' => $user->email,
            'fullname' => $user->fullname,
            'email' => $user->email,
            'address' => $user->address,
            'gsm' => $user->gsm,
            'phone' => $user->phone,
            'gender' => ($user->gender == 'L') ? 'Laki - Laki' : 'Perempuan',
            'active' => ($user->active) ? 'Aktif' : 'Tidak Aktif',
            'description' => $user->description
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);

        return response()->json([
            'id' => $user->id,
            'username' => $user->username,
            'role' => $user->role()->id,
            'fullname' => $user->fullname,
            'email' => $user->email,
            'address' => $user->address,
            'gsm' => $user->gsm,
            'phone' => $user->phone,
            'gender' => $user->gender,
            'active' => $user->active,
            'description' => $user->description
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePenggunaRequest $request, $id)
    {
        if ($request->ajax()) {
            $input = $request->all();

            if (!isset($input['_token'])) {
                return response()->json([
                    'data' => $input->toArray()
                ]);
            } else {
                $user = User::find($id);

                $userCari = User::where('username', $input['username'])->first();
                if ($userCari != null) {
                    if ($user->id != $userCari->id) {
                        return response()->json([
                            'data' => ['Username ini sudah digunakan oleh data lainnya!']
                        ], 422);
                    }
                }

                $userEmail = User::where('email', $input['email'])->first();
                if ($userEmail != null) {
                    if ($user->id != $userEmail->id) {
                        return response()->json([
                            'data' => ['Alamat email ini sudah digunakan oleh data lainnya!']
                        ], 422);
                    }
                }

                if ($user != null) {
                    $role = Role::find($input['role']);
                    if ($role != null) {
                        $hasil = $this->simpanTransaksiUpdate($input, $user, $role);
                        if ($hasil == '') {
                            return response()->json([
                                'data' => 'Sukses Mengubah Data'
                            ]);
                        } else {
                            return response()->json([
                                'data' => ['Gagal mengubah data user! Periksa data anda dan pastikan server MySQL anda sedang aktif! Err: ' + $hasil]
                            ], 422);
                        }
                    } else {
                        return response()->json([
                            'data' => ['Group User tidak terdaftar di database! Silahkan refresh browser anda!']
                        ], 422);
                    }
                } else {
                    return response()->json([
                        'data' => ['Gagal mengubah data user! User Aplikasi tidak ditemukan di database']
                    ], 422);
                }
            }
        }
    }

    protected function simpanTransaksiUpdate($input, $user, $role) {
        DB::beginTransaction();

        try {
            $dataubah = [
                'fullname' => $input['fullname'],
                'email' => $input['email'],
                'username' => $input['username'],
                'address' => $input['address'],
                'gsm' => $input['gsm'],
                'email' => $input['email'],
                'phone' => $input['phone'],
                'gender' => $input['gender'],
                'description' => $input['description'],
                'active' => $input['active'],
                'updated_at' => date('Y/m/d H:i:s')
            ];

            if ($input['ubahkatasandi'] != '0') {
                $dataubah['password'] = bcrypt($input['password']);
            }

            DB::table('users')
                ->where('id', $user->id)
                ->update($dataubah);

            $user->roles()->sync([]);
            $user->attachRole($role);
        } catch (ValidationException $ex) {
            DB::rollback();
            return $ex->getMessage();
        } catch (Exception $ex) {
            DB::rollback();
            return $ex->getMessage();
        }

        DB::commit();

        return '';
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);

        if ($user->id == Auth::user()->id) {
            return response()->json([
                'data' => ['Tidak dapat menghapus user yang sedang aktif saat ini']
            ], 422);
        }

        $user->loadTambahan();

        $hasil = $this->simpanTransaksiDelete($user);
        if ($hasil == '') {
            return response()->json([
                'data' => 'Sukses Menghapus Data'
            ]);
        } else {
            return response()->json([
                'data' => ['Gagal Menghapus data! Mungkin data ini sedang digunakan oleh data di tabel lainnya! Err: ' + $hasil]
            ], 422);
        }
    }

    protected function simpanTransaksiDelete($user)
    {
//        dd($input);
        DB::beginTransaction();

        try {
            $user->delete();
        } catch (ValidationException $ex) {
            DB::rollback();
            return $ex->getMessage();
        } catch (Exception $ex) {
            DB::rollback();
            return $ex->getMessage();
        }

        DB::commit();

        return '';
    }

    public function gantipassword() {
        return view('page.pengguna.password');
    }

    public function changePassword(GantiPasswordRequest $request) {
        if ($request->ajax()) {
            $input = $request->all();

            if (!isset($input['_token'])) {
                return response()->json([
                    'data' => $input->toArray()
                ]);
            } else {
                if (!isset($input['passwordlama'])) {
                    return response()->json([
                        'data' => ['Gagal mengubah kata sandi. Kata Sandi lama tidak ada!!!']
                    ], 422);
                }

                if (!isset($input['passwordbaru'])) {
                    return response()->json([
                        'data' => ['Gagal mengubah kata sandi. Kata Sandi baru tidak ada!!!']
                    ], 422);
                }

                if (!Hash::check($input['passwordlama'], Auth::user()->password)) {
                    return response()->json([
                        'data' => ['Kata sandi lama tidak benar!!!']
                    ], 422);
                }

                $hasil = $this->simpanTransaksiPassword($input);
                if ($hasil == '') {
                    return response()->json([
                        'data' => 'Sukses Mengubah Kata Sandi'
                    ]);
                } else {
                    return response()->json([
                        'data' => ['Gagal mengubah kata sandi! Periksa data anda dan pastikan server MySQL anda sedang aktif! Err: ' + $hasil]
                    ], 422);
                }

            }
        }
    }

    protected function simpanTransaksiPassword($input) {
        DB::beginTransaction();

        try {

            $dataubah = [
                'password' => bcrypt($input['passwordbaru']),
                'updated_at' => date('Y/m/d H:i:s')
            ];
            DB::table('users')
                ->where('id', Auth::user()->id)
                ->update($dataubah);
        } catch (ValidationException $ex) {
            DB::rollback();
            return $ex->getMessage();;
        } catch (Exception $ex) {
            DB::rollback();
            return $ex->getMessage();;
        }

        DB::commit();

        return '';
    }
}