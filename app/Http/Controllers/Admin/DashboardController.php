<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\HomeController;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\View;

class DashboardController extends HomeController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        return view('dashboard');
    }

//    public function showView($name = null)
//    {
//        if (Auth::check()) {
//            $url = URL::current();
//
//            $route_segment = explode('/', $url);
//            $count_segment = count($route_segment);
//
//            for ($x = 3; $x <= $count_segment - 1; $x++) {
//                if (isset($route_segment[3])) {
//                    if (View::exists($route_segment[$x] . '.' . $name)) {
//                        return view($route_segment[$x] . '.' . $name);
//                    }
//                }
//
//                if (isset($route_segment[4])) {
//                    if (View::exists($route_segment[3] . '.' . $route_segment[4] . '.' . $name)) {
//                        return view($route_segment[3] . '.' . $route_segment[4] . '.' . $name);
//                    }
//                }
//
//                if (isset($route_segment[5])) {
//                    if (View::exists($route_segment[3] . '.' . $route_segment[4] . '.' . $route_segment[5] . '.' . $name)) {
//                        return view($route_segment[3] . '.' . $route_segment[4] . '.' . $route_segment[5] . '.' . $name);
//                    }
//                }
//
//                return view('errors/404');
//            }
//
//        }
//        return redirect('auth/login')->with('error', 'You must be logged in !');
//
//
//    }
}
