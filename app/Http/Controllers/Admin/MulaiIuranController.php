<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\HomeController;
use App\Http\Requests\CreateMulaiIuranRequest;
use App\Http\Requests\UpdateMulaiIuranRequest;
use App\Models\MulaiIuran;
use App\Models\Siswa;
use Exception;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;

class MulaiIuranController extends HomeController
{
    public function __construct() {
        parent::__construct();
    }

    public function index() {
        return view('page.pengaturan.mulaiiuran.index');
    }

    public function mulaiiurans() {
        $mulaiiuran = MulaiIuran::all();
        $cacah = 0;
        $data = [];

        foreach ($mulaiiuran as $d) {
            $d->loadTambahan();

            $data[$cacah] = [$d->nisn_siswa, $d->nama_siswa, $d->nama_mulaiiuran, $d->id];
            $cacah++;

        }

        return response()->json([
            'data' => $data
        ]);
    }

    public function create() {
        return view('page.pengaturan.mulaiiuran.create');
    }

    public function store(CreateMulaiIuranRequest $request)
    {
//        dd($request->all());

        if ($request->ajax()) {
            $input = $request->all();

            if (!isset($input['_token'])) {
                return response()->json([
                    'data' => $input->toArray()
                ]);
            } else {
                $siswa = Siswa::where('nisn', $input['nisn'])->first();
                if ($siswa == null) {
                    return response()->json([
                        'data' => ['NISN Siswa yang diinputkan tidak terdaftar di databse !']
                    ], 422);
                }

                $mulaiiuran = $siswa->mulaiiuran()->first();
                if ($mulaiiuran != null) {

                    return response()->json([
                        'data' => ['Data mulai iuran untuk siswa ini sudah terdaftar di database !']
                    ], 422);
                }

                $hasil = $this->simpanTransaksiCreate($input, $siswa);
                if ($hasil == '') {
                    return response()->json([
                        'data' => 'Sukses Menyimpan'
                    ]);
                } else {
                    return response()->json([
                        'data' => ['Gagal menyimpan data mulai iuran siswa! Periksa data anda dan pastikan server MySQL anda sedang aktif! Err: ' + $hasil]
                    ], 422);
                }

            }
        }
    }

    /**
     * @param $input
     * @return string
     */
    protected function simpanTransaksiCreate($input, $siswa) {
        DB::beginTransaction();

        try {
            $mulaiiuran = new MulaiIuran();
            $mulaiiuran->siswa_id = $siswa->id;
            $mulaiiuran->bulan_id = $input['bulan_id'];
            $mulaiiuran->tahun_id = $input['tahun_id'];
            $mulaiiuran->save();
        } catch (ValidationException $ex) {
            DB::rollback();
            return $ex->getMessage();;
        } catch (Exception $ex) {
            DB::rollback();
            return $ex->getMessage();;
        }

        DB::commit();

        return '';
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $mulaiiuran = MulaiIuran::find($id);
        $mulaiiuran->loadTambahan();

        return response()->json([
            'id'=>$mulaiiuran->id,
            'nisn' => $mulaiiuran->nisn_siswa,
            'nama' => $mulaiiuran->nama_siswa,
            'bulan_id' => $mulaiiuran->bulan_id,
            'tahun_id' => $mulaiiuran->tahun_id,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateMulaiIuranRequest $request, $id)
    {
//        dd($request->all());
        if ($request->ajax()) {
            $input = $request->all();

            if (!isset($input['_token'])) {
                return response()->json([
                    'data' => $input->toArray()
                ]);
            } else {
                $mulaiiuran = MulaiIuran::find($id);

                if ($mulaiiuran != null) {
                    $hasil = $this->simpanTransaksiUpdate($input, $mulaiiuran);
                    if ($hasil == '') {
                        return response()->json([
                            'data' => 'Sukses Mengubah Data'
                        ]);
                    } else {
                        return response()->json([
                            'data' => ['Gagal mengubah data mulai iuran siswa! Periksa data anda dan pastikan server MySQL anda sedang aktif! Err: ' + $hasil]
                        ], 422);
                    }
                } else {
                    return response()->json([
                        'data' => ['Gagal mengubah data mulai iuran siswa! Data Mulai Iuran tidak ditemukan']
                    ], 422);
                }
            }
        }
    }

    protected function simpanTransaksiUpdate($input, $mulaiiuran) {
        DB::beginTransaction();

        try {
            DB::table('mulai_iuran')
                ->where('id', $mulaiiuran->id)
                ->update(
                    [
                        'bulan_id' => $input['bulan_id'],
                        'tahun_id' => $input['tahun_id'],
                        'updated_at' => date('Y/m/d H:i:s')
                    ]);
        } catch (ValidationException $ex) {
            DB::rollback();
            return $ex->getMessage();
        } catch (Exception $ex) {
            DB::rollback();
            return $ex->getMessage();
        }

        DB::commit();

        return '';
    }
}