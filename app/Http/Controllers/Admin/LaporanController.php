<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\HomeController;
use App\Http\Requests\LaporanPembayaranRequest;
use App\Http\Requests\LaporanPembayaranTerseleksiRequest;
use App\Models\Identitas;
use App\Models\Iuran;
use App\Models\IuranDetail;
use App\Models\Kelas;
use App\Models\Pengesah;
use App\Models\Penunggak;
use App\Models\Program;
use App\Models\Siswa;
use App\Models\TahunAjaran;
use App\Models\Utility;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class LaporanController extends HomeController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function daftarPenunggak()
    {
        return view('page.laporan.daftar-penunggak');
    }

    public function getDaftarPenunggak()
    {
        $siswa = Siswa::where('aktif', true)->orderBy('nama')->get();

        $data = [];

        $cacah = 0;

//        dd($siswa);

        foreach ($siswa as $s) {
            $penunggak = new Penunggak($s);

            if ($penunggak->total > 0) {
                $data[$cacah] = [$s->id, $s->nisn, $s->nama, number_format($penunggak->total, 0, ',', '.'), $penunggak->string_keterangan];

                $cacah++;
            }

//            dd('disini');
        }

//        dd('data = '.$data);

        return response()->json([
            'data' => $data
        ]);
    }

    public function suratTagihan($rows_selected)
    {
//        $idsiswa = $rows_selected;
        $penunggak = [];

        $pesan = '';

        $idsiswa = explode(',', $rows_selected);

        foreach ($idsiswa as $id) {
//            dd($id);

            $siswa = Siswa::where(['id' => $id, 'aktif' => true])->first();
            $p = new Penunggak($siswa);
            $penunggak[] = $p;
        }

//        dd($penunggak);

        $pengesah = Pengesah::where('name', 'bendahara_komite')->first();
        $identitas = Identitas::first();

        $pdf = App::make('dompdf.wrapper');
//        $pdf->loadView('laporan.preview.cetak-daftar-penunggak', ['penunggak' => $data, 'total'=>$total]);
        $pdf->loadView('page.laporan.preview.preview-surat-tagihan', [
            'penunggak' => $penunggak,
            'pengesah' => $pengesah,
            'tahunsekarang' => Utility::getTahunAjaranSekarang(),
            'kota' => $identitas->kota,
            'srcimage' => env('IMG_DIR') . '/' . env('IMG_IDENTITAS_DIR') . '/' . $identitas->logo,
            'namadinas' => strtoupper($identitas->dinas),
            'namakabupaten' => 'KABUPATEN '.strtoupper($identitas->kabupaten),
            'namasekolah' => $identitas->nama,
            'namaalamat' => $identitas->alamat.(trim($identitas->kelurahan) != '' ? ' Kel. '.$identitas->kelurahan : '')
                .(trim($identitas->kecamatan) != '' ? ' Kec. '.$identitas->kecamatan : '')
                .(trim($identitas->telp) != '' ? ' Telp. '.$identitas->telp : '')
                .(trim($identitas->fax) != '' ? ' Fax. '.$identitas->fax : ''),
        ]);
        $pdf->setPaper('a4')->setWarnings(false);
        return $pdf->stream();

    }

    //        return response()->json([
//            'id' => $identitas->id,
//            'nama' => $identitas->nama,
//            'alamat' => $identitas->alamat,
//            'telp' => $identitas->telp,
//            'fax' => $identitas->fax,
//            'logo' => public_path() . '/' . env('IMG_DIR') . '/' . env('IMG_IDENTITAS_DIR') . '/' . $identitas->logo,
//            'srclogogambar' => '/' . env('IMG_DIR') . '/' . env('IMG_IDENTITAS_DIR') . '/' . $identitas->logo,
//            'dinas' => $identitas->dinas,
//            'kota' => $identitas->kota,
//            'kelurahan' => $identitas->kelurahan,
//            'kecamatan' => $identitas->kecamatan,
//            'kabupaten' => $identitas->kabupaten,
//        ]);

    public function laporanDaftarPenunggak() {
        $siswa = Siswa::all();

        $data = [];
        $total = 0;

        foreach ($siswa as $s) {
            $penunggak = new Penunggak($s);
            if ($penunggak->total > 0) {
                $total += $penunggak->total;
                $data[] = $penunggak;
            }
        }

        $identitas = Identitas::first();

        $pdf = App::make('dompdf.wrapper');
        $pdf->loadView('page.laporan.preview.cetak-daftar-penunggak', [
            'penunggak' => $data,
            'total'=>$total,
            'kota' => $identitas->kota,
            'srcimage' => env('IMG_DIR') . '/' . env('IMG_IDENTITAS_DIR') . '/' . $identitas->logo,
            'namadinas' => strtoupper($identitas->dinas),
            'namakabupaten' => 'KABUPATEN '.strtoupper($identitas->kabupaten),
            'namasekolah' => $identitas->nama,
            'namaalamat' => $identitas->alamat.(trim($identitas->kelurahan) != '' ? ' Kel. '.$identitas->kelurahan : '')
                .(trim($identitas->kecamatan) != '' ? ' Kec. '.$identitas->kecamatan : '')
                .(trim($identitas->telp) != '' ? ' Telp. '.$identitas->telp : '')
                .(trim($identitas->fax) != '' ? ' Fax. '.$identitas->fax : ''),
        ]);
//        $pdf->setPaper('a4')->setOrientation('landscape')->setWarnings(false);
        $pdf->setPaper('a4', 'landscape')->setWarnings(false);
        return $pdf->stream();
    }

    public function daftarSiswa() {
        return view('page.laporan.daftar-siswa');
    }

    public function getDaftarSiswa() {
        $siswa = Siswa::where('aktif', true)->orderBy('nama')->get();

        $data = [];

        $cacah = 0;
        foreach ($siswa as $s) {
            $s->loadTambahan();

            $data[$cacah] = [$s->id, $s->nisn, $s->nama, ($s->jk == 'L' ? 'Laki - Laki' : 'Perempuan'), $s->kelas_name];
            $cacah++;
        }

        return response()->json([
            'data' => $data
        ]);
    }

    public function laporanDaftarSiswa() {
        $siswa = Siswa::where('aktif', true)->orderBy('nama')->get();

        foreach ($siswa as $s) {
            $s->loadTambahan();
        }
        $identitas = Identitas::first();
        $pdf = App::make('dompdf.wrapper');
        $pdf->loadView('page.laporan.preview.previewlaporandaftarsiswa', ['siswa' => $siswa
            ,'srcimage' => env('IMG_DIR') . '/' . env('IMG_IDENTITAS_DIR') . '/' . $identitas->logo,
            'namadinas' => strtoupper($identitas->dinas),
            'namakabupaten' => 'KABUPATEN '.strtoupper($identitas->kabupaten),
            'namasekolah' => $identitas->nama,
            'namaalamat' => $identitas->alamat.(trim($identitas->kelurahan) != '' ? ' Kel. '.$identitas->kelurahan : '')
                .(trim($identitas->kecamatan) != '' ? ' Kec. '.$identitas->kecamatan : '')
                .(trim($identitas->telp) != '' ? ' Telp. '.$identitas->telp : '')
                .(trim($identitas->fax) != '' ? ' Fax. '.$identitas->fax : ''),]);
        $pdf->setPaper('a4', 'landscape')->setWarnings(false);
        return $pdf->stream();
    }

    public function laporanIndividuSiswa($rows_selected) {
        $idsiswa = explode(',', $rows_selected);

        $siswa = [];

        foreach ($idsiswa as $id) {
//            dd($id);

            $s = Siswa::where(['id' => $id, 'aktif' => true])->first();
            if ($s !== null) {
                $s->loadTambahan();
                $siswa[] = $s;
            }
        }
        $identitas = Identitas::first();
        $pdf = App::make('dompdf.wrapper');
        $pdf->loadView('page.laporan.preview.previewlaporanindividusiswa', ['siswa' => $siswa
            ,'srcimage' => env('IMG_DIR') . '/' . env('IMG_IDENTITAS_DIR') . '/' . $identitas->logo,
            'namadinas' => strtoupper($identitas->dinas),
            'namakabupaten' => 'KABUPATEN '.strtoupper($identitas->kabupaten),
            'namasekolah' => $identitas->nama,
            'namaalamat' => $identitas->alamat.(trim($identitas->kelurahan) != '' ? ' Kel. '.$identitas->kelurahan : '')
                .(trim($identitas->kecamatan) != '' ? ' Kec. '.$identitas->kecamatan : '')
                .(trim($identitas->telp) != '' ? ' Telp. '.$identitas->telp : '')
                .(trim($identitas->fax) != '' ? ' Fax. '.$identitas->fax : ''),]);
        $pdf->setPaper('a4')->setWarnings(false);
        return $pdf->stream();
    }

    public function siswaPerProgram() {
        return view('page.laporan.siswaperprogram');
    }

    public function getSiswaPerProgram(Request $request) {
        if (!$request->ajax()) {
            return null;
        }

        $keywords = Input::get('keywords');

        $siswa = DB::table('siswa')
            ->join('kelas', 'kelas.id', '=', 'siswa.kelas_id')
            ->join('programs', 'programs.id', '=', 'kelas.program_id')
            ->where('program_id', $keywords)
            ->where('aktif', true)
            ->select('siswa.*')
            ->get();

        $arraySiswa = [];
        foreach ($siswa as $s) {
            $instanceSiswa = new Siswa();
            $instanceSiswa->id = $s->id;
            $instanceSiswa->nisn = $s->nisn;
            $instanceSiswa->jk= $s->jk;
            $instanceSiswa->nama = $s->nama;
            $instanceSiswa->tempat_lahir = $s->tempat_lahir;
            $instanceSiswa->tgl_lahir = $s->tgl_lahir;
            $instanceSiswa->alamat = $s->alamat;
            $instanceSiswa->aktif = $s->aktif;
            $instanceSiswa->kelas_id = $s->kelas_id;
            $instanceSiswa->updated_at = $s->updated_at;
            $instanceSiswa->created_at = $s->created_at;
            $instanceSiswa->loadTambahan();

            $arraySiswa[] = $instanceSiswa;
        }

        $data = [];

        $cacah = 0;
        foreach ($arraySiswa as $s) {
            $data[$cacah] = [$s->nisn, $s->nama, $s->jk, $s->kelas_name];
            $cacah++;
        }

        return response()->json([
            'data' => $data
        ]);
    }

    public function laporanSiswaPerprogram($keywords) {
        $siswa = DB::table('siswa')
            ->join('kelas', 'kelas.id', '=', 'siswa.kelas_id')
            ->join('programs', 'programs.id', '=', 'kelas.program_id')
            ->where('program_id', $keywords)
            ->where('aktif', true)
            ->select('siswa.*')
            ->get();

        $arraySiswa = [];
        foreach ($siswa as $s) {
            $instanceSiswa = new Siswa();
            $instanceSiswa->id = $s->id;
            $instanceSiswa->nisn = $s->nisn;
            $instanceSiswa->jk= $s->jk;
            $instanceSiswa->nama = $s->nama;
            $instanceSiswa->tempat_lahir = $s->tempat_lahir;
            $instanceSiswa->tgl_lahir = $s->tgl_lahir;
            $instanceSiswa->alamat = $s->alamat;
            $instanceSiswa->aktif = $s->aktif;
            $instanceSiswa->kelas_id = $s->kelas_id;
            $instanceSiswa->updated_at = $s->updated_at;
            $instanceSiswa->created_at = $s->created_at;
            $instanceSiswa->loadTambahan();

            $arraySiswa[] = $instanceSiswa;
        }

        $program = Program::where('id', $keywords)->first();

        $identitas = Identitas::first();
        $pdf = App::make('dompdf.wrapper');
        $pdf->loadView('page.laporan.preview.previewlaporanprogramsiswa', ['siswa' => $arraySiswa, 'program'=>$program->name
            ,'srcimage' => env('IMG_DIR') . '/' . env('IMG_IDENTITAS_DIR') . '/' . $identitas->logo,
            'namadinas' => strtoupper($identitas->dinas),
            'namakabupaten' => 'KABUPATEN '.strtoupper($identitas->kabupaten),
            'namasekolah' => $identitas->nama,
            'namaalamat' => $identitas->alamat.(trim($identitas->kelurahan) != '' ? ' Kel. '.$identitas->kelurahan : '')
        .(trim($identitas->kecamatan) != '' ? ' Kec. '.$identitas->kecamatan : '')
        .(trim($identitas->telp) != '' ? ' Telp. '.$identitas->telp : '')
        .(trim($identitas->fax) != '' ? ' Fax. '.$identitas->fax : ''),]);
        $pdf->setPaper('a4', 'landscape')->setWarnings(false);

        return $pdf->stream();
    }

    public function siswaPerKelas() {
        return view('page.laporan.siswaperkelas');
    }

    public function getSiswaPerKelas (Request $request) {
        if (!$request->ajax()) {
            return null;
        }

        $keywords = Input::get('keywords');

        $siswa = Siswa::where(['kelas_id' => $keywords, 'aktif' => true])->get();

        foreach ($siswa as $s) {
            $s->loadTambahan();
        }

        $data = [];

        $cacah = 0;
        foreach ($siswa as $s) {
            $data[$cacah] = [$s->nisn, $s->nama, $s->jk, $s->kelas_name];
            $cacah++;
        }

        return response()->json([
            'data' => $data
        ]);
    }

    public function laporanSiswaPerKelas($keywords) {
        $siswa = Siswa::where(['kelas_id' => $keywords, 'aktif' => true])->get();

        foreach ($siswa as $s) {
            $s->loadTambahan();
        }

        $kelas = Kelas::where('id', $keywords)->first();
        $kelas->loadTambahan();

        $identitas = Identitas::first();
        $pdf = App::make('dompdf.wrapper');
        $pdf->loadView('page.laporan.preview.previewlaporankelassiswa', ['siswa' => $siswa,
            'kelas'=>$kelas->program_name.' '.$kelas->tingkatan
            ,'srcimage' => env('IMG_DIR') . '/' . env('IMG_IDENTITAS_DIR') . '/' . $identitas->logo,
            'namadinas' => strtoupper($identitas->dinas),
            'namakabupaten' => 'KABUPATEN '.strtoupper($identitas->kabupaten),
            'namasekolah' => $identitas->nama,
            'namaalamat' => $identitas->alamat.(trim($identitas->kelurahan) != '' ? ' Kel. '.$identitas->kelurahan : '')
                .(trim($identitas->kecamatan) != '' ? ' Kec. '.$identitas->kecamatan : '')
                .(trim($identitas->telp) != '' ? ' Telp. '.$identitas->telp : '')
                .(trim($identitas->fax) != '' ? ' Fax. '.$identitas->fax : ''),]);
        $pdf->setPaper('a4', 'landscape')->setWarnings(false);
        return $pdf->stream();
    }

    public function daftarPembayaran() {
        return view('page.laporan.pembayaraniuran');
    }

    public function getDaftarPembayaran(Request $request) {
        if (!$request->ajax()) {
            return null;
        }

        $tahunajaran = Input::get('tahunajaran');
        $mulaitanggal = Input::get('mulaitanggal');
        $sampaitanggal = Input::get('sampaitanggal');

        $data = [];

        if ($tahunajaran == ' ' && $mulaitanggal == '' && $sampaitanggal == '') {
            return response()->json([
                'data'=>$data
            ]);
        }

        if ($tahunajaran == ' ') {
            return response()->json(['data'=>$data]);
        }

        if ($mulaitanggal == '' || $sampaitanggal == '') {
            $iuranid = DB::table('iuran_detail')->distinct()
                ->join('tahun_ajaran', 'tahun_ajaran.id', '=', 'iuran_detail.tahun_id')
                ->join('iuran', 'iuran.id', '=', 'iuran_detail.iuran_id')
                ->where('iuran_detail.tahun_id', $tahunajaran)
                ->select('iuran.id')
                ->get();
        } else {
            $arr_tgl_dari = explode ("/", $mulaitanggal, 3);
            $arr_tgl_sampai= explode ("/", $sampaitanggal, 3);

            $from = $arr_tgl_dari[2].'/'.$arr_tgl_dari[1].'/'.$arr_tgl_dari[0];
            $to = $arr_tgl_sampai[2].'/'.$arr_tgl_sampai[1].'/'.$arr_tgl_sampai[0];

            $iuranid = DB::table('iuran_detail')->distinct()
                ->join('tahun_ajaran', 'tahun_ajaran.id', '=', 'iuran_detail.tahun_id')
                ->join('iuran', 'iuran.id', '=', 'iuran_detail.iuran_id')
                ->whereBetween('iuran.tgl',[$from, $to], 'and')
                ->where('iuran_detail.tahun_id', $tahunajaran)
                ->select('iuran.id')
                ->get();
        }

        $cacah=0;
        foreach($iuranid as $iid) {
            $iuran = Iuran::where('id', $iid->id)->first();
            $iuran->loadTambahan();

            $data[$cacah] = [ $cacah + 1,$iuran->id, $iuran->created_at->format('d-m-Y'), $iuran->nisn_siswa, $iuran->name_siswa, number_format($iuran->getTotalPerTahun($tahunajaran),0,',','.'), $iuran->getStringKeteranganPerTahun($tahunajaran)];
            $cacah++;
        }

        return response()->json([
            'data'=>$data
        ]);
    }

    protected function getDaftarPembayaranIuran($tahunajaran, $mulaitanggal, $sampaitanggal) {
        $data = [];

        if ($tahunajaran == ' ' && $mulaitanggal == '' && $sampaitanggal == '') {
            return null;
        }

        if ($tahunajaran == ' ') {
            return null;
        }

        if ($mulaitanggal == '' || $sampaitanggal == '') {
            $iuranid = DB::table('iuran_detail')->distinct()
                ->join('tahun_ajaran', 'tahun_ajaran.id', '=', 'iuran_detail.tahun_id')
                ->join('iuran', 'iuran.id', '=', 'iuran_detail.iuran_id')
                ->where('iuran_detail.tahun_id', $tahunajaran)
                ->select('iuran.id')
                ->get();
        } else {
            $arr_tgl_dari = explode ("/", $mulaitanggal, 3);
            $arr_tgl_sampai= explode ("/", $sampaitanggal, 3);

            $from = $arr_tgl_dari[2].'/'.$arr_tgl_dari[1].'/'.$arr_tgl_dari[0];
            $to = $arr_tgl_sampai[2].'/'.$arr_tgl_sampai[1].'/'.$arr_tgl_sampai[0];

            $iuranid = DB::table('iuran_detail')->distinct()
                ->join('tahun_ajaran', 'tahun_ajaran.id', '=', 'iuran_detail.tahun_id')
                ->join('iuran', 'iuran.id', '=', 'iuran_detail.iuran_id')
                ->whereBetween('iuran.tgl',[$from, $to], 'and')
                ->where('iuran_detail.tahun_id', $tahunajaran)
                ->select('iuran.id')
                ->get();
        }

        $cacah=0;
        foreach($iuranid as $iid) {
            $iuran = Iuran::where('id', $iid->id)->first();
            $iuran->loadTambahan();

            $data[$cacah] = [ $cacah + 1,$iuran->id, $iuran->created_at->format('d-m-Y'), $iuran->nisn_siswa, $iuran->name_siswa, number_format($iuran->getTotalPerTahun($tahunajaran),0,',','.'), $iuran->getStringKeteranganPerTahun($tahunajaran)];
            $cacah++;
        }

        return $data;
    }

    public function laporanIuran(LaporanPembayaranRequest $request)
    {
        $input = $request->all();

        $data = $this->getDaftarPembayaranIuran($input['tahunajaran'], $input['daritanggal'],$input['sampaitanggal']);

        $namaTahunAjaran = TahunAjaran::where('id', $input['tahunajaran'])->select('name')->first();
        $subtitle = 'Tahun Ajaran : '.$namaTahunAjaran['name'];

        if ($input['daritanggal'] != '' && $input['sampaitanggal'] != '') {
            $subtitle .= ', Periode : '.$input['daritanggal'].' s/d '.$input['sampaitanggal'];
        }

        $total = 0;
        foreach($data as $d) {
            $totalstring = $d[5];
            $totalstring = str_replace('.', '', $totalstring);
            $total += $totalstring;
        }

        $identitas = Identitas::first();

        $pdf = App::make('dompdf.wrapper');
        $pdf->loadView('page.laporan.preview.previewlaporaniuran', ['pembayaran'=>$data, 'subtitle'=>$subtitle, 'total'=>$total,
            'srcimage' => env('IMG_DIR') . '/' . env('IMG_IDENTITAS_DIR') . '/' . $identitas->logo,
            'namadinas' => strtoupper($identitas->dinas),
            'namakabupaten' => 'KABUPATEN '.strtoupper($identitas->kabupaten),
            'namasekolah' => $identitas->nama,
            'namaalamat' => $identitas->alamat.(trim($identitas->kelurahan) != '' ? ' Kel. '.$identitas->kelurahan : '')
                .(trim($identitas->kecamatan) != '' ? ' Kec. '.$identitas->kecamatan : '')
                .(trim($identitas->telp) != '' ? ' Telp. '.$identitas->telp : '')
                .(trim($identitas->fax) != '' ? ' Fax. '.$identitas->fax : ''),
        ]);
        $pdf->setPaper('a4', 'landscape')->setWarnings(false);
        return $pdf->stream();
//        dd($data);
    }

    protected function getIuranDetailTahun($tahunajaran, $idiuran) {
        $iurandetail = IuranDetail::where('iuran_id', $idiuran)->get();

        $kembali = [];

        foreach($iurandetail as $iud) {
            if ($iud->tahun_id == $tahunajaran) {
                $iud->loadTambahan();
                $kembali[$iud->id] = $iud;
            }
        }

        return $kembali;
    }

    public function laporanIuranDetail(LaporanPembayaranRequest $request) {
        $input = $request->all();

        $data = $this->getDaftarPembayaranIuran($input['tahunajaran'], $input['daritanggal'],$input['sampaitanggal']);

        $namaTahunAjaran = TahunAjaran::where('id', $input['tahunajaran'])->select('name')->first();
        $subtitle = 'Tahun Ajaran : '.$namaTahunAjaran['name'];

        if ($input['daritanggal'] != '' && $input['sampaitanggal'] != '') {
            $subtitle .= ', Periode : '.$input['daritanggal'].' s/d '.$input['sampaitanggal'];
        }

        $total = 0;
        foreach($data as $key => $d) {
            $totalstring = $d[5];
            $totalstring = str_replace('.', '', $totalstring);
            $total += $totalstring;

            $data[$key][7] = $this->getIuranDetailTahun ($input['tahunajaran'], $d[1]);
        }

        $identitas = Identitas::first();

        $pdf = App::make('dompdf.wrapper');
        $pdf->loadView('page.laporan.preview.previewlaporaniurandetail', ['pembayaran'=>$data, 'subtitle'=>$subtitle, 'total'=>$total,
            'cacah'=> 1
            ,'srcimage' => env('IMG_DIR') . '/' . env('IMG_IDENTITAS_DIR') . '/' . $identitas->logo,
            'namadinas' => strtoupper($identitas->dinas),
            'namakabupaten' => 'KABUPATEN '.strtoupper($identitas->kabupaten),
            'namasekolah' => $identitas->nama,
            'namaalamat' => $identitas->alamat.(trim($identitas->kelurahan) != '' ? ' Kel. '.$identitas->kelurahan : '')
                .(trim($identitas->kecamatan) != '' ? ' Kec. '.$identitas->kecamatan : '')
                .(trim($identitas->telp) != '' ? ' Telp. '.$identitas->telp : '')
                .(trim($identitas->fax) != '' ? ' Fax. '.$identitas->fax : ''),]);
        $pdf->setPaper('a4', 'landscape')->setWarnings(false);
        return $pdf->stream();
    }

    public function laporanIuranDetailTerseleksi(LaporanPembayaranTerseleksiRequest $request) {
        $input = $request->all();

        $dataAwal = $this->getDaftarPembayaranIuran($input['tahunajaran'], $input['daritanggal'],$input['sampaitanggal']);

        $data = [];

        foreach($dataAwal as $key => $d) {
            $ada = false;
            foreach($input['id'] as $idSeleksi) {
                if ($idSeleksi == $d[0]) {
                    $ada = true;
                    break;
                }
            }
            if ($ada) {
                $data[$key] = $d;
            }
        }

//        dd($data);

        $namaTahunAjaran = TahunAjaran::where('id', $input['tahunajaran'])->select('name')->first();
        $subtitle = 'Tahun Ajaran : '.$namaTahunAjaran['name'];

        if ($input['daritanggal'] != '' && $input['sampaitanggal'] != '') {
            $subtitle .= ', Periode : '.$input['daritanggal'].' s/d '.$input['sampaitanggal'];
        }

//        foreach($data as $key => $d) {
//        }

        $total = 0;
        foreach($data as $key => $d) {
            $totalstring = $d[5];
            $totalstring = str_replace('.', '', $totalstring);
            $total += $totalstring;

            $data[$key][7] = $this->getIuranDetailTahun ($input['tahunajaran'], $d[1]);

//            $d[7] = $this->getIuranDetailTahun ($input['tahunajaran']);
        }

        $identitas = Identitas::first();

        $pdf = App::make('dompdf.wrapper');
        $pdf->loadView('page.laporan.preview.previewlaporaniurandetail', ['pembayaran'=>$data, 'subtitle'=>$subtitle, 'total'=>$total, 'cacah'=> 1,'srcimage' => env('IMG_DIR') . '/' . env('IMG_IDENTITAS_DIR') . '/' . $identitas->logo,
            'namadinas' => strtoupper($identitas->dinas),
            'namakabupaten' => 'KABUPATEN '.strtoupper($identitas->kabupaten),
            'namasekolah' => $identitas->nama,
            'namaalamat' => $identitas->alamat.(trim($identitas->kelurahan) != '' ? ' Kel. '.$identitas->kelurahan : '')
                .(trim($identitas->kecamatan) != '' ? ' Kec. '.$identitas->kecamatan : '')
                .(trim($identitas->telp) != '' ? ' Telp. '.$identitas->telp : '')
                .(trim($identitas->fax) != '' ? ' Fax. '.$identitas->fax : ''),]);
        $pdf->setPaper('a4', 'landscape')->setWarnings(false);
        return $pdf->stream();
    }

}