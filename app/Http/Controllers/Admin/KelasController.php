<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\HomeController;
use App\Http\Requests\CreateKelasRequest;
use App\Models\Kelas;
use Exception;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;

class KelasController extends HomeController
{
    public function __construct() {
        parent::__construct();
    }

    public function index() {
        return view('page/pengaturan/kelas/index');
    }

    public function kelases() {
        $kelas = Kelas::orderBy('program_id')->orderBy('tingkatan')->get();
        $cacah = 0;
        $data = [];

        foreach ($kelas as $d) {
            $d->loadTambahan();

            $data[$cacah] = [$d->program_name, $d->tingkatan, $d->id];
            $cacah++;

        }

        return response()->json([
            'data' => $data
        ]);
    }

    public function create() {
        return view('page/pengaturan/kelas/create');
    }

    public function getProgramStudi() {
        $program = DB::table('programs')->select('id', 'name')->orderBy('name')->get();

        $cacah = 0;
        $data = [];

        foreach ($program as $i => $d) {
            $data[$cacah] = [$d->id, $d->name];
            $cacah++;

        }

        return response()->json([
            'data' => $data
        ]);
    }

    public function store(CreateKelasRequest $request)
    {
//        dd($request->all());

        if ($request->ajax()) {
            $input = $request->all();

            if (!isset($input['_token'])) {
                return response()->json([
                    'data' => $input->toArray()
                ]);
            } else {
                $cari = Kelas::where(['program_id'=> $input['program_studi'], 'tingkatan'=> $input['tingkatan']])->first();
//                dd($cari);
                if ($cari != null) {

                    return response()->json([
                        'data' => ['Kelas dengan Program Studi dan Tingkatan yang diinputkan, sudah terdaftar !']
                    ], 422);
                }

                $hasil = $this->simpanTransaksiCreate($input);
                if ($hasil == '') {
                    return response()->json([
                        'data' => 'Sukses Menyimpan'
                    ]);
                } else {
                    return response()->json([
                        'data' => ['Gagal menyimpan data kelas! Periksa data anda dan pastikan server MySQL anda sedang aktif! Err: ' + $hasil]
                    ], 422);
                }

            }
        }
    }

    /**
     * @param $input
     * @return string
     */
    protected function simpanTransaksiCreate($input) {
        DB::beginTransaction();

        try {
//             simpan kelas di sini
            $kelas = new Kelas();
            $kelas->program_id = $input['program_studi'];
            $kelas->tingkatan = $input['tingkatan'];

            $kelas->save();
        } catch (ValidationException $ex) {
            DB::rollback();
            return $ex->getMessage();;
        } catch (Exception $ex) {
            DB::rollback();
            return $ex->getMessage();;
        }

        DB::commit();

        return '';
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $kelas = Kelas::find($id);

        return response()->json([
            'id'=>$kelas->id,
            'tingkatan' => $kelas->tingkatan,
            'program_id' => $kelas->program_id,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(CreateKelasRequest $request, $id)
    {
//        dd($request->all());
        if ($request->ajax()) {
            $input = $request->all();

            if (!isset($input['_token'])) {
                return response()->json([
                    'data' => $input->toArray()
                ]);
            } else {
                $kelas = Kelas::find($id);

                $cari = Kelas::where(['program_id'=> $input['program_studi'], 'tingkatan'=> $input['tingkatan']])->first();
                if ($cari != null) {
                    if ($kelas->id != $cari->id) {
                        return response()->json([
                            'data' => ['Kelas dengan Program Studi dan Tingkatan yang diinputkan, sudah digunakan oleh data lainnya!']
                        ], 422);
                    }
                }

                if ($kelas != null) {
                    $hasil = $this->simpanTransaksiUpdate($input, $kelas);
                    if ($hasil == '') {
                        return response()->json([
                            'data' => 'Sukses Mengubah Data'
                        ]);
                    } else {
                        return response()->json([
                            'data' => ['Gagal mengubah data kelas! Periksa data anda dan pastikan server MySQL anda sedang aktif! Err: ' + $hasil]
                        ], 422);
                    }
                } else {
                    return response()->json([
                        'data' => ['Gagal mengubah data kelas! Kelas tidak ditemukan']
                    ], 422);
                }
            }
        }
    }

    protected function simpanTransaksiUpdate($input, $kelas) {
        DB::beginTransaction();

        try {
            DB::table('kelas')
                ->where('id', $kelas->id)
                ->update(
                    [
                        'tingkatan' => $input['tingkatan'],
                        'program_id' => $input['program_studi'],
                        'updated_at' => date('Y/m/d H:i:s')
                    ]);
        } catch (ValidationException $ex) {
            DB::rollback();
            return $ex->getMessage();
        } catch (Exception $ex) {
            DB::rollback();
            return $ex->getMessage();
        }

        DB::commit();

        return '';
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $kelas = Kelas::find($id);

        $kelas->loadTambahan();

        if ($kelas->banyaksiswa > 0) {
            return response()->json([
                'data' => ['Gagal Menghapus data! Kelas ini digunakan oleh '.$kelas->banyaksiswa.' siswa.']
            ], 422);
        }

        $hasil = $this->simpanTransaksiDelete($kelas);
        if ($hasil == '') {
            return response()->json([
                'data' => 'Sukses Menghapus Data'
            ]);
        } else {
            return response()->json([
                'data' => ['Gagal Menghapus data! Mungkin data ini sedang digunakan oleh data di tabel lainnya! Err: ' + $hasil]
            ], 422);
        }
    }

    protected function simpanTransaksiDelete($kelas)
    {
        DB::beginTransaction();

        try {
            $kelas->delete();
        } catch (ValidationException $ex) {
            DB::rollback();
            return $ex->getMessage();
        } catch (Exception $ex) {
            DB::rollback();
            return $ex->getMessage();
        }

        DB::commit();

        return '';
    }
}
