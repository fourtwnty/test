<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\HomeController;
use App\Http\Requests\CreateSiswaRequest;
use App\Http\Requests\UpdateSiswaRequest;
use App\Models\Kelas;
use App\Models\Siswa;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;

class SiswaController extends HomeController
{
    public function __construct() {
        parent::__construct();
    }

    public function index() {
        return view('page/siswa/index');
    }

    public function siswas() {
        $siswa = Siswa::all();
        $cacah = 0;
        $data = [];

        foreach ($siswa as $d) {
            $d->loadTambahan();

            $data[$cacah] = [$d->nisn, $d->nama, ($d->jk == 'L' ? 'Laki - Laki' : 'Perempuan'), $d->kelas_name, ($d->aktif == '1' ? 'Aktif' : 'Tidak Aktif'), $d->id];
            $cacah++;

        }

        return response()->json([
            'data' => $data
        ]);
    }

    public function create() {
        return view('page/siswa/create');
    }

    public function kelases() {
        $kelas = Kelas::select('id', 'program_id', 'tingkatan')->orderBy('program_id')->orderBy('tingkatan')->get();

        $cacah = 0;
        $data = [];

        foreach ($kelas as $d) {
            $d->loadTambahan();
            $data[$cacah] = [$d->id, $d->program_name. ' ' .$d->tingkatan];
            $cacah++;

        }

        return response()->json([
            'data' => $data
        ]);
    }

    public function store(CreateSiswaRequest $request)
    {
//        dd($request->all());

        if ($request->ajax()) {
            $input = $request->all();

            if (!isset($input['_token'])) {
                return response()->json([
                    'data' => $input->toArray()
                ]);
            } else {
                $cari = Siswa::where('nisn', $input['nisn'])->first();
                if ($cari != null) {

                    return response()->json([
                        'data' => ['NISN Siswa yang diinputkan, sudah terdaftar !']
                    ], 422);
                }

                $hasil = $this->simpanTransaksiCreate($input);
                if ($hasil == '') {
                    return response()->json([
                        'data' => 'Sukses Menyimpan'
                    ]);
                } else {
                    return response()->json([
                        'data' => ['Gagal menyimpan data siswa! Periksa data anda dan pastikan server MySQL anda sedang aktif! Err: ' + $hasil]
                    ], 422);
                }

            }
        }
    }

    /**
     * @param $input
     * @return string
     */
    protected function simpanTransaksiCreate($input) {
        DB::beginTransaction();

        try {
//             simpan kelas di sini
            $arr_tgl = explode ("/", $input['tgl_lahir'], 3);

            $siswa = new Siswa();
            $siswa->nisn = $input['nisn'];
            $siswa->nama = $input['nama'];
            $siswa->jk = $input['jk'];
            $siswa->tempat_lahir = $input['tempat_lahir'];
            $siswa->tgl_lahir = Carbon::createFromDate((int)$arr_tgl[2],(int)$arr_tgl[0],(int)$arr_tgl[1]);
            $siswa->alamat = $input['alamat'];
            $siswa->aktif = $input['aktif'];
            $siswa->kelas_id = $input['kelas_id'];
            $siswa->save();
        } catch (ValidationException $ex) {
            DB::rollback();
            return $ex->getMessage();;
        } catch (Exception $ex) {
            DB::rollback();
            return $ex->getMessage();;
        }

        DB::commit();

        return '';
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
//        $table->increments('id');
//        $table->string('nisn', 10)->unique;
//        $table->string('nama', 100);
//        $table->enum('jk', ['L', 'P']);
//        $table->string('tempat_lahir', 50);
//        $table->date('tgl_lahir');
//        $table->text('alamat');
//        $table->enum('aktif', ['0', '1']);
//
//        $table->integer('kelas_id')->unsigned();
        $siswa = Siswa::find($id);

        return response()->json([
            'id'=>$siswa->id,
            'nisn' => $siswa->nisn,
            'nama' => $siswa->nama,
            'jk'=>$siswa->jk,
            'tempat_lahir' => $siswa->tempat_lahir,
            'tgl_lahir' => $siswa->tgl_lahir->format('m/d/Y'),
            'alamat'=>$siswa->alamat,
            'aktif' => $siswa->aktif,
            'kelas_id' => $siswa->kelas_id,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateSiswaRequest $request, $id)
    {
//        dd($request->all());
        if ($request->ajax()) {
            $input = $request->all();

            if (!isset($input['_token'])) {
                return response()->json([
                    'data' => $input->toArray()
                ]);
            } else {
                $siswa = Siswa::find($id);

                $cari = Siswa::where('nisn', $input['nisn'])->first();
                if ($cari != null) {
                    if ($siswa->id != $cari->id) {
                        return response()->json([
                            'data' => ['Siswa dengan NISN yang diinputkan, sudah digunakan oleh data lainnya!']
                        ], 422);
                    }
                }

                if ($siswa != null) {
                    $hasil = $this->simpanTransaksiUpdate($input, $siswa);
                    if ($hasil == '') {
                        return response()->json([
                            'data' => 'Sukses Mengubah Data'
                        ]);
                    } else {
                        return response()->json([
                            'data' => ['Gagal mengubah data siswa! Periksa data anda dan pastikan server MySQL anda sedang aktif! Err: ' + $hasil]
                        ], 422);
                    }
                } else {
                    return response()->json([
                        'data' => ['Gagal mengubah data siswa! Siswa tidak ditemukan']
                    ], 422);
                }
            }
        }
    }

    protected function simpanTransaksiUpdate($input, $siswa) {
        DB::beginTransaction();

//        $table->increments('id');
//        $table->string('nisn', 10)->unique;
//        $table->string('nama', 100);
//        $table->enum('jk', ['L', 'P']);
//        $table->string('tempat_lahir', 50);
//        $table->date('tgl_lahir');
//        $table->text('alamat');
//        $table->enum('aktif', ['0', '1']);
//
//        $table->integer('kelas_id')->unsigned();

        $arr_tgl = explode ("/", $input['tgl_lahir'], 3);

        try {
            DB::table('siswa')
                ->where('id', $siswa->id)
                ->update(
                    [
                        'nisn' => $input['nisn'],
                        'nama' => $input['nama'],
                        'jk' => $input['jk'],
                        'tempat_lahir' => $input['tempat_lahir'],
                        'tgl_lahir' => Carbon::createFromDate((int)$arr_tgl[2],(int)$arr_tgl[0],(int)$arr_tgl[1]),
                        'alamat' => $input['alamat'],
                        'aktif' => $input['aktif'],
                        'kelas_id' => $input['kelas_id'],
                        'updated_at' => date('Y/m/d H:i:s')
                    ]);
        } catch (ValidationException $ex) {
            DB::rollback();
            return $ex->getMessage();
        } catch (Exception $ex) {
            DB::rollback();
            return $ex->getMessage();
        }

        DB::commit();

        return '';
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $siswa = Siswa::find($id);

        $siswa->loadTambahan();

        if ($siswa->banyakmulai > 0 || $siswa->banyakiuran > 0) {
            return response()->json([
                'data' => ['Gagal Menghapus data! Siswa ini digunakan oleh '.$siswa->banyakiuran.' transaksi iuran komite dan '.$siswa->banyakmulai.' data mulai iuran siswa.']
            ], 422);
        }

        $hasil = $this->simpanTransaksiDelete($siswa);
        if ($hasil == '') {
            return response()->json([
                'data' => 'Sukses Menghapus Data'
            ]);
        } else {
            return response()->json([
                'data' => ['Gagal Menghapus data! Mungkin data ini sedang digunakan oleh data di tabel lainnya! Err: ' + $hasil]
            ], 422);
        }
    }

    protected function simpanTransaksiDelete($siswa)
    {
        DB::beginTransaction();

        try {
            $siswa->delete();
        } catch (ValidationException $ex) {
            DB::rollback();
            return $ex->getMessage();
        } catch (Exception $ex) {
            DB::rollback();
            return $ex->getMessage();
        }

        DB::commit();

        return '';
    }

    public function getDaftarSiswa() {
        $siswa = Siswa::where('aktif', true)->get();
        $cacah = 0;
        $data = [];

        foreach ($siswa as $d) {
            $d->loadTambahan();

            $data[$cacah] = [$d->nisn, $d->nama, ($d->jk == 'L' ? 'Laki - Laki' : 'Perempuan'), $d->kelas_name, $d->id];
            $cacah++;

        }

        return response()->json([
            'data' => $data
        ]);
    }
}