<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\HomeController;
use App\Http\Requests\CreateTarifRequest;
use App\Models\Tarif;
use Exception;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;

class TarifController extends HomeController
{
    public function __construct() {
        parent::__construct();
    }

    public function index() {
        return view('page/pengaturan/tarif/index');
    }

    public function tarifs() {
        $tarif = Tarif::orderBy('program_id')->get();
        $cacah = 0;
        $data = [];

        foreach ($tarif as $d) {
            $d->loadTambahan();

            $data[$cacah] = [$d->program_name, $d->nilai, $d->id];
            $cacah++;

        }

        return response()->json([
            'data' => $data
        ]);
    }

    public function create() {
        return view('page/pengaturan/tarif/create');
    }

    public function store(CreateTarifRequest $request)
    {
//        dd($request->all());

        if ($request->ajax()) {
            $input = $request->all();

            if (!isset($input['_token'])) {
                return response()->json([
                    'data' => $input->toArray()
                ]);
            } else {
                $cari = Tarif::where('program_id', $input['program_studi'])->first();
                if ($cari != null) {

                    return response()->json([
                        'data' => ['Tarif iuran dengan program studi/jurusan yang diinputkan, sudah terdaftar !']
                    ], 422);
                }

                $hasil = $this->simpanTransaksiCreate($input);
                if ($hasil == '') {
                    return response()->json([
                        'data' => 'Sukses Menyimpan'
                    ]);
                } else {
                    return response()->json([
                        'data' => ['Gagal menyimpan data tarif iuran! Periksa data anda dan pastikan server MySQL anda sedang aktif! Err: ' + $hasil]
                    ], 422);
                }

            }
        }
    }

    /**
     * @param $input
     * @return string
     */
    protected function simpanTransaksiCreate($input) {
        DB::beginTransaction();

        try {
//             simpan kelas di sini
            $tarif = new Tarif();
            $tarif->program_id = $input['program_studi'];
            $tarif->nilai = $input['nilai'];

            $tarif->save();
        } catch (ValidationException $ex) {
            DB::rollback();
            return $ex->getMessage();;
        } catch (Exception $ex) {
            DB::rollback();
            return $ex->getMessage();;
        }

        DB::commit();

        return '';
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tarif = Tarif::find($id);

        return response()->json([
            'id'=>$tarif->id,
            'nilai' => $tarif->nilai,
            'program_id' => $tarif->program_id,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(CreateTarifRequest $request, $id)
    {
//        dd($request->all());
        if ($request->ajax()) {
            $input = $request->all();

            if (!isset($input['_token'])) {
                return response()->json([
                    'data' => $input->toArray()
                ]);
            } else {
                $tarif = Tarif::find($id);

                $cari = Tarif::where('program_id', $input['program_studi'])->first();
                if ($cari != null) {
                    if ($tarif->id != $cari->id) {
                        return response()->json([
                            'data' => ['Tarif iuran untuk program studi/jurusan yang diinputkan, sudah digunakan oleh data lainnya!']
                        ], 422);
                    }
                }

                if ($tarif != null) {
                    $hasil = $this->simpanTransaksiUpdate($input, $tarif);
                    if ($hasil == '') {
                        return response()->json([
                            'data' => 'Sukses Mengubah Data'
                        ]);
                    } else {
                        return response()->json([
                            'data' => ['Gagal mengubah data tarif iuran! Periksa data anda dan pastikan server MySQL anda sedang aktif! Err: ' + $hasil]
                        ], 422);
                    }
                } else {
                    return response()->json([
                        'data' => ['Gagal mengubah data tarif iuran! Tarif iuran tidak ditemukan']
                    ], 422);
                }
            }
        }
    }

    protected function simpanTransaksiUpdate($input, $tarif) {
        DB::beginTransaction();

        try {
            DB::table('tarif')
                ->where('id', $tarif->id)
                ->update(
                    [
                        'nilai' => $input['nilai'],
                        'program_id' => $input['program_studi'],
                        'updated_at' => date('Y/m/d H:i:s')
                    ]);
        } catch (ValidationException $ex) {
            DB::rollback();
            return $ex->getMessage();
        } catch (Exception $ex) {
            DB::rollback();
            return $ex->getMessage();
        }

        DB::commit();

        return '';
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $tarif = Tarif::find($id);

//        $tarif->loadTambahan();
//
//        if ($kelas->banyaksiswa > 0) {
//            return response()->json([
//                'data' => ['Gagal Menghapus data! Kelas ini digunakan oleh '.$kelas->banyaksiswa.' siswa.']
//            ], 422);
//        }

        $hasil = $this->simpanTransaksiDelete($tarif);
        if ($hasil == '') {
            return response()->json([
                'data' => 'Sukses Menghapus Data'
            ]);
        } else {
            return response()->json([
                'data' => ['Gagal Menghapus data! Mungkin data ini sedang digunakan oleh data di tabel lainnya! Err: ' + $hasil]
            ], 422);
        }
    }

    protected function simpanTransaksiDelete($tarif)
    {
        DB::beginTransaction();

        try {
            $tarif->delete();
        } catch (ValidationException $ex) {
            DB::rollback();
            return $ex->getMessage();
        } catch (Exception $ex) {
            DB::rollback();
            return $ex->getMessage();
        }

        DB::commit();

        return '';
    }
}