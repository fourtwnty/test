<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateIdentitasRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
//        $table->string('nama', 100);
//        $table->string('alamat', 200);
//        $table->string('telp', 20);
//        $table->string('fax', 20);
//        $table->string('logo', 100);
//        $table->string('dinas', 100);
//        $table->string('kota', 100);
//        $table->string('kelurahan', 100);
//        $table->string('kecamatan', 100);
//        $table->string('kabupaten', 100);

        return [
            'id' => 'required|numeric|exists:identitas,id',
            'nama' => 'required|min:3|max:100',
            'alamat' => 'required|min:3|max:200',
            'telp' => 'max:20',
            'fax' => 'max:20',
            'dinas' => 'required|min:3|max:100',
            'kota' => 'required|min:3|max:100',
            'kelurahan' => 'required|min:3|max:100',
             'kecamatan' => 'required|min:3|max:100',
            'kabupaten' => 'required|min:3|max:100',
        ];
    }

    public function messages()
    {
        return [
            'id.required'  => 'Id identitas harus ada',
            'id.numeric'  => 'Id identitas tidak valid',
            'id.exists'  => 'Id identitas tidak ditemukan di database',

            'nama.required'  => 'Nama identitas harus ada',
            'nama.max'  => 'Nama identitas maksimal :max karakter',
            'nama.min'  => 'Nama identitas minimum :min karakter',

            'alamat.required'  => 'Alamat harus ada',
            'alamat.max'  => 'Alamat maksimal :max karakter',
            'alamat.min'  => 'Alamat minimum :min karakter',

            'telp.max'  => 'Telepon maksimal :max karakter',
            'fax.max'  => 'Fax maksimal :max karakter',

            'dinas.required'  => 'Nama dinas harus ada',
            'dinas.max'  => 'Nama dinas maksimal :max karakter',
            'dinas.min'  => 'Nama dinas minimum :min karakter',

            'kota.required'  => 'Nama kota harus ada',
            'kota.max'  => 'Nama kota maksimal :max karakter',
            'kota.min'  => 'Nama kota minimum :min karakter',

            'kelurahan.required'  => 'Nama kelurahan harus ada',
            'kelurahan.max'  => 'Nama kelurahan maksimal :max karakter',
            'kelurahan.min'  => 'Nama kelurahan minimum :min karakter',

            'kecamatan.required'  => 'Nama kecamatan harus ada',
            'kecamatan.max'  => 'Nama kecamatan maksimal :max karakter',
            'kecamatan.min'  => 'Nama kecamatan minimum :min karakter',

            'kabupaten.required'  => 'Nama kabupaten harus ada',
            'kabupaten.max'  => 'Nama kabupaten maksimal :max karakter',
            'kabupaten.min'  => 'Nama kabupaten minimum :min karakter',
        ];
    }
}
