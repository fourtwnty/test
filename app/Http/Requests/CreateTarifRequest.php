<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateTarifRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'program_studi'       => 'required|numeric|exists:programs,id',
            'nilai'       => 'required|numeric|min:1',
        ];
    }

    public function messages()
    {
        return [

            'program_studi.required'  => 'Program studi / jurusan harus ada',
            'program_studi.numeric'  => 'Program studi / jurusan tidak valid',
            'program_studi.exists'  => 'Program studi / jurusan tidak terdaftar di database',

            'nilai.required'  => 'Nilai tarif iuran harus ada',
            'nilai.numeric'  => 'Nilai tarif iuran harus berupa angka',
            'nilai.min'  => 'Nilai tarif iuran minimal :min',
        ];
    }
}
