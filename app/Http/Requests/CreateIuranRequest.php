<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateIuranRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
//        T160924033216
        return [
            'kodetransaksi' => 'required|min:13|max:13|unique:iuran,id',
            'nisn' => 'required|min:10|max:10|exists:siswa,nisn',
            'data_seleksi' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'kodetransaksi.required'  => 'Kode Transaksi harus ada',
            'kodetransaksi.max'  => 'Kode Transaksi maksimal :max karakter.',
            'kodetransaksi.min'  => 'Kode Transaksi minimal :min karakter.',
            'kodetransaksi.unique'  => 'Kode Transaksi sudah terdaftar di database. Refresh page anda!',

            'nisn.required'  => 'NISN harus ada',
            'nisn.max'  => 'NISN Siswa maksimal :max karakter.',
            'nisn.min'  => 'NISN Siswa minimal :min karakter.',
            'nisn.exists'  => 'NISN Siswa tidak terdaftar di database.',

            'data_seleksi.required'  => 'Data tunggakan yang akan dibayarkan harus ada',
        ];
    }
}
