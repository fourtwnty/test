<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateSiswaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nisn'       => 'required|max:10|min:10|unique:siswa',
            'nama'       => 'required|max:100|min:2',
            'jk'       => 'required|in:L,P',
            'tempat_lahir'      => 'required|min:3:max:50',
            'tgl_lahir'      => 'required|date|before:today',
            'kelas_id'       => 'required|numeric|exists:kelas,id',
            'aktif'      => 'required|numeric|in:1,0',
            'alamat' 	 => 'required|max:200|min:3',
        ];
    }

    public function messages()
    {
        return [
            'nisn.required'  => 'NISN harus diisi',
            'nisn.max'  => 'NISN harus :max karakter angka',
            'nisn.min'  => 'NISN harus :min karakter numeric',
            'nisn.unique'  => 'NISN sudah terdaftar di database',

            'nama.required'  => 'Nama harus diisi',
            'nama.max'  => 'Nama maksimal berisi :max karakter',
            'nama.min'  => 'Nama minimal berisi :min karakter',

            'jk.required' => 'Jenis kelamin belum dipilih',
            'jk.in' 	 => 'Jenis kelamin harus bernilai L atau P',

            'tempat_lahir.required'=> 'Tempat kelahiran harus diisi',
            'tempat_lahir.min'=> 'Tempat kelahiran minimal :min karakter',
            'tempat_lahir.max'=> 'Tempat kelahiran maksimal :max karakter',

            'tgl_lahir.required'=> 'Tanggal kelahiran harus diisi',
            'tgl_lahir.date'=> 'Tanggal kelahiran harus berupa tanggal',
            'tgl_lahir.before'=> 'Tanggal kelahiran tidak valid',

            'kelas_id.required'=> 'Kelas siswa belum dipilih',
            'kelas_id.numeric'=> 'Kelas siswa tidak valid',
            'kelas_id.exists'=> 'Kelas siswa tidak terdaftar di database',

            'aktif.required'=> 'Status siswa belum dipilih',
            'aktif.numeric'=> 'Status siswa tidak valid',
            'aktif.in'=> 'Status siswa harus bernilai Aktif atau Tidak Aktif',

            'alamat.required'  => 'Alamat harus diisi',
            'alamat.max'  => 'Alamat maksimal berisi :max karakter',
            'alamat.min'  => 'Alamat minimal berisi :min karakter',
        ];
    }
}
