<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateMulaiIuranRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'bulan_id' => 'required|numeric|exists:bulan,id',
            'tahun_id' => 'required|numeric|exists:tahun_ajaran,id'
        ];
    }

    public function messages()
    {
        return [
            'bulan_id.required'  => 'Bulan di tahun ajaran belum dipilih.',
            'bulan_id.numeric'  => 'Bulan di tahun ajaran, tidak valid.',
            'bulan_id.exists'  => 'Bulan di tahun ajaran tidak terdaftar di database',

            'tahun_id.required'  => 'Tahun ajaran belum dipilih.',
            'tahun_id.numeric'  => 'Tahun ajaran tidak valid.',
            'tahun_id.exists'  => 'Tahun ajaran tidak terdaftar di database',
        ];
    }
}
