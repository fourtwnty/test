<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LaporanPembayaranTerseleksiRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'tahunajaran' => 'required|numeric|exists:tahun_ajaran,id',
            'id'=>'required'
        ];
    }

    public function messages()
    {
        return [
            'tahunajaran.required'  => 'Anda belum memilih tahun ajaran!',
            'tahunajaran.numeric'  => 'Tahun Ajaran tidak valid!',
            'tahunajaran.exists'  => 'Tahun Ajaran tidak terdaftar di database!',

            'id.required'  => 'Silahkan memilih data yang akan dicetak di daftar!',
        ];
    }
}
