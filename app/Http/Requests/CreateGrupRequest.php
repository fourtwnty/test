<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateGrupRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'       => 'required|max:100|min:2|unique:roles',
            'display_name'       => 'required|max:200|min:2',
            'permissions_grup'      => 'required',
        ];
    }

    public function messages()
    {
        return [
            'name.required'  => 'Kolom Nama Group User harus diisi',
            'name.max'  => 'Kolom Nama Group User maksimal :max karakter',
            'name.min'  => 'Kolom Nama Group User minimal :min karakter',
            'name.unique'  => 'Nama Group User sudah terdaftar',

            'display_name.required'  => 'Kolom Nama yang akan terlihat harus diisi',
            'display_name.max'  => 'Kolom Nama yang akan terlihat maksimal :max karakter',
            'display_name.min'  => 'Kolom Nama yang akan terlihat minimal :min karakter',

            'permissions_grup.required' => 'Group User harus memiliki Hak Akses'
        ];
    }
}
