<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdatePengesahRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'display_name' => 'required|min:3|max:200',
            'nilai' => 'required|min:3|max:200',
            'nip' => 'required|min:18|max:18'
        ];
    }

    public function messages()
    {
        return [
            'display_name.required'  => 'Kolom nama yang akan terlihat harus diinputkan',
            'display_name.max'  => 'Nama yang akan terlihat maksimal :max karakter',
            'display_name.min'  => 'Nama yang akan terlihat minimal :min karakter',

            'nilai.required'  => 'Kolom nilai pengesah harus diinputkan',
            'nilai.max'  => 'Nilai dari pengesah maksimal :max karakter',
            'nilai.min'  => 'Nilai dari pengesah minimal :min karakter',

            'nip.required'  => 'Kolom NIP pengesah harus diinputkan',
            'nip.max'  => 'NIP dari pengesah maksimal :max karakter',
            'nip.min'  => 'NIP dari pengesah minimal :min karakter'
        ];
    }
}
