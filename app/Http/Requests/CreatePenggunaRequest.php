<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreatePenggunaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username'       => 'required|max:50|min:3|unique:users',
            'email' => 'required|email|max:255|unique:users',
            'role'       => 'required|numeric|exists:roles,id',
            'fullname'       => 'required|max:200|min:3',
            'password'       => 'required|max:50|min:3',
            'address'       => 'required|max:200|min:3',
            'gsm'       => 'max:20',
            'phone'       => 'max:20',
            'gender'       => 'required|in:L,P',
            'description'       => 'max:200',
            'active'      => 'required|in:1,0',
        ];
    }

    public function messages()
    {
        return [
            'email.required'  => 'Kolom alamat email harus diisi',
            'email.max'  => 'Kolom alamat email maksimal :max karakter',
            'email.email'  => 'Alamat email tidak valid',
            'email.unique'  => 'Alamat email yang diinputkan sudah ada',

            'username.required'  => 'Nama pengguna harus ada',
            'username.max'  => 'Nama pengguna maksimal :max karakter',
            'username.min'  => 'Nama pengguna minimal :min karakter',
            'username.unique'  => 'Nama pengguna sudah terdaftar di database',

            'role.required'  => 'Grup pengguna harus ada',
            'role.numeric'  => 'Grup pengguna tidak valid',
            'role.exists'  => 'Grup pengguna tidak terdaftar di database',

            'fullname.required'  => 'Nama lengkap pengguna harus ada',
            'fullname.max'  => 'Nama lengkap pengguna maksimal :max karakter',
            'fullname.min'  => 'Nama lengkap pengguna minimal :min karakter',

            'password.required'  => 'Kata sandi pengguna harus ada',
            'password.max'  => 'Kata sandi pengguna maksimal :max karakter',
            'password.min'  => 'Kata sandi pengguna minimal :min karakter',

            'address.required'  => 'Alamat pengguna harus ada',
            'address.max'  => 'Alamat pengguna maksimal :max karakter',
            'address.min'  => 'Alamat pengguna minimal :min karakter',

            'gsm.max'  => 'No. HP maksimal :max karakter',
            'phone.max'  => 'Nomor Telepon maksimal :max karakter',

            'gender.required'  => 'Jenis kelamin harus ada',
            'gender.in'  => 'Jenis kelamin harus bernilai L atau P',

            'description.max'  => 'Deskripsi tambahan pengguna maksimal :max karakter',

            'active.required'  => 'Status aktif harus ada',
            'active.in'  => 'Status aktif harus bernilai True atau False',
        ];
    }
}
