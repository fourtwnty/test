<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateProgramRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'       => 'required|max:100|min:2|unique:programs',
        ];
    }

    public function messages()
    {
        return [
            'name.required'  => 'Kolom Nama Program Studi / Jurusan harus diisi',
            'name.max'  => 'Kolom Nama Program Studi / Jurusan maksimal :max karakter',
            'name.min'  => 'Kolom Nama Program Studi / Jurusan minimal :min karakter',
            'name.unique'  => 'Nama Program Studi / Jurusan sudah terdaftar',
        ];
    }
}
