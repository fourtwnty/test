<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateTahunRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'       => 'required|max:100|min:2|unique:tahun_ajaran',
        ];
    }

    public function messages()
    {
        return [
            'name.required'  => 'Kolom Nama Tahun Ajaran harus diisi',
            'name.max'  => 'Kolom Nama Tahun Ajaran maksimal :max karakter',
            'name.min'  => 'Kolom Nama Tahun Ajaran minimal :min karakter',
            'name.unique'  => 'Nama Nama Tahun Ajaran sudah terdaftar',
        ];
    }
}
