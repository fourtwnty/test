<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateKelasRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'program_studi'       => 'required|numeric|exists:programs,id',
            'tingkatan'       => 'required|min:1|max:10',
        ];
    }

    public function messages()
    {
        return [

            'program_studi.required'  => 'Program studi / jurusan harus ada',
            'program_studi.numeric'  => 'Program studi / jurusan tidak valid',
            'program_studi.exists'  => 'Program studi / jurusan tidak terdaftar di database',

            'tingkatan.required'  => 'Tingkatan kelas harus ada',
            'tingkatan.min'  => 'Tingkatan kelas minimal :min karakter',
            'tingkatan.max'  => 'Tingkatan kelas maksimal :max karakter',
        ];
    }
}
